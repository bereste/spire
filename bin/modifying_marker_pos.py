# For deatiled information please refer to the script: cluster_the_markers.sh. () 


import sys 

Marker_special_format = open("%s"%(sys.argv[1]),"rU")
output_folder=sys.argv[2]
new_marekrs_file = open(output_folder+"Markers_pos_clustered.txt","w")



def mean(numberList):
    if len(numberList) == 0:
        return float('nan')
 
    floatNums = [float(x) for x in numberList]
    return sum(floatNums) / len(numberList)

new_markers={} # key:marker,value: [chr,begin, end]

new_marekrs_file.write("Marker\tchr\tPP\tMean\n")
begin=0
for line in Marker_special_format:
	Marker_name=line.split("\t")[0]
	Marker_chr=line.split("\t")[1]
	Marker_pos=line.split("\t")[2]
	des=Marker_name[0]
	Marker_name=Marker_name.lstrip(":-")
	if begin==1:
	# there is an open beginning position of a marker
		if int(begin_name)==int(Marker_name)-1:
			if des==":":
			# the maker before this one is a singleton cluster
				# closing the singleton one
				singleton_chr=new_markers[begin_name][0]
				singleton_pos=new_markers[begin_name][1]
				new_markers[begin_name]=[singleton_chr, singleton_pos, singleton_pos]
				# and opening a new cluster with the current one
				new_markers[Marker_name]=[Marker_chr, Marker_pos]
				begin_name=Marker_name
			if des=="-":
			# It's a two-cluster. This marker describes the endmarker of the given cluster
				tmp=new_markers[begin_name]
	                        tmp.append(Marker_pos)
        	                new_markers[begin_name]=tmp
                	        begin=0
	
		else:
		# this Marker describes the endmarker of the given cluster
			tmp=new_markers[begin_name]
			tmp.append(Marker_pos)
			new_markers[begin_name]=tmp
			begin=0
		 	
	else:
	# no open beginning of a marker. => make a new one
		new_markers[Marker_name]=[Marker_chr, Marker_pos]				
		begin_name=Marker_name	
		begin=1




for Marker_name in new_markers:
	
	chrom=new_markers[Marker_name][0]
	PP_b=new_markers[Marker_name][1].replace("\n","")
	PP_e=new_markers[Marker_name][2].replace("\n","")
	mean_a=mean([PP_e,PP_b])

	
	new_marekrs_file.write("%s\t%s\t%s-%s\t%s\n"%(Marker_name,chrom,PP_b,PP_e,mean_a))


