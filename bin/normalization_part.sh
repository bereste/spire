#!/bin/bash

# The Normalization part of the main script (procedure.sh)

norm_meth=$1
output_dir=$2 
output_process_file=$3 
DUAL_BIB_MODE=$4 
miRNA_lib=$5 
miRNA_lib2=$6  
norm_func_file=$7 
tmp_folder_path=$8 
EXCLUDE_NORM=$9 
both_libs=${10} 
scripts_dir=${11}
plot_dir=${12}
excluding_normalization=("${@:13}")

# no qqplotting as of now! (01.11.2011)
case ${norm_meth} in 
1*)  
	norm_meth_name="Absolute count";;
2*)  
	norm_meth_name="Quantile normalization";;
3*)  
	norm_meth_name="Reference based quantile normalization (rahman)";;
4*)  
	norm_meth_name="Trimmed end mean of M value (TMM)";;
*)   
	echo "  \e[00;35mNOTE:\e[00m Normalization method is not defined. Setting to default (Reference based quantile normalization) "
	norm_meth_name="Reference based quantile normalization (rahman)"
	${norm_meth}=3;;
esac 
echo -e "\n\e[00;34mNOTE:\e[00m Normalizing data. Using method: ${norm_meth_name} (${norm_meth})"
echo -e "	Normalization of the data (method: ${norm_meth_name}) and plotting-----" >> ${output_dir}/${output_process_file}


echo "habada:"${normalized_coverage_file}


if [ "$DUAL_BIB_MODE" -eq 1 ];then
	echo -e "       \e[00;35mNOTE:\e[00m Using two gene libraries. ( $(basename "$miRNA_lib") AND $(basename "$miRNA_lib2")).\n	The counts are normalized separately for each of the libraries" 
	echo -e "	NOTE: Using two gene libraries. ( $(basename "$miRNA_lib") AND $(basename "$miRNA_lib2")).\n	The counts are normalized separately for each of the libraries" >> ${output_dir}/${output_process_file}

	echo "First Library (${miRNA_lib})"
	echo "	First Library (${miRNA_lib})" >> ${output_dir}/${output_process_file}
	Rscript ${scripts_dir}plotting.r "${output_dir}allQX_miRNA_coverage.txt_lib1" $norm_func_file "lib1" ${norm_meth} $(basename "$miRNA_lib") ${output_dir} ${plot_dir}
	# Shifting the first line with the names of the samples one tab to the right (to fitt the format. R function write.table does not do that for some reason)
	awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib1
	mv ${output_dir}allQX_miRNA_coverage_norm.txt  ${output_dir}allQX_miRNA_coverage_norm.txt_bak

	echo "Second Library (${miRNA_lib2})"
	echo "	Second Library (${miRNA_lib2})"  >> ${output_dir}/${output_process_file}
	awk '(NR==1){print $0}' ${output_dir}allQX_miRNA_coverage.txt_lib1 > ${tmp_folder_path}/header_for_lib2.tmp
	cat ${tmp_folder_path}/header_for_lib2.tmp ${output_dir}allQX_miRNA_coverage.txt_lib2 > ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_ready_tmp
	mv ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_ready_tmp ${output_dir}/allQX_miRNA_coverage.txt_lib2
	awk '!x[$0]++' ${output_dir}allQX_miRNA_coverage.txt_lib2 > ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_tmpTMP
	mv ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_tmpTMP ${output_dir}/allQX_miRNA_coverage.txt_lib2
	Rscript ${scripts_dir}plotting.r "${output_dir}/allQX_miRNA_coverage.txt_lib2" $norm_func_file "lib2" ${norm_meth}  $(basename "$miRNA_lib2") ${output_dir} ${plot_dir}
	awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}/allQX_miRNA_coverage_norm.txt | sed 1d > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib2
	awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}/allQX_miRNA_coverage_norm.txt > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib2_with_names
	cat ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib1 ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib2 > ${output_dir}allQX_miRNA_coverage_norm_all.txt

	if  [ "$EXCLUDE_NORM" -eq 1 ];then
		# Extra for libraries without defined files (e.g. without the parents: HW and N2) (Refer to the config file)
		echo -e "	\e[00;35mNOTE:\e[00m Normalizing the libraries without the following columns:  (${excluding_normalization[@]})"
		echo -e "	NOTE: Normalizing the libraries without the following columns: (${excluding_normalization[@]})" >> ${output_dir}/${output_process_file}

		awk '(NR==1){print $0}' ${output_dir}allQX_miRNA_coverage.txt_lib1 | sed 's/\t/\n/g' | sed '/^$/d' > ${tmp_folder_path}/header_to_grep.txt
		echo "${excluding_normalization[@]}" | sed 's/ /\n/g' | sed 's/.fastq//g'> ${tmp_folder_path}/files_to_exclude.txt
		columns_to_exclude_tmp=$(grep -n -h -f ${tmp_folder_path}/files_to_exclude.txt ${tmp_folder_path}/header_to_grep.txt | awk -F ":" {'print $1'}| sed 's/$/,/g' )
		columns_to_exclude=$(echo $columns_to_exclude_tmp | sed 's/,$//g' | sed 's/ //g')

		cut -f "${columns_to_exclude}" --complement ${output_dir}allQX_miRNA_coverage.txt_lib1 > ${output_dir}allQX_miRNA_coverage.txt_lib1_EXCL
		cut -f "${columns_to_exclude}" --complement ${output_dir}allQX_miRNA_coverage.txt_lib2 > ${output_dir}allQX_miRNA_coverage.txt_lib2_EXCL

		Rscript ${scripts_dir}plotting.r "${output_dir}allQX_miRNA_coverage.txt_lib1_EXCL" $norm_func_file "lib1_EXCL" ${norm_meth} $(basename "$miRNA_lib") ${output_dir} ${plot_dir}
		awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib1_EXCL
		awk '(NR==1){print $0}' ${output_dir}allQX_miRNA_coverage.txt_lib1_EXCL > ${tmp_folder_path}/header_for_lib2_EXCL.tmp
		cat ${tmp_folder_path}/header_for_lib2_EXCL.tmp ${output_dir}allQX_miRNA_coverage.txt_lib2_EXCL > ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_ready_tmp_EXCL
		mv ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_ready_tmp_EXCL ${output_dir}allQX_miRNA_coverage.txt_lib2_EXCL
		awk '!x[$0]++' ${output_dir}allQX_miRNA_coverage.txt_lib2_EXCL > ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_tmpTMP_EXCL
		mv ${tmp_folder_path}/allQX_miRNA_coverage.txt_lib2_tmpTMP_EXCL ${output_dir}allQX_miRNA_coverage.txt_lib2_EXCL
		Rscript ${scripts_dir}plotting.r "${output_dir}allQX_miRNA_coverage.txt_lib2_EXCL" $norm_func_file "lib2_EXCL" ${norm_meth} $(basename "$miRNA_lib2") ${output_dir} ${plot_dir}
		awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt | sed 1d > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib2_EXCL
		awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib2_with_names_EXCL
		cat ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib1_EXCL ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_lib2_EXCL > ${output_dir}allQX_miRNA_coverage_norm.txt_EXCL
		normalized_coverage_file=${output_dir}allQX_miRNA_coverage_norm.txt_EXCL
	fi;


	# Normalization of the two libraries together
	echo -e "Normalizing the maping results for both libraries together"    
	echo -e "	Normalizing both the maping results of the both libraries together ($both_libs)" >> ${output_dir}/${output_process_file}
	Rscript ${scripts_dir}plotting.r "${output_dir}allQX_miRNA_coverage.txt" $norm_func_file "both" ${norm_meth}  $(basename "$both_libs") ${output_dir} ${plot_dir}
	# shifting the first line one tab to the right
	awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_tmp
	mv ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_tmp  ${output_dir}allQX_miRNA_coverage_norm_2libs_together.txt


	if  [ "$EXCLUDE_NORM" -eq 1 ];then
		echo -e "Normalizing both libraries together, leaving out the defined files (strains): (${excluding_normalization[@]}) "
		echo -e "	Normalizing both libraries together, leaving out the defined files (strains): (${excluding_normalization[@]}) ">> ${output_dir}/${output_process_file}
		cut -f "${columns_to_exclude}" --complement ${output_dir}allQX_miRNA_coverage.txt > ${output_dir}allQX_miRNA_coverage.txt_EXCL
		Rscript ${scripts_dir}plotting.r "${output_dir}allQX_miRNA_coverage.txt_EXCL" $norm_func_file "both_EXCL" ${norm_meth} $(basename "$both_libs") ${output_dir} ${plot_dir}
		awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt > ${output_dir}allQX_miRNA_coverage_norm.txt_EXCL_norm_together
		echo "		NOTE: allQX_miRNA_coverage_norm.txt_EXCL: libs with excluded strains are normalized sepately and merged together into one file afterwards "  >> ${output_dir}/${output_process_file}
		echo "		NOTE: allQX_miRNA_coverage_norm.txt_EXCL_norm_together: libs normalized together "  >> ${output_dir}/${output_process_file}
		normalized_coverage_file=${output_dir}allQX_miRNA_coverage_norm.txt_EXCL_norm_together
		
	fi;
	
	normalized_coverage_file=${output_dir}allQX_miRNA_coverage_norm_2libs_together.txt
	echo "		NOTE: allQX_miRNA_coverage_norm_all.txt: libs normalized sepately and merged together into one file afterwards "  >> ${output_dir}/${output_process_file}
	echo "		NOTE: allQX_miRNA_coverage_norm_2libs_together.txt: libs normalized together "  >> ${output_dir}/${output_process_file}

	######################
	# Cleaning up
	#rm header_for_lib2.tmp header_for_lib2_NO_N2_HW.tmp ${dataset_dir}allQX_miRNA_coverage.txt_lib1_NO_N2_HW ${dataset_dir}allQX_miRNA_coverage.txt_lib2_NO_N2_HW 
	#rm allQX_miRNA_coverage.txt_NO_HW_N2 allQX_miRNA_coverage_norm.txt
	######################

else
	echo -e "	\e[00;35mNOTE:\e[00m Using one gene library. (${miRNA_lib}).\n  The mapping counts are normalized" 
	echo -e "	NOTE: Using one gene library. (${miRNA_lib}).\n The counts are normalized" >> ${output_dir}/${output_process_file}
	Rscript ${scripts_dir}plotting.r "${output_dir}allQX_miRNA_coverage.txt" $norm_func_file "single_lib" ${norm_meth} $(basename "$both_libs") ${output_dir} ${plot_dir}
	awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_tmp
	mv ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_tmp ${output_dir}allQX_miRNA_coverage_norm.txt
	normalized_coverage_file=${output_dir}allQX_miRNA_coverage_norm.txt

	if  [ "$EXCLUDE_NORM" -eq 1 ];then
		
		echo -e "Normalizing the library, leaving out the defined files (strains): (${excluding_normalization[@]}) "
		echo -e "	Normalizing the library, leaving out the defined files (strains): (${excluding_normalization[@]}) ">> ${output_dir}/${output_process_file}

		awk '(NR==1){print $0}' ${output_dir}allQX_miRNA_coverage.txt | sed 's/\t/\n/g' | sed '/^$/d' > ${tmp_folder_path}/header_to_grep.txt
		echo "${excluding_normalization[@]}" | sed 's/ /\n/g' | sed 's/.fastq//g'> ${tmp_folder_path}/files_to_exclude.txt
		columns_to_exclude_tmp=$(grep -n -h -f ${tmp_folder_path}/files_to_exclude.txt ${tmp_folder_path}/header_to_grep.txt | awk -F ":" {'print $1'}| sed 's/$/,/g' )
		columns_to_exclude=$(echo $columns_to_exclude_tmp | sed 's/,$//g' | sed 's/ //g')

		cut -f "${columns_to_exclude}" --complement ${output_dir}allQX_miRNA_coverage.txt > ${output_dir}allQX_miRNA_coverage.txt_EXCL
		Rscript ${scripts_dir}plotting.r "${output_dir}allQX_miRNA_coverage.txt_EXCL" $norm_func_file "single_lib_EXLC" ${norm_meth} $(basename "$both_libs") ${output_dir} ${plot_dir}
		awk '(NR==1){print "\t"$0} (NR>1){print $0}' ${output_dir}allQX_miRNA_coverage_norm.txt > ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_tmp
		mv ${tmp_folder_path}/allQX_miRNA_coverage_norm.txt_tmp ${output_dir}allQX_miRNA_coverage_norm.txt_EXCL
		normalized_coverage_file=${output_dir}allQX_miRNA_coverage_norm.txt_EXCL
	fi;
fi;


#### Exporting the normalized_coverage_file as a global variable
## The problem faced here is following: the parent script ./procedure.sh get the value of normalized_coverage_file from the config file (In case there is no normalization needed and THIS script does not run). If the child script ./preprocessing.sh runs it needs to update the variable normalized_coverage_file. Unfortunately the child bash script cannot update a global variable (even using export function). Therefore ./preprocessing.sh creates a small file, which holds the new value of the variable. This file is than read into the bash environment of the parent script. 
rm -f ${tmp_folder_path}/normalized_coverage_file_update.txt
echo "normalized_coverage_file=${normalized_coverage_file}" >> ${tmp_folder_path}/normalized_coverage_file_update.txt

