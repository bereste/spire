#!/bin/bash

# The statistical part of the main script (procedure.sh)

miRBase_conservations=$1
miRNA_lib=$2 
normalized_coverage_file=$3 
tmp_folder_path=$4 
DUAL_BIB_MODE=$5 
miRNA_lib2=$6  
output_dir=$7 
output_process_file=$8 
scripts_dir=$9 
cons_lib=${10} 
plot_dir=${11}
both_libs=${12}

echo -e "\n\e[00;34mNOTE:\e[00m Doing a further statistical analysis on the normalized data"
echo -e "       Doing a further statistical analysis on the normalized data.\n  Using the following file with normalized gene counts: ${normalized_coverage_file}" >> ${output_dir}/${output_process_file}

# Checking if analysis regarding the conservation is needed
# checking if the config file exists
if [ ! -f "$miRBase_conservations" ]
then
	no_cons=1
else
	no_cons=0
fi;



# Selecting genes from the used libraries
first_lib_grep=$(head -1 ${miRNA_lib} | sed 's/>//g' | awk -F "[-_]" '{print $1}')
awk '(NR > 1){print $1}' ${normalized_coverage_file} | grep ${first_lib_grep} | sed "s/${first_lib_grep}_//g" | sed "s/${first_lib_grep}-//g" |sed "s/$/\t/g" > ${tmp_folder_path}/miRNA_names_${first_lib_grep}

if [ "$DUAL_BIB_MODE" -eq 1 ];then
	second_lib_grep=$(head -1 ${miRNA_lib2} | sed 's/>//g' | awk -F "[-_]" '{print $1}')
	awk '(NR > 1){print $1}' ${normalized_coverage_file} | grep ${second_lib_grep} | sed "s/${second_lib_grep}_//g" | sed "s/${second_lib_grep}-//g" |sed "s/$/\t/g" > ${tmp_folder_path}/miRNA_names_${second_lib_grep}
	grep -i -f ${tmp_folder_path}/miRNA_names_${second_lib_grep} ${tmp_folder_path}/miRNA_names_${first_lib_grep} -v > ${tmp_folder_path}/${first_lib_grep}_uniq_miRNAs
	grep -i -f ${tmp_folder_path}/miRNA_names_${first_lib_grep} ${tmp_folder_path}/miRNA_names_${second_lib_grep} -v | sed 's/\n$/\t\n/g' > ${tmp_folder_path}/${second_lib_grep}_uniq_miRNAs
	# selecting miRNA pairs
	grep -i -f ${tmp_folder_path}/miRNA_names_${second_lib_grep} ${tmp_folder_path}/miRNA_names_${first_lib_grep} | sed 's/[\t*]\n$/\t\n/g' > ${tmp_folder_path}/miRNAs_${second_lib_grep}_${first_lib_grep}_pairs
	awk '(NR==1){print $0}' ${normalized_coverage_file} > ${tmp_folder_path}/strains_used_names.txt
	sed 's/\t//g' ${tmp_folder_path}/miRNAs_${second_lib_grep}_${first_lib_grep}_pairs | sed 's/$/$/g'> ${tmp_folder_path}/miRNAs_${second_lib_grep}_${first_lib_grep}_pairs_no_tabs
	rm -f ${tmp_folder_path}/Identical_miRNAs.txt ${tmp_folder_path}/Not_identical_miRNAs.txt

	echo -e "       Checking for genes with identical/non-identical sequences"
	echo -e "       Checking for genes with identical/non-identical sequences" >> ${output_dir}/${output_process_file}
	for name_to_check in $(cat ${tmp_folder_path}/miRNAs_${second_lib_grep}_${first_lib_grep}_pairs_no_tabs)
	do
		simsim=$(grep -i $name_to_check ${both_libs} -A1|  sed 's/--//g' | sed '/^$/d'| grep -v -e "^>" | uniq -c | awk '(NR==1){print $1}')
		if [[ $simsim -eq 2 ]];then
			echo -e $name_to_check | sed 's/\$//g' >>  ${tmp_folder_path}/Identical_miRNAs.txt
		else
			echo -e $name_to_check | sed 's/\$//g'>>  ${tmp_folder_path}/Not_identical_miRNAs.txt
		fi;
	done

	# TODO: In case of two gene libraries(And also only in case we have a conservation of genes): the expression should be calculated for each gene-pair individually (as for the eQTL). MA-Plot however requieres in this case the values of both genes in a pair. In this case two files should be handed to the analysis.r file. (NOTE: I can attach the "new, special" expression file at the end of the Rscript analysis.r Aufruf. In this way in wont coplain in any of the other cases)

fi;




# Running the statistical analysis script
echo -e "       Running statistical analysis... ($(basename ${normalized_coverage_file}))"
echo -e "       Running statistical analysis  ($(basename ${normalized_coverage_file}))" >> ${output_dir}/${output_process_file}

# Dual library mode check               
if [ "$DUAL_BIB_MODE" -eq 1 ];then
	echo -e "               Dual library mode is active"
	Dual_mode=1
	lib_name="${first_lib_grep}_${second_lib_grep}"
else
	Dual_mode=0
	lib_name="${first_lib_grep}"
fi;
echo $miRBase_conservations
Rscript ${scripts_dir}analysing.r "${normalized_coverage_file}" "${lib_name}" "${Dual_mode}" "0.45" "${output_dir}" "${plot_dir}" $no_cons ${tmp_folder_path}/Identical_miRNAs.txt ${tmp_folder_path}/Not_identical_miRNAs.txt ${tmp_folder_path}/${first_lib_grep}_uniq_miRNAs $miRBase_conservations $cons_lib

