# Target expression analysis
# This produces an expression plot of the miRNA and its Target genes



pictar_data_folder=$1 #"/data/bioinformatics/Collaborations/Hawaii/Pictar_v18_RIna_novel_all/pictar_all/"
qtl_results=$2 # "QTL_name" "miRNA_name" "QTL_pos_in_CM" "chromosome" "miRNA_marker" format. This file is also needed for cis/trans analysis  
Rockman_R_Object=$3 #"/data/bioinformatics/Collaborations/Hawaii/Rockman_RIL/RILGeneExpressionData.R"
my_R_object=$4 #"/data/bioinformatics/Collaborations/Hawaii/QX/RF/cross_data_new_map.R"
scripts_dir=$5 
output_dir=$6


miRNA_check=$(awk -F" " '(NR>1){print $2}' $qtl_results |uniq |sed 's/"//g'|sed 's/cel.//g' | sed 's/mir./mir-/g' |sed 's/_/-/g'|sed 's/$/\t/g')
for i in $miRNA_check;
do 
     
        i_print=$i
     
        # parsing the right pictar file with targets    
        c1=$(ls -1 $pictar_data_folder |grep -i "${i}$")
        if [[ "$c1" = "" ]];then
                c1=$(ls -1 $pictar_data_folder |grep -i "${i}-5p$")     
        fi; 
        if [[ "$c1" = "" ]];then
                i=$(echo $i | awk -F "-length-" '{print $1}')
                c1=$(ls -1 $pictar_data_folder |grep -i "${i}$")     
        fi; 
        if [[ "$c1" = "" ]];then
                i=$(echo $i | awk -F "[..]" '{print $1"-"$2}')
                c1=$(ls -1 $pictar_data_folder |grep -i "${i}$")     
        fi; 
        echo -e "\t"$i_print"\t:"$c1

        if [[ "$c1" != ""  ]];then
     
                # extracting all GeneSymbols from 
                targets=$(cat $pictar_data_folder$c1 | awk -F "\t" '(NR>1){print $3}' |awk -F "U" '{print $1}' |sort |uniq |sed 's/QX//g' |sed ':a;N;$!ba;s/\n/ /g' |sed 's/ /,/g')
     
                echo $target
                # starting an R script, which does the plotting given  miRNA name + the list of targets 
                lines=$(sed 's/\t/\n/g' /data/bioinformatics/Collaborations/Hawaii/QX/QX_30RILs/allQX_miRNA_coverage_norm_RIL_names.txt |sed '1d' |sed 's/QX//g' |sed ':a;N;$!ba;s/\n/ /g' |sed 's/ /,/g')      
                Rscript ${scripts_dir}target_exp.r $i_print $targets $lines $Rockman_R_Object $my_R_object $output_dir
                echo "ready"
                echo "--------------------"
        else
                echo    "no targets"
                echo "--------------------"
        fi; 
     
done

