#%%%% HEAD %%%%#
#/-
#%% FDR correction of a p_values produced by a eQTL mapping

# Loading the libraries
suppressMessages(library(qtl))
suppressMessages(library(snow))

# Loading the arguments
args <- commandArgs(TRUE)
tmp_folder_path <- args[1] 
use_variable <- args [2] # /data/bioinformatics/Collaborations/Hawaii/QX/QX_30RILs/
sign_level_FDR <- as.numeric(args[3])
plot_folder <- args[4]
RIL_used <- args[5]
step_v <- args[6]
error_prob_v <- args[7]
map_function_v <- args[8]
stepwidth_v <- args[9]
n_draws_v <- args[10]
cross_file_R_OBJECT<- args[11]
output_dir <- args[12]
Number_of_threads_eQTL_FDR<-args[13]


###########################
# loading the cross_file as an R object
load(cross_file_R_OBJECT) # the name to adress the object is cross_data
#creatng the file with FDR corrected p-values
suppressMessages(file.create(paste(output_dir,"QTLs_identified_univariant_FDR.txt", sep=""),showWarnings=FALSE))
# Reading the table with the p_values
all_pvals = read.table(paste(tmp_folder_path, "eQTL_univariate_results_pvalues_all.txt", sep=""),header=T)
# Which method has been used. And applying the apropriate genotypes to the dataset
if (use_variable == "use_all_variable"){
        qtl_method_to_use= c("em", "ehk", "imp")
}else{
        qtl_method_to_use=use_variable 
}

#%%%% BODY %%%%#
# /-
# For each of the methods used
first_run=1
for (ii in qtl_method_to_use) {
	# select only p_values of the one group
        method_selected=subset(all_pvals, method==ii)
	# perform an FDR correction
        qvals=p.adjust(method_selected$pval, method = "fdr")
        all_qvals=cbind(method_selected,qvals)
	# changing the order of the columns
	all_qvals=all_qvals[,c(5,6,4,1,2,8,3,7)]
	print (all_qvals)

        # Checking if we have any results at all for the FDR of the chosen level%.  
        signif_qval=subset(all_qvals, qvals<=sign_level_FDR)
	# Writing the results into a file (the same file as would be produced without the FDR correction, with one aditional column (qval))
	if (first_run==1){
		write.table(signif_qval, file = paste(output_dir,"QTLs_identified_univariant_FDR.txt",sep=""), row.names=FALSE, append=TRUE)
		first_run=0
	}else{
		write.table(signif_qval, file = paste(output_dir,"QTLs_identified_univariant_FDR.txt",sep=""),col.names=FALSE ,row.names=FALSE, append=TRUE)
	}
	count_vals=0
	print (length(signif_qval$qvals))
        if (dim(signif_qval)[1]==0)
        {
                cat ("!FDR_CORRECTON:			No QTLs detected by the method:",ii,"survived an FDR of ",sign_level_FDR,"%\n")
                cat ("!FDR_CORRECTON:			The lowest qval is: ",min(qvals),"\n")
	# In case we found some significant results
        }else{
                # ploting the LOD-distribution of the significant (FDR of chosen threshold%) QTLs 
                for (i in 1:length(signif_qval$qvals)){
			#print (i)
			#print (signif_qval)
                        chr=signif_qval[i,]$chr
                        pos=signif_qval[i,]$pos
                        lod=signif_qval[i,]$lod
                        pval=signif_qval[i,]$pval
                        name=toString(signif_qval[i,]$name)
                        qval=signif_qval[i,]$qvals
                        method=toString(signif_qval[i,]$method)

			print (method)
			
                        # LOD plot of the significant results
                        pdf(paste(plot_folder, name,"_FDR_", method,".pdf", sep=""), height=12, width=17)
                        mapped_data.em <- scanone(cross_data,method=method,pheno.col=name,model="normal",n.cluster=Number_of_threads_eQTL_FDR)
                        plot(mapped_data.em,main=name)
                        marker=find.marker(cross_data,chr=chr, pos=pos)
                        mtext( paste("q-value (FDR ",sign_level_FDR,"%):",qval, "Marker: ",marker, "/ Used method:", method), cex=0.9, col="red")
                        dev.off()
			count_vals=count_vals+1
			print (count_vals)
			
                }
        }
}
#-/

