#%%%% HEAD %%%%#
#/-
# This script generates a file in the format needed for the eqtl R package (cross_file)

# Importing packages
import sys 

# Reading the atributes
file_cross = open("%s"%(sys.argv[1]),"a") # file with the prepared header line. The output is writen into this file
file_coverage = open("%s"%(sys.argv[2]),"rU")	# file with the coverge of miRNAs to the specific QX Line
file_genotype = open("%s"%(sys.argv[3]),"rU")	# Genotyped Lines (only lines, which are present in the analysis)

# Declaring the variables
lines_dict={} # key: line_name, value: [miRNA expression miRNA 1, miRNA2, ... , Marker value at marker 1, Marker 2,...]
a=1 
#-/



#%%%% BODY %%%%#
#/-
for miRNA_line in file_coverage:
	lines_line=miRNA_line.split("\t")
	if a==1:
		ref_seq=lines_line		
		a=a+1
	else:
		lines_line = lines_line[1:]	
		while '' in ref_seq:
			ref_seq.remove('')
		while '\n' in ref_seq:
                          ref_seq.remove('\n')
		while ' ' in ref_seq:
                          ref_seq.remove(' ')
		for num,expression in enumerate(lines_line):
			num=num%len(ref_seq)
			try:
				expression = float(expression.replace("\n",""))
			except ValueError:
				expression = expression 
				if expression == "\n":
					continue
			if lines_dict.has_key(ref_seq[num].replace("\n","")):
				tmp=lines_dict[ref_seq[num].replace("\n","")]
				tmp.append(expression)	
				lines_dict[ref_seq[num].replace("\n","")]=tmp
			else:
				lines_dict[ref_seq[num].replace("\n","")]=[expression]	
		#	if ref_seq[num].replace("\n","")=='X215':
		#		print ("X215",len(lines_dict[ref_seq[num].replace("\n","")]))
		#		print (lines_dict[ref_seq[num].replace("\n","")])
		#		print (num,len(ref_seq))
		#		print (len(lines_line))
		#		print (lines_line)
		#	if ref_seq[num].replace("\n","")=='X217':
		#		print ("X217",len(lines_dict[ref_seq[num].replace("\n","")]))
		#		print (lines_dict[ref_seq[num].replace("\n","")])
		#		print (num,len(ref_seq))
		#		print (len(lines_line))
		#idf a==3:
		#	break
		a=a+1


a=0
#print(len(lines_dict.keys()))
#print (len(lines_dict['X215']))
#print (len(set(lines_dict['X215'])))
#print (len(lines_dict['X217']))
#print (len(lines_dict['X210']))
m=0

for Line_line in file_genotype:
#	print (m)
	Line=Line_line.split("\t")[0]	
	line_of_Line=Line_line.split("\t")[1:]
	if a==0:
		a=a+1
		continue
	else:
		tmp=lines_dict[Line]
		tmp=tmp+line_of_Line
		
		lines_dict[Line]=tmp
	m=m+1
		
print (lines_dict.keys())
#for k in lines_dict.keys():
#	if not k.startswith('QX'):
##		if not k.startswith('N2'): 
##			if not k.startswith('HW'):
#		lines_dict.pop(k)

for RIL_Line in lines_dict.keys():
	to_print = '\t'.join(str(n) for n in lines_dict[RIL_Line])
	file_cross.write("%s\t%s\n"%(RIL_Line,to_print))	
	
file_cross.close()
#-/
