#%%%% HEAD %%%%#
#/-
#%% This script reads the cross_file or the cross_object, does all the necessary proprocessing caclulations and hands out a ready-to-use cross_data R-Object

# loading libraries
suppressMessages(library(eqtl))
suppressMessages(library(snow))

# reading the command line arguments
args <- commandArgs(TRUE)
cross_file <- args[1]
RIL_used <- args[2]
step_v<-as.numeric(args[3])
error_prob_v<-as.numeric(args[4])
cross_data_object_ready<-as.numeric(args[5])
cross_data_object<-args[6]
use_variable<-args[7]
map_function_v<-args[8]
stepwidth_v<-args[9]
n_draws_v<-as.numeric(args[10])
tmp_folder_path<-args[11]
Clustering_used<-as.numeric(args[12])
Number_of_threads_eQTL_preload<-as.numeric(args[13])


# reading the data
###########################
# reading the cross_file
# In case the markers were previously clustered, the R object is loaded, which is faster, than reading a table
if (cross_data_object_ready == 0)
{
        if (Clustering_used == 1)
        {
                cat ("Clustering has been used. Loading the following cross_file Object: ",paste(tmp_folder_path,"cross_file_clustered_markers.R",sep=""), "\n")
                load(paste(tmp_folder_path,"cross_file_clustered_markers.R",sep=""))
        }else{
                cross_data <-read.cross("csv", dir="", file=cross_file, sep="\t", estimate.map=FALSE);
                # checking for monomorphic markers
                if (RIL_used==1)
                {
                        cross_data <- convert2riself(cross_data)

                        # Also any monomorphic markers are ommited (All of the samples have the same allel on at least one of the markers), as they produce a singularity matrix
                        print ("!1")
                        if ((any(geno.table(cross_data)$BB==0)) || (any(geno.table(cross_data)$AA==0)) )
                        {
                                print ("!eQTL mapping: There were some monomorphic markers. They will be removed from the dataset. Removing markers:")
                                dropNames=rownames(geno.table(cross_data)[which (geno.table(cross_data)$AA==0 | geno.table(cross_data)$BB==0),])
                                print (geno.table(cross_data)[dropNames,])
                                cross_data=drop.markers(cross_data, dropNames)
                        }
                }else{
                        if ((any(geno.table(cross_data)$BB==0)) || (any(geno.table(cross_data)$AA==0)) || (any(geno.table(cross_data)$AB==0)))
                        {
                                print ("!eQTL mapping: There were some monomorphic markers. They will be removed from the dataset. Removing markers:")
                                dropNames=rownames(geno.table(cross_data)[which ((geno.table(cross_data)$AA==0 | geno.table(cross_data)$BB==0 | geno.table(cross_data)$AB==0) & (geno.table(cross_data)$chr)!="X"),])
                                print ("Number of markers to remove:")
				print (length(dropNames))
                                print ("Removing following markers:")
                                print (geno.table(cross_data)[dropNames,])
                                # specific for the X chr
                                print ("!eQTL mapping: Removing Monomorphic markers on X chr. ")
				dropNamesX=rownames(geno.table(cross_data)[which ((geno.table(cross_data)$chr)=="X" & geno.table(cross_data)$ABf==0 & geno.table(cross_data)$ABr==0 & geno.table(cross_data)$AB==0),])

                                print ("Number of removed markers on the X chr.")
                                print (length(dropNamesX))
                                print ("Removing following markers:")
                                print (geno.table(cross_data)[dropNamesX,])
                                dropNames=c(dropNames,dropNamesX)
                                cross_data=drop.markers(cross_data, dropNames)

                        }
                }
		# the est.map's (function) multithreading with n.cluster seems not to work properly. Here is a small "by hand" walkaround
#		library(snow) 
#		cl <- makeCluster(Number_of_threads_eQTL_preload)  # set up 8 node cluster 
#		clusterSetupRNG(cl)  # set up random number generator 
#		clusterEvalQ(cl, require(qtl, quietly=TRUE))  # each cluster needs to load the R/qtl package
		
		cat ("Starting to calculate the genetic map. Used number of cores:",Number_of_threads_eQTL_preload,"\n")
                newmap<-est.map(cross_data, verbose=TRUE, n.cluster=Number_of_threads_eQTL_preload, omit.noninformative=TRUE)
                cross_data <- replace.map(cross_data, newmap)
        }
}else{
        cat ("An already clustered cross_file R-Object has been supplied. Loading: ",cross_data_object, "\n")
        bar <- load(cross_data_object)
        cross_data <- get(bar)
        if (RIL_used==1)
        {
                cross_data <- convert2riself(cross_data)
        }

        ###################
        # Block for the Rockman stuff
        cross_data = cleanphe(cross_data, string="OldRilNumber")
        cross_data = cleanphe(cross_data, string="RILletter")
        cross_data = cleanphe(cross_data, string="QXstrain")
        cross_data = cleanphe(cross_data, string="V15892")
        ###################
}
# converting the data to a special format in case it is a RIL dataset
print (cross_data)

#-/
#%%%% BODY %%%%#
#/-
# generating the pseudomarkers with their conditional probabilities. Check the description of the functions to be sure they do what you indend them to do
# calc.genoprob(): calculates the conditional probabilities for genotypes of pseudomarkers. (required my em and ehk methods)
# sim.geno(): simulates genotypes at the pseudomark positions from the joint distribution of the markers (required by the multiple imputation method)
if (cross_data_object_ready == 0)
{
        print ("calc sim calc stuff")
        if (use_variable == "use_all_variable") {
                qtl_method_to_use= c("em", "ehk", "imp")
                print ("calc calc.genoprob")
                cross_data <- calc.genoprob(cross_data, step=step_v, off.end=0, error.prob=error_prob_v, map.function=map_function_v, stepwidth=stepwidth_v);
                print ("calc sim.geno")
                cross_data <- sim.geno(cross_data, step=step_v, off.end=0, error.prob=error_prob_v, n.draws=n_draws_v, map.function=map_function_v, stepwidth=stepwidth_v);
        }else if (use_variable=="imp"){
                print ("calc sim.geno")
                cross_data <- sim.geno(cross_data, step=step_v, off.end=0, error.prob=error_prob_v, n.draws=n_draws_v, map.function=map_function_v, stepwidth=stepwidth_v);
                qtl_method_to_use="imp"
        }else {
                print ("calc calc.genoprob .")
                cross_data <- calc.genoprob(cross_data, step=step_v, off.end=0, error.prob=error_prob_v, map.function=map_function_v, stepwidth=stepwidth_v);
                qtl_method_to_use=use_variable
        }
        print (cross_data)
}else{
        if (use_variable == "use_all_variable") {
                qtl_method_to_use= c("em", "ehk", "imp")
        }else {
                qtl_method_to_use=use_variable
        }

}
# countig the number of genes
print ("writing gene_number_file.txt")
write.table(paste("gene_number=",length(phenames(cross_data)),sep=""), file = paste(tmp_folder_path, "gene_number_file.txt", sep=""), row.names=FALSE)

# saving the cross_file as an R object for a faster loading time during the next steps.
save(cross_data, file=paste(tmp_folder_path,"cross_file_ready.R",sep=""))

