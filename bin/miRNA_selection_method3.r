# This script gets file with Marker values
# TODO: In case we have two equidistant markers the one with the highest value should be taken, but right now the first one (=> random) is taken. 

library(qtl)

args <- commandArgs(TRUE)
genotype_markers_file <- args[1] 
normalized_coverage_file <- args[2] 
markers_raw<-args[3]
gene_name_raw<-args[4]

marker <- unlist(strsplit(markers_raw, ","))[1] 

parent_type=c("","")
genotype_markers<-read.table(genotype_markers_file, header=T, row.names=1)
normalized_coverage=read.table(normalized_coverage_file, header=T,row.names=1)

#selecting the genes with the two different expression values across the samples
norm_cov_for_gene=normalized_coverage[grep(paste(gene_name_raw,"$",sep=""),rownames(normalized_coverage),ignore.case=TRUE),]
if (dim(norm_cov_for_gene)[1]==0) 
{
	norm_cov_for_gene=normalized_coverage[grep(paste(gene_name_raw,"",sep=""),rownames(normalized_coverage),ignore.case=TRUE),]
}
# selecting the Genotypes for all of the samples for the given gene
markers_genotype_list=genotype_markers[marker]

markers_genotype_list_with_gent=rbind(norm_cov_for_gene,t(markers_genotype_list))

s=sapply(names(norm_cov_for_gene), function(x) if(markers_genotype_list_with_gent[,x][3]=="A"){(markers_genotype_list_with_gent[,x][1])}else{(markers_genotype_list_with_gent[,x][2])})

cat(as.vector(s))




