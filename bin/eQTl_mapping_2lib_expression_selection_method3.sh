#!/bin/bash

#%%%% HEAD %%%%#
#/-
#%% In case two gene libraries are used for the eQTL - mapping part, the expression for each of the genes is selected using this script (method1)
#%%  Method 3: for each individual (sample), for each miRNA it is decided what expression value is taken (lib1 or lib2), depending on the value of the Marker nearest to the gene itself. (e.g. For individual 1 the gene 1 has the expression 10 in lib1 and 20 in lib2. The Marker nearest to the gene 1 has a genotype of the lib2, so gene 1 gets the expression of lib2 assigned to him)

# reading the variables
gene_lib=$1 
gene_lib2=$2  
normalized_coverage_file=$3
tmp_folder_path=$4
output_dir=$5
gene_annotations=$6
scripts_dir=$7
Marker_pos_variable=$8
#-/

#%%%% BODY %%%%#
#/-
# defining the names of the libraries
first_lib_grep=$(head -1 ${gene_lib} | sed 's/>//g' | awk -F "[-_]" '{print $1}')
second_lib_grep=$(head -1 ${gene_lib2} | sed 's/>//g' | awk -F "[-_]" '{print $1}')

# selecting the gene names for the following selection of the expression 
awk '(NR > 1){print $1}' $normalized_coverage_file | grep $first_lib_grep | sed -r "s/$first_lib_grep[_-]//g" >  ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt
awk '(NR > 1){print $1}' $normalized_coverage_file | grep $second_lib_grep | sed -r "s/$second_lib_grep[_-]//g" >  ${tmp_folder_path}/short_gene_names_$second_lib_grep.txt
awk '(NR > 1){print $1}' $normalized_coverage_file | grep $first_lib_grep >  ${tmp_folder_path}/long_gene_names_$first_lib_grep.txt
awk '(NR > 1){print $1}' $normalized_coverage_file | grep -v -i -f ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt > ${tmp_folder_path}/unique_gene_names_$second_lib_grep.txt
echo "grep -w -v -i -f  ${tmp_folder_path}/short_gene_names_$second_lib_grep.txt ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt > ${tmp_folder_path}/unique_gene_names_$first_lib_grep.txt"
#echo $(grep -w -v -i -f  ${tmp_folder_path}/short_gene_names_$second_lib_grep.txt ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt)
grep -w -v -i -f  ${tmp_folder_path}/short_gene_names_$second_lib_grep.txt ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt > ${tmp_folder_path}/unique_gene_names_$first_lib_grep.txt
#cat ${tmp_folder_path}/unique_gene_names_$first_lib_grep.txt
#awk '(NR > 1){print $1}' $normalized_coverage_file | grep -v -i -f ${tmp_folder_path}/short_gene_names_$second_lib_grep.txt > ${tmp_folder_path}/unique_gene_names_$first_lib_grep.txt
cat ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt ${tmp_folder_path}/unique_gene_names_$second_lib_grep.txt > ${tmp_folder_path}/needed_gene_names_for_method3.txt

grep -i -w -f ${tmp_folder_path}/short_gene_names_$second_lib_grep.txt ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt | sed 's/[\t*]\n$/\t\n/g' > ${tmp_folder_path}/gene_names_${second_lib_grep}_${first_lib_grep}_pairs.txt

# selecting the names of the lines
awk '(NR==1){print $0}' $normalized_coverage_file >  ${tmp_folder_path}/line_names.txt


# adding to the output file the expression of these miRNAs
grep -i -f ${tmp_folder_path}/unique_gene_names_$first_lib_grep.txt $normalized_coverage_file -i |grep $first_lib_grep  > ${tmp_folder_path}/${first_lib_grep}_uniq_genes_expression.txt_tmp 

bn=$(basename ${normalized_coverage_file} .txt)
cat ${tmp_folder_path}/line_names.txt ${tmp_folder_path}/${first_lib_grep}_uniq_genes_expression.txt_tmp > ${output_dir}${bn}_expr_mth3.txt 
grep -i -f ${tmp_folder_path}/unique_gene_names_$second_lib_grep.txt $normalized_coverage_file -i |grep $first_lib_grep >> ${output_dir}${bn}_expr_mth3.txt 
      
for gene_pair_raw in $(cat ${tmp_folder_path}/gene_names_${second_lib_grep}_${first_lib_grep}_pairs.txt)
do   
	echo $gene_pair_raw
	gene_pair=$(echo $gene_pair_raw | sed 's/[-:+]/./g')
	echo -n -e $gene_pair_raw"\t" > ${tmp_folder_path}/gene_name

	# getting the nearest Marker to the current gene
	grep $gene_pair -i $gene_annotations |awk '{print $2"\t"$3"\t"$4}'|head -1 > ${tmp_folder_path}/gene_pos_tmp
	prox_marker=$(python ${scripts_dir}find_marker_prox_to_miRNA.py $Marker_pos_variable ${tmp_folder_path}/gene_pos_tmp)	

	right_line=$(echo $(Rscript ${scripts_dir}miRNA_selection_method3.r ${output_dir}genotype_marker.txt $normalized_coverage_file $prox_marker $gene_pair_raw)| sed 's/ /\t/g')
	echo $right_line
	# adding the line to the file
	echo -n -e $right_line"\n" >> ${tmp_folder_path}/gene_name     

	cat ${output_dir}${bn}_expr_mth3.txt ${tmp_folder_path}/gene_name > ${tmp_folder_path}/gene_expression_method3.txt_tmp
	mv ${tmp_folder_path}/gene_expression_method3.txt_tmp ${output_dir}${bn}_expr_mth3.txt
done

# removing empty lines just in case

sed '/^ *$/d' ${output_dir}${bn}_expr_mth3.txt | sed 's/ /\t/g' > ${tmp_folder_path}/${bn}_expr_mth3.txt_Noemptylines
mv ${tmp_folder_path}/${bn}_expr_mth3.txt_Noemptylines ${output_dir}${bn}_expr_mth3.txt


#### Exporting the a global variable from a child script 
rm -f ${tmp_folder_path}/normalized_coverage_file_for_eQTL_update.txt
echo "normalized_coverage_file_for_eQTL=${output_dir}${bn}_expr_mth3.txt" >> ${tmp_folder_path}/normalized_coverage_file_for_eQTL_update.txt

