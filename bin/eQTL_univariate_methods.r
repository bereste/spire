#%%%% HEAD %%%%#
#/-
#%% this script runs the eQTL mapping using the univariate methods from then QTL/R package (started from univariate_methods.sh)
#%% It recieves the file with miRNA expressions+Marker values, a file with Marker positions on the genome and a file with miRNA physical positions on the genome

 
# loading libraries
suppressMessages(library(eqtl))
suppressMessages(library(snow))

# reading the command line arguments
args <- commandArgs(TRUE)
marker_positions_file <- args[1]
plot_folder <- args[2]
Number_of_threads_eQTL<-as.numeric(args[3])
tmp_folder_path<-args[4]
sign_level<-as.numeric(args[5])
use_FDR<-as.numeric(args[6])
drop_v<-as.numeric(args[7])
first_number<-as.numeric(args[8])
second_number<-as.numeric(args[9])
use_variable<-args[10]
n_perm_v<-as.numeric(args[11])
LIST_of_gene_NAMES_file=args[12]



#loading the R object
load(paste(tmp_folder_path,"cross_file_ready.R",sep=""))


print (cross_data)
#reading Marker positions
marker_positions <- read.table(marker_positions_file ,header=TRUE,sep="\t");
##########################

if (use_variable == "use_all_variable") {
	qtl_method_to_use= c("em", "ehk", "imp")
}else {
	qtl_method_to_use=use_variable
}
#-/
#%%%% BODY %%%%#
#/-

# In case you only need specific genes to be tested
nphe_num=0
if (LIST_of_gene_NAMES_file != 0)
{
	print (LIST_of_gene_NAMES_file)
	LIST_of_gene_NAMES <- read.table(LIST_of_gene_NAMES_file)	
	names_to_remove=phenames(cross_data)
	for (g_name in LIST_of_gene_NAMES[,1][first_number:second_number])
	{
		names_to_remove=subset(names_to_remove,!(names_to_remove %in%  names_to_remove[grep(paste("*", g_name,sep=""), names_to_remove)]))
		print (g_name)
		print (length(names_to_remove))
	}

	##
	print ("using the phenotypes -1 (hmm)")
	nphe_num=1

}else{
	# removing the non-needed phenotypes
	# To remove phenotypes chuncks of 2000 names are created and then removed by the function as a regexp
	print (c(first_number,second_number))
	print (phenames(cross_data)[first_number:second_number])
	names_to_remove=subset(phenames(cross_data),!(phenames(cross_data) %in% phenames(cross_data)[first_number:second_number]))
	
}

chucks=c(seq(1,length(names_to_remove),by=2000),length(names_to_remove))
fir=1
a=""
for (chuck in c(chucks,length(names_to_remove))[2:tail(length(c(chucks,length(names_to_remove)))-1,1)])
{
	chuck=chuck-1
	print (c(fir,chuck))
	cross_data <-cleanphe(cross_data,paste(names_to_remove[fir:chuck],collapse="$|"))
	print (length(phenames(cross_data)))
	print (length(names_to_remove[fir:chuck]))
	fir=chuck+1
}
print(phenames(cross_data))
if (nphe_num==0)
{
	last_phe=nphe(cross_data)	
}else{
	last_phe=(nphe(cross_data)-1)
}
print (nphe(cross_data))
print (nphe(cross_data)-1)
#for (name_drop in names_to_remove){
#	cross_data = cleanphe(cross_data, string=name_drop)
#}

print (cross_data)

print ("eQTL_mapping.r:  running scannone function (Genome scan with a single QTL model)")
cat ("eQTL_mapping.r:  Using following methods: ",qtl_method_to_use ,"\n")

# Calculating the LOD score for each of the phenotypes / genes. Each of the genotypes is calculated independenly for further FDR correction (the p_values for each of the chromosomes are colected and saved)
first_check=1
#if (chrnames(cross_data) %in% "X"){last_phe=nphe(cross_data)}else{last_phe=nphe(cross_data)-2}
print (phenames(cross_data))
print (1:last_phe)
for(i in 1:last_phe)
{
	if (phenames(cross_data)[i]!="sex" & phenames(cross_data)[i]!="pgm" & is.numeric(cross_data$pheno[i][1,]))
	{
		# for each of the methods used
		for (ii in qtl_method_to_use) {
			# Running the eQTL mapping
			# scanone(): Genome scan with a single QTL model, the actual mapping function, returns a LOD cruve for each phenotype (x-axis: genomeposition, y-axis: LOD score)
			mapped_data.em <- scanone(cross_data,method=ii,pheno.col=i,model="normal",n.cluster=Number_of_threads_eQTL);
			# Running the eQTL mapping on permutated data for a p_value estimation
			mapped_data.em.perm <- scanone(cross_data,pheno.col=i, n.perm=n_perm_v, method=ii,n.cluster=Number_of_threads_eQTL);
			print (c(phenames(cross_data)[i], ii))
			print (summary(mapped_data.em, perms=mapped_data.em.perm, alpha=sign_level, pvalues=TRUE, format="onepheno", lodcolumn=1))
			QTL_results_map_all=summary(mapped_data.em, perms=mapped_data.em.perm, alpha=sign_level, pvalues=TRUE, format="onepheno", lodcolumn=1)
			# calculating the 1-LOD interval of the QTL (in case it is significant). It is printed into the output
			LOD_int_Markers="-"
			# in case there are QTLs found on more than one Chromosome there is the following for loop
			lod_int_matrix=matrix(nrow=2,ncol=dim(QTL_results_map_all)[1])
			for (results_line in 1:dim(QTL_results_map_all)[1])
			{
				QTL_results_map=QTL_results_map_all[results_line,]
				if (!is.na(QTL_results_map[1,]$pos))
				{
					LOD_sup_int <- lodint(mapped_data.em, chr=QTL_results_map$chr, drop=drop_v, expandtomarkers=TRUE)
					QTL_chr_uno=as.numeric(as.character(QTL_results_map$chr))
					lodint_miRNA <- rownames(LOD_sup_int)
					#print (QTL_results_map)
					# if the QTL Marker is a pseudomarker, we search for the nearest real marker. 
					# the 1-lod interval Markers are calculated according to the pseudomarker still
					a<-LOD_sup_int[2,]
					m<-find.marker(cross_data, chr=a$chr[1], pos=a$pos[1])
					m_t<-subset (marker_positions, Marker %in% m)
					if (dim(m_t)[1]==0)
					{
						m<-subset (marker_positions, PP %in% m)
					}else{
						m<-m_t
					}
					QTL_Markers<-subset (marker_positions, Marker %in% lodint_miRNA) # second marker: QTL Marker. first and the third markers: 1-lod intervl markers for the QTL
					if (dim(QTL_Markers)[1]==0){QTL_Markers<-subset (marker_positions, PP %in% lodint_miRNA)}
					if (length(QTL_Markers$Marker) == 2){QTL_Markers=rbind(QTL_Markers[1,],m,QTL_Markers[2,])}
					if (length(QTL_Markers$Marker) > 3){QTL_Markers=rbind(QTL_Markers[1,],QTL_Markers[2,],tail(QTL_Markers,1))}
					LOD_int_Markers=paste(as.character(QTL_Markers$Marker), collapse="_")
					lod_int_matrix[,results_line]=c(QTL_chr_uno,LOD_int_Markers)
					print (LOD_int_Markers)
				}
				# printing a plot of the LOD score
				pdf(paste(plot_folder, paste(phenames(cross_data)[i],ii, "pdf", sep="."), sep=""), height=12, width=17)
				plot(mapped_data.em,main=phenames(cross_data)[i])
				abline(h=summary(mapped_data.em.perm, alpha=sign_level)[1], col="red")
				chr=summary(mapped_data.em, perms=mapped_data.em.perm, alpha=sign_level, pvalues=TRUE, format="onepheno", lodcolumn=1)[1,]$chr
				pos=summary(mapped_data.em, perms=mapped_data.em.perm, alpha=sign_level, pvalues=TRUE, format="onepheno", lodcolumn=1)[1,]$pos
				# in case a marker over a threshold has been found the position is printed on the plot. 
				if (length(pos)!=0){
					marker=find.marker(cross_data,chr=chr, pos=pos)
					mtext( paste("p-value (significance level ",sign_level,"%):",summary(mapped_data.em.perm, alpha=sign_level)[1], "Marker: ",marker, "/ Used method:", ii), cex=0.9, col="red")
				}else{
					mtext( paste("p-value (significance level ",sign_level,"%):",summary(mapped_data.em.perm, alpha=sign_level)[1], "/ Used method:", ii), cex=0.9, col="red")
				}
					
				
				dev.off()
		
			}	
				## gathering the p-values for the FDR (BH) adjustment. (1 max peak per chr per gene)
#				if (dim(lod_int_matrix)[2]==0){for_the_loop=1}else{for_the_loop=dim(lod_int_matrix)[2]}
				if (use_FDR ==1)
				{              
					name=phenames(cross_data)[i]
					method=ii
					# collecting the p_vals
					new_pvals=subset(summary(mapped_data.em, perms=mapped_data.em.perm, pvalues=TRUE, format="onepheno", lodcolumn=1),!duplicated(chr))     
					best_line=seq(along=new_pvals$pval)[new_pvals$pval == min(new_pvals$pval)]
					best=new_pvals[best_line,][1,]
					colnames(best)=c("chr","pos","score","pval")
					# the in lod_int_matrix all of the results are saved, in case we have more than one "best" QTLs. The following line can help you extract thoese if needed
					
					if (LOD_int_Markers!="-")
					{
						confInt=lod_int_matrix[,which (lod_int_matrix[1,]==best$chr)][2]
					}else
					{
						confInt=LOD_int_Markers
					}
					if (first_check==1){
						all_pvals=best
						all_pvals=cbind(all_pvals, name)
						all_pvals=cbind(all_pvals, method)
						all_pvals=cbind(all_pvals, confInt)
						first_check=0
					}else{
						best=cbind(best, name)
						best=cbind(best, method)
						best=cbind(best, confInt)
						all_pvals=rbind(all_pvals, best)
					}
					# writing the p_values
					write.table(all_pvals, file = paste(tmp_folder_path, "eQTL_univariate_results_pvalues", first_number,"_" ,second_number,".txt", sep=""), row.names=FALSE)
				}
			

        	}
	}
}

#-/

