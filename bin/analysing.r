#%%%% HEAD %%%%#
#/-
#%% This R script does a further analysis for of the rna-seq miRNA expression datarna-seq  

# Loading necessary libraries
suppressMessages(library(BiocGenerics))
suppressMessages(library(Biobase))
suppressMessages(library(affy))
suppressMessages(library(gplots))

# Reading comand line variables 
args <- commandArgs(TRUE)
data_norm_location <- args[1]
name_dif <- args[2]
dual_mod <- as.numeric(args[3])
heatmap_factor <- args[4]
output_dir <- args[5]
plot_dir <- args[6]
no_cons <- as.numeric(args[7])
ident_miRNAs <- args[8]
not_ident_miRNAs <- args[9]
uniq_names <- args[10]
miRNA_conservations <- args[11]
cons_code <- args[12]
#-/

if (no_cons==0)
{
	suppressMessages(library(ggplot2))
	suppressMessages(library(car))
	library(RColorBrewer)
}

# -------------------
# function, which greps a miRNA name in a given set
grep_name <- function(miRNA_name, data)
{

	pos<-grep(paste(miRNA_name," *$",sep=""),data,ignore.case=TRUE)
	if (identical(pos, integer(0))){pos<-grep(miRNA_name,data,ignore.case=TRUE)}
	if (identical(pos, integer(0))){pos<-NA}
	return(pos)
}
# -------------------
# -------------------
# function, which processes a conservation file. (Given 5prime and 3prime conservations, it combines them into one)
conservations_merge <- function(gene_conservations)
{
	gene_conservations_merged=matrix(ncol=2,nrow=dim(gene_conservations)[1])		
	for (line in seq(1,length(gene_conservations[,1])))
	{
		# unlisting the vectors
		unls_five=unlist(strsplit(as.character(gene_conservations[line,])[2], split=""))	
		unls_three=unlist(strsplit(as.character(gene_conservations[line,])[3], split=""))
	
		# In case there are some not filled out positions (they are filled out with " ")
		max_len=max(length(unls_five),length(unls_three))
		unls_three=c(unls_three, rep(" ",max_len-length(unls_three)))
		unls_five=c(unls_five, rep(" ",max_len-length(unls_five)))
		
		a=1
		cons_all=vector()
		# compare 5prime and 3prime
		for (i in unls_five)	
		{
			if (unls_three[a] == "+" || unls_five[a] == "+"){cons="+"}else{cons="-"}
			cons_all=paste(cons_all,cons, sep="")
			a=a+1
		}
		gene_conservations_merged[line,][2]=cons_all	
		gene_conservations_merged[line,][1]=as.character(gene_conservations[line,][1])		
		
		
	}
	return(gene_conservations_merged)
}
# -------------------
# -------------------
# function, which gets two verctors of characters element [+-], as well as a cosercation coding and returs back a name representing a conservation category
# can olso handle just one vector given (NOTE: This function is actually not used in the code)
conservation_check <- function(cons_code_tab, five_prime, three_prime)
{

	# in case of just one supplied vector
	if (missing(three_prime)){three_prime=five_prime} 
	
	# unlisting the vectors
	unls_five=unlist(strsplit(as.character(five_prime), split=""))
	unls_three=unlist(strsplit(as.character(three_prime), split=""))

	# In case there are some not filled out positions they are filled out with " "
	max_len=max(length(unls_five),length(unls_three))
	unls_three=c(unls_three, rep(" ",max_len-length(unls_three)))
	unls_five=c(unls_five, rep(" ",max_len-length(unls_five)))
	
	a=1
	cons_all=vector()
	# compare 5prime and 3prime
	for (i in unls_five)	
	{
		if (unls_three[a] == "+" || unls_five[a] == "+"){cons="+"}else{cons="-"}
		cons_all=paste(cons_all,cons, sep="")
		a=a+1
	}
	
	cons_cat="No category"
	# for each conservation category
	for (n in names(cons_code_tab))
	{
		cons_vals=strsplit(as.character(cons_code_tab[n]), split="/")[[1]]
		# for each of the potential profiles
		for (nn in cons_vals)
		{
			if (nn==cons_all)
			{
				cons_cat=n		
			}
		}
	}

	return (cons_cat)
}
# -------------------

#%%%% BODY %%%%#
#/-
### Reading the files
data <- read.table(data_norm_location, header=T)
data_log <- log(data)
if (no_cons==0)
{
	miRNA_cons <- read.table(miRNA_conservations, header=T, colClasses = "character")
}

## miRNAs are ordered accorduing to their standart deviation
cat (paste("            (",name_dif,"): plotting miRNAs in the order of their standart deviation (Log scale) \n", sep=""))
pdf(paste(plot_dir,"sd_plot_miRNAs_",name_dif, ".pdf", sep=""),height=12, width=17)
par(las=2)
barplot(log(sort(apply(t(data_log), 2, sd))),cex.names=0.25)
invisible(dev.off())

cat (paste("            (",name_dif,"): plotting miRNAs (descr here) \n", sep=""))
pdf(paste(plot_dir, "sd_div_mean_plot_miRNAs_log_", name_dif, ".pdf", sep=""), height=12, width=17)
par(las=2)
barplot(sort(apply(t(data_log), 2, sd)/colMeans(t(data_log))),cex.names=0.2, main = "Standard deviation of miRNA expression divided by the Mean of the expression of all samples")
invisible(dev.off())

cat (paste("            (",name_dif,"): plotting miRNAs (descr here) \n", sep=""))
pdf(paste(plot_dir, "sd_AND_mean_plot_miRNAs_log_", name_dif, ".pdf", sep=""), height=12, width=17)
par(las=2)
barplot(colMeans(t(data_log)),cex.names=0.2, main = "Standard deviation of miRNA expression divided by the Mean of the expression of all samples (log data)", ylim=c((min(data_log)),max(data_log)), col="blue")
par(new=TRUE, lwd=0.05)
a<-barplot(apply(t(data_log), 2, sd), axisnames = FALSE, col="red", ylim=c((min(data_log)),max(data_log)))
legend("topright", title="Legend", pch=16, col=c("red", "blue"), legend=c("SD", "Mean"), ncol=2)
invisible(dev.off())


## heatmaps
#cat (paste("            (",name_dif,"): Producing a heatmap of the expression data \n", sep=""))
#data_matrix <- as.matrix(data_log)
#pdf(paste(plot_dir, "heatmap_miRNAs_expression_", name_dif, ".pdf", sep=""),height=15, width=17)
#heatmap.2(data_matrix, col=colorRampPalette(c("white","green","green4","violet","purple"))(100), scale="none",key=TRUE, symkey=FALSE, density.info="density", trace="column",  cexRow=heatmap_factor)
#invisible(dev.off())


## Density plot of the expression of the genes depending of the conservation of the genes (only if the user so desires)
if (no_cons==0)
{
	cat (paste("            (",name_dif,"): Density plot of the expression of the genes depending of the conservation of the genes \n", sep=""))
	data_mean <- colMeans(t(data))	
	
	# Checking if 5prime and 3prime conservations are present. If yes, merge them
	if (dim(miRNA_cons)[2]==3)
	{
		miRNA_cons_ready=conservations_merge(miRNA_cons)
	}else{
		miRNA_cons_ready=miRNA_cons
	}


	# Reading the table with conservation code, defining the conservation groups
	cons_code_tab <- read.table(cons_code, header=T, sep="\t", colClasses = "character")

	# Creating color schema
	myColors <- brewer.pal(length(cons_code_tab),"Set1")
	names(myColors) <- names(cons_code_tab)

	# ploting the initial 
	pdf(paste(plot_dir, "Gene_conservations_", name_dif, ".pdf", sep=""),height=15, width=17)
	plot(y=rep(1,length(data_mean)),x=log(data_mean), ylim=c(-3,10), t="n")

	# adding a density plot	
	for (cat_name in names(cons_code_tab))
	{
		cons_vals=strsplit(as.character(cons_code_tab[cat_name]), split="/")[[1]]	
		genes_of_the_group=subset(miRNA_cons_ready, miRNA_cons_ready[,2]%in%cons_vals)[,1]
                color_to_use=myColors[cat_name]
		points(log(data_mean[genes_of_the_group]),rep(0,length(data_mean[genes_of_the_group])), col=color_to_use, cex=0.8)
			
		dens=density(as.numeric(log(data_mean[genes_of_the_group])), na.rm=T)
		points(dens$x,dens$y*5, type="l", col=color_to_use, lwd=4)
		
	}
	cons_leg=names(miRNA_cons_ready)[2]
	legend("bottom", title=paste("Gene age (conservations in ",cons_leg,")",sep=""), pch="-", col=myColors, legend=names(myColors), ncol=2, cex=1.5, lwd=3)


	invisible(dev.off())
}


#	genes_with_conservation=matrix(ncol=2,nrow=length(data_mean))
#	genes_with_conservation[,1]=data_mean
#	rownames(genes_with_conservation)=names(data_mean)
#	colnames(genes_with_conservation)=c("Expression", "Conservation")
#
#	# Reading the table with conservation code, defining the conservation groups
#	cons_code_tab <- read.table(cons_code, header=T, sep="\t", colClasses = "character")
#	
#	# Creating color schema
#	myColors <- brewer.pal(length(cons_code_tab),"Set1")
#	names(myColors) <- names(cons_code_tab)
#
#
#	
#	# looking up the conservation for each of the genes and applying the appropriate color
#	for (gene_name in names(data_mean))
#	{
#		cat(gene_name,"\n")
#		# checking if the gene conservations have 5prime and 3prime conservations or just the 5prime
#		if (length(miRNA_cons)==3)
#		{
#			f_p<-subset(miRNA_cons, miRNA_cons[,1]==gene_name)[2]	
#			t_p<-subset(miRNA_cons, miRNA_cons[,1]==gene_name)[3]
#			cons_cat=conservation_check(cons_code_tab,f_p, t_p)				
#		}else{
#			f_p<-subset(miRNA_cons, miRNA_cons[,1]==gene_name)[2]	
#			cons_cat=conservation_checkfunction(cons_code_tab,f_p)				
#		}
#		
#		color_to_use=myColors[cons_cat]
#		points(log(data_mean[gene_name]),0, col=color_to_use, cex=0.8)
#
#		genes_with_conservation[gene_name,][2]=cons_cat
#	}
#
#	# adding a density plot	
#	for (cat_name in names(cons_code_tab))
#	{
#		cat_subset=subset(genes_with_conservation, (genes_with_conservation[,"Conservation"]==cat_name))[,1]
#		dens=density(as.numeric(cat_subset), na.rm=T)
#		color_to_use=myColors[cat_name]
#		points(dens$x,dens$y*5, type="l", col=color_to_use, lwd=4)
#		
#	}
#	legend("bottom", title="miRNA age (conservations in Nematoda/Arthropoda/Lophotrochozoa/Vertebrata)", pch="-", col=myColors, legend=names(myColors), ncol=2, cex=1.5, lwd=2)





# The following is only applied in case one gene library is used
if (dual_mod==0){
	## SD vs Mean plot
	mean_all <- colMeans(t(data_log))
	sd_all <- apply(t(data_log),2,sd)

	cat (paste("            (",name_dif,"): ploting an SD vs Mean plot of miRNA expression (with log data)\n", sep=""))
	pdf(paste(plot_dir,"sd_vs_mean_plot_miRNAs_expression_", name_dif, ".pdf", sep=""),height=12, width=17)
	plot (mean_all,sd_all, xlab="Mean", ylab="SD", col="red", xlim=c(min(mean_all),max(mean_all)), ylim=c(min(sd_all), max(sd_all)))
	abline(lm(apply(t(data_log),2,sd)~colMeans(t(data_log))))
	legend("topright",c("Genes of intereset"),col=c("red"), pch=1)
	text(mean_all,sd_all, labels=rownames(data_log), pos=3, cex=0.7)
	invisible(dev.off())


# The following can only be applied in case of the dual library mode
}else{
	## SD vs Mean plot
	# Preparation
	mean_all <- colMeans(t(data_log))
	sd_all <- apply(t(data_log),2,sd)
	uniq_genes <- read.table(uniq_names)
	ident <- read.table(ident_miRNAs)
	not_ident <- read.table(not_ident_miRNAs)
	lib1_name = strsplit(name_dif, "_")[[1]][1]
	lib2_name = strsplit(name_dif, "_")[[1]][2]

	# Calculations
	pos_uniq = sapply(X=uniq_genes[,1],FUN=grep_name, names(mean_all))
	pos_ident = sapply(X=ident[,1],FUN=grep_name, names(mean_all))
	pos_NOTident = sapply(X=not_ident[,1],FUN=grep_name, names(mean_all))
	lib1_pos_NOT_ident = pos_NOTident[grep (lib1_name,names(mean_all[pos_NOTident]))]
	lib2_pos_NOT_ident = pos_NOTident[grep (lib2_name,names(mean_all[pos_NOTident]))]

	# Ploting
	cat (paste("            (",name_dif,"): ploting an sd vs mean plot of miRNA expression (with log data)\n", sep=""))
	pdf(paste(plot_dir, "sd_vs_mean_plot_miRNAs_expression_", name_dif, ".pdf", sep=""),height=12, width=17)
	plot (mean_all[pos_ident],sd_all[pos_ident], xlab="Mean", ylab="SD", col="red", xlim=c(min(mean_all),max(mean_all)), ylim=c(min(sd_all), max(sd_all)))
	points(mean_all[pos_NOTident],sd_all[pos_NOTident], col="blue")
	abline(lm(apply(t(data_log),2,sd)~colMeans(t(data_log))))
	text(mean_all,sd_all, labels=rownames(data_log), pos=3, cex=0.7)
	invisible(dev.off())


	## Doing another plot (highlightning the different sets in the set on non-identical genes (lib1/lib2))
	cat (paste("            (",name_dif,"): ploting an sd vs mean plot of miRNA expression (with log data), highlighting identical/non-identical genes between two libraries\n", sep=""))
	pdf(paste(plot_dir, "sd_vs_mean_plot_miRNAs_expression_2_", name_dif, ".pdf", sep=""),height=12, width=17)
	plot (mean_all[pos_ident],sd_all[pos_ident], xlab="Mean", ylab="SD", col="lightgrey", xlim=c(min(mean_all),max(mean_all)), ylim=c(min(sd_all), max(sd_all)))
	if (!identical(lib2_pos_NOT_ident, integer(0)))
	{	
		lib2_check=1
		points(mean_all[lib2_pos_NOT_ident],sd_all[lib2_pos_NOT_ident], col="blue", lwd=2)
		text(mean_all[lib2_pos_NOT_ident],sd_all[lib2_pos_NOT_ident], labels=rownames(data_log)[lib2_pos_NOT_ident], pos=1, cex=0.5)
	}
	if (!identical(lib1_pos_NOT_ident, integer(0)))
	{ 
		lib1_check=1
		points(mean_all[lib1_pos_NOT_ident],sd_all[lib1_pos_NOT_ident], col="red", lwd=2)
		text(mean_all[lib1_pos_NOT_ident],sd_all[lib1_pos_NOT_ident], labels=rownames(data_log)[lib1_pos_NOT_ident], pos=3, cex=0.5)
	}

	abline(lm(apply(t(data_log),2,sd)~colMeans(t(data_log))))
		# checking if we are dealing with the second library (HW), in which case we do slightely different plot
	legend("topright",c("Identical genes between the two libraries","Non identical genes (top library)","Non identical miRNAs (bottom library)"),col=c("lightgrey","red","blue"), pch=1)

	dev.off()



	## MA plot 
	# Preparation
	mean_ma<-colMeans(t(data))
	matrix_for_ma_notIdent=matrix(mean_ma[pos_NOTident],ncol=2,byrow=T)
	labels_notIdent=gsub(paste(lib1_name,"_",sep=""),"",gsub(paste(lib1_name,"-",sep=""),"",names(mean_ma[pos_NOTident[1,]])))
	M_notIdent=log2(matrix_for_ma_notIdent[, 1])-log2(matrix_for_ma_notIdent[, 2])
	A_notIdent=rowMeans(log2(matrix_for_ma_notIdent))

	matrix_for_ma_Ident=matrix(mean_ma[pos_ident],ncol=2,byrow=T)
	labels_Ident=gsub(paste(lib1_name,"_",sep=""),"",gsub(paste(lib1_name,"_",sep=""),"",names(mean_ma[pos_ident[1,]])))
	M_Ident=log2(matrix_for_ma_Ident[, 1])-log2(matrix_for_ma_Ident[, 2])
	A_Ident=rowMeans(log2(matrix_for_ma_Ident))

	matrix_all_not_log=rbind(matrix_for_ma_Ident,matrix_for_ma_notIdent)
	A_all=c(A_Ident,A_notIdent)
	M_all=c(M_Ident,M_notIdent)
	labels_all=c(labels_Ident,labels_notIdent)

	# Ploting
	cat (paste("            (",name_dif,"): Ploting an MA-Plot\n", sep=""))
	pdf(paste(plot_dir,"MA_plot_", name_dif, ".pdf", sep=""),height=12, width=17)
	ma.plot(A_all, M_all, cex=2, pch=NA,cex.axis=1.4,cex.lab=1.5, ylim=c(-2.5,4))
	points(A_all[seq(1,length(A_Ident))],M_all[seq(1,length(M_Ident))], col="black", cex=1.4, pch=4)
	points(A_all[seq((length(A_Ident)+1), (length(A_Ident)+length(A_notIdent)))],M_all[seq((length(M_Ident)+1), (length(M_Ident)+length(M_notIdent)))], col="darkred", cex=1.4)
	text(A_all[which (abs(M_all) >= sd(M_all)/2)], M_all[which (abs(M_all) >= sd(M_all)/2)], labels=labels_all[which (abs(M_all) >= sd(M_all)/2)],pos=1, cex=1.3)
	legend("top", pch=c(4,1), legend=c("miRNAs with identical sequence (lib1 and lib2)", "miRNAs with non-identical sequence (lib1 and lib2)"), ncol=2, cex=1.5)
	invisible(dev.off())
		







#		# MA plot with different colors depending on the conservation of the miRNA
#		pdf(paste(plot_dir, "MA_conserved_", name_dif, ".pdf", sep=""),height=12, width=17)
#		ma.plot(A_all, M_all, cex=2, pch=NA, loess.col="black", cex.axis=1.4,cex.lab=1.5)
#		a=1
#		for (point in labels_all)
#		{
#			point_pos=grep(paste(point,"$",sep=""),miRNA_cons$V1,ignore.case=TRUE)
#			color_code=miRNA_cons[point_pos, ]$V2
#			color=recode(color_code, "'1' = 'red';'2' = 'blue';'3' = 'green';'4' = 'black';else = 'yellow'")
#			color=paste(color)
#			points(A_all[a],M_all[a], col=color, cex=0.8)
#			print (point)
#			print (color)
#			print (c(A_all[a],M_all[a]))
#			
#			a=a+1
#		}	
#		# Adding a density line for each of the colors
#
#		
#
#		for (num in c(0,1,2,3,4))
#		{
#			num_miRNA=subset(miRNA_cons,V2 ==num)$V1
#			num_miRNA=gsub("novel-","",gsub("cel_","",gsub("cel-","",num_miRNA)))
#
#			color=recode(num, "'1' = 'red';'2' = 'blue';'3' = 'darkgrey';'4' = 'black';else = 'greenyellow'")
#			color=paste(color)
#			pos_names=unlist(sapply(X=num_miRNA,FUN=grep_name, labels_all))
#			A_for_num=A_all[pos_names]
#			M_for_num=M_all[pos_names]
#			points(A_for_num,M_for_num, col=color, cex=1)
#			dens=density(A_for_num, na.rm=T)
#			points(dens$x,dens$y*30, type="l", col=color, lwd=4)
#			cat ("could not find:",names(pos_names[which(is.na(pos_names))]),"\n")
#			
#		}
#
#		#text(A_all[which (abs(M_all) >= sd(M_all)/2)], M_all[which (abs(M_all) >= sd(M_all)/2)], labels=labels_all[which (abs(M_all) >= sd(M_all)/2)],pos=1, cex=0.6)
#		legend("bottom", title="miRNA age (conservations in Nematoda/Arthropoda/Lophotrochozoa/Vertebrata)", pch="-", col=c("greenyellow", "red","blue","darkgrey","black"), legend=c("C.Elegans only(-/-/-/-)", "Very young(+/-/-/-, -/+/-/-)", "Young(+/+/-/-)","Middle","Old(+/+/+/+)"), ncol=2, cex=2.4, lwd=3)
#		dev.off()
#
#		# generating an conserved MA plot with modified level 3 (no ---+)	
#		pdf(paste("MA_conserved_edit", name_dif, "pdf", sep="."),height=12, width=17)
#		ma.plot(A_all, M_all, cex=2, pch=NA, loess.col="black", cex.axis=1.4,cex.lab=1.5)
#		# Adding a density line for each of the colors
#		for (num in c(0,1,2,3,4,5))
#		{
#			num_miRNA=subset(miRNA_cons_all_edited,V2 ==num)$V1
#			num_miRNA=gsub("novel-","",gsub("cel_","",gsub("cel-","",num_miRNA)))
#
#			color=recode(num, "'1' = 'red';'2' = 'blue';'3' = 'darkgrey';'4' = 'black';'5'='magenta';else = 'greenyellow'")
#			color=paste(color)
#			pos_names=unlist(sapply(X=num_miRNA,FUN=grep_name, labels_all))
#			A_for_num=A_all[pos_names]
#			M_for_num=M_all[pos_names]
#			points(A_for_num,M_for_num, col=color, cex=1)
#			dens=density(A_for_num, na.rm=T)
#			points(dens$x,dens$y*30, type="l", col=color, lwd=4)
#			cat ("could not find:",names(pos_names[which(is.na(pos_names))]),"\n")
#			
#		}
#
#		#text(A_all[which (abs(M_all) >= sd(M_all)/2)], M_all[which (abs(M_all) >= sd(M_all)/2)], labels=labels_all[which (abs(M_all) >= sd(M_all)/2)],pos=1, cex=0.6)
#		legend("bottom", title="miRNA age (conservations in Nematoda/Arthropoda/Lophotrochozoa/Vertebrata)", pch="-", col=c("greenyellow", "red","blue","darkgrey","black","magenta"), legend=c("C.Elegans only(-/-/-/-)", "Very young(+/-/-/-, -/+/-/-)", "Young(+/+/-/-)","Middle","Old(+/+/+/+)","Vertebrata only (-/-/-/+)"), ncol=2, cex=2.4, lwd=3)
#		dev.off()
#	
#	# getting level three
#	cat ("asdas\n")
#	num=3
#	num_miRNA=subset(miRNA_cons,V2 ==num)$V1
#	pdf(paste("MA_conserved_level_three", name_dif, "pdf", sep="."),height=12, width=17)
#	ma.plot(A_all, M_all, cex=1, pch=NA, add.loess=F)
#	color=c("red","yellow","blue","black")
#	a=1
#	print (table(apply(miRNA_cons_all[,2:5], 1, paste, collapse="")))
#	for (check in list(c("+","-","-","+"),c("+","+","+","-"),c("+","+","-","+"),c("-","-","-","+")))
#	{
#		l_three=miRNA_cons_all[miRNA_cons_all[,1] %in% num_miRNA,]
#		print (table(apply(l_three[,2:5], 1, paste, collapse="")))
#		names_l_three=subset(l_three, (V2==check[1] & V3==check[2] & V4==check[3] & V5==check[4]))$V1
#		names_l_three=gsub("novel-","",gsub("cel_","",gsub("cel-","",names_l_three)))
#		pos_names=unlist(sapply(X=names_l_three,FUN=grep_name, labels_all))
#		print (check)
#		print (names_l_three)
#		A_for_num=A_all[pos_names]
#		M_for_num=M_all[pos_names]
#		if (!is.null(pos_names))
#		{
#			if (!is.na(any(pos_names)))
#			{
#				if (length(A_for_num)>=2)
#				{
#					print (A_for_num)
#					print (color[a])
#					points(A_for_num,M_for_num, col=color, cex=1)
#					dens=density(A_for_num, na.rm=T)
#					points(dens$x,dens$y*30, type="l", col=color[a])
#					#text(mean(dens$x),mean(dens$y*100),labels=paste(color[a], check[1], check[2], check[3], check[4]),pos=1)
#				}
#			}
#		}
#		a=a+1
#		
#	}
#	legend("top", title="miRNA level of conservation(level 3) (Nematoda/Arthropoda/Lophotrochozoa/Vertebrata)", lwd=2, col=c("red","yellow","blue","black"), legend=c("+--+","+++-","++-+","---+"), ncol=2, cex=1)
#	dev.off()
}

