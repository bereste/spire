# Using the results from random Forest mapping this script performes a FDR correction over all Markers + all genes and plots the results
# NOTE: due to the preprocessing of the script before the values in the whole_data Object are as following:
#	- SF, P : without the NAs between the Chromosomes
#	- miRNA position (nearest marker): with NAs between the chromosomes.

# loading libraries
suppressMessages(library(randomForest))
suppressMessages(library(qtl))
suppressMessages(library(snow))
suppressMessages(library(car))

# reading the parameters
args <- commandArgs(TRUE)
plot_folder <- args[1]
tmp_folder_path <- args[2]
RIL_used<-as.numeric(args[3])
cross_file<-args[4]
Clustering_used<-as.numeric(args[5])
sign_level_FDR<-as.numeric(args[6])
output_folder<-args[7]
univar_run<- args[8]
marker_positions_file<-args[9]
markers_on_chromosomes<-unlist(strsplit(toString(args[10]),"_"))

print (markers_on_chromosomes)


#############################
# the cross_file is loaded and converted for the further use only in case: the cis/trans plot is needed.
if (univar_run == 0)
{
	if (Clustering_used == 1)
	{
		load(paste(tmp_folder_path,"cross_file_clustered_markers.R",sep=""))
	}else{
		cross_data <-read.cross("csv", dir="", file=cross_file, sep="\t");
	}
	print(cross_data)		
	# converting the data to a special format in case it is a RIL dataset
	if (RIL_used==1)
	{
		cross_data <- convert2riself(cross_data)

	        AA<-geno.table(cross_data)$AA
		BB<-geno.table(cross_data)$BB
		# Also any monomorphic markers are ommited (All of the samples have the same allel on at least one of the markers), as they produce a singularity matrix
		if ((TRUE %in% (geno.table(cross_data)$BB==0)) || (TRUE %in% (geno.table(cross_data)$AA==0)) )
		{
			print ("!eQTL mapping:                  There were some monomorphic markers. They will be removed from the dataset. Removing markers:")
			dropNames=rownames(geno.table(cross_data)[which (geno.table(cross_data)$AA==0 | geno.table(cross_data)$BB==0),])
			cross_data=drop.markers(cross_data, dropNames)
			print (geno.table(cross_data)[which (geno.table(cross_data)$AA==0 | geno.table(cross_data)$BB==0),])
		}
	}else{
		if ((any(geno.table(cross_data)$BB==0)) || (any(geno.table(cross_data)$AA==0)) || (any(geno.table(cross_data)$AB==0)))
		{
			print ("!eQTL mapping: There were some monomorphic markers. They will be removed from the dataset. Removing markers:")
			dropNames=rownames(geno.table(cross_data)[which ((geno.table(cross_data)$AA==0 | geno.table(cross_data)$BB==0 | geno.table(cross_data)$AB==0) & (geno.table(cross_data)$chr)!="X"),])
			print ("Number of removed markers")
			print (length(dropNames))
			# specific for the X chr
			print ("!eQTL mapping: Removing Monomorphic markers on X chr. ")
			dropNamesX=rownames(geno.table(cross_data)[which ((geno.table(cross_data)$ABf==0 & geno.table(cross_data)$ABr==0 & geno.table(cross_data)$AB==0 & geno.table(cross_data)$chr)=="X"),])
			print ("Number of removed markers")
			print (length(dropNamesX))
			dropNames=c(dropNames,dropNamesX)
			cross_data=drop.markers(cross_data, dropNames)
		}
	}
	
}else{
	# in case the univariate methods were run prior to this run, the cross_file created by that script is used to save time of loading the data
	load(paste(tmp_folder_path,"cross_file_ready.R",sep=""))
}

cross_data


marker_positions <- read.table(marker_positions_file ,header=TRUE,sep="\t");


#############################
# Reading the produced R objects by the random_forest_eQTL_multithread_firstpart.r
# Each of the Objects is run through and the values of all of them are saved into one object
Objects <- list.files(path = tmp_folder_path, pattern="P_all")
whole_data=list()
for (object in Objects)
{
	load (paste(tmp_folder_path,object,sep=""))
	print (object)
	whole_data$gene_name=c(whole_data$gene_name,P_all$gene_name)            
	whole_data$SF=c(whole_data$SF,P_all$SF)                
	whole_data$P=c(whole_data$P,P_all$P)               
	whole_data$gene_marker_pos=c(whole_data$gene_marker_pos,P_all$gene_marker_pos)
	whole_data$gene_Marker=c(whole_data$gene_Marker, P_all$gene_Marker)
}

print (whole_data)
# calculating the FDR correction (if desired)
Q_all = p.adjust(whole_data$P, "fdr")
QTL_to_print_names=c("name","method","pval","chr","pos","qvals","score","confInt")
# old naming: QTL_to_print_names=c("QTL_name","miRNA_name","chromosome","QTL_pos_in_CM","miRNA_marker")
QTL_to_print=matrix(ncol=8)
print (Q_all)
print (sum(Q_all <= sign_level_FDR))
print (sign_level_FDR)
cat ("!-",sum(Q_all < 0.05),"QTLs found (<5%)\n")
cat ("!-",sum(Q_all < 0.10),"QTLs found (<10%)\n")
cat ("!-",sum(Q_all < 0.15),"QTLs found (<15%)\n")
# producing results
if ((sum(Q_all <= sign_level_FDR))>=1){

	# how many q_vals per gene
	number_of_clusters=(length(Q_all)/length(whole_data$gene_name))
	first_r=1
	# selecting only the results over the defined threshold
	qtl_pos_orig=(which (Q_all < sign_level_FDR)) # positions of markers with QTLs
	# finding the position of the gene with significant result
	gene_with_qtls=trunc(qtl_pos_orig/number_of_clusters)+1
	
	for (p1 in 1:length(gene_with_qtls))
	{
		if (first_r==1){gene_nr1="test"}
		gene_same_test=gene_with_qtls[p1]
		if (gene_same_test != gene_nr1)
		{
			gene_nr1=gene_with_qtls[p1]
			first_r=0
			sig_markers_single_gene=numeric()
			check=0
			for (p2 in p1:length(gene_with_qtls))
			{
				gene_nr2=gene_with_qtls[p2]										
					
				if (gene_nr1==gene_nr2)
				{
					sig_marker=qtl_pos_orig[p2]
					sig_markers_single_gene=c(sig_markers_single_gene,sig_marker)	
					
				}
				if ((gene_nr1!=gene_nr2) || (p2==length(gene_with_qtls)))
				{
					# now we have the list of significant QTLs in sig_markers_single_gene for a gene on the current position in gene_with_qtls
					gene_name=whole_data$gene_name[gene_nr1]
					gene_oring_pos=whole_data$gene_Marker[gene_nr1]
					import_pos=(number_of_clusters*gene_nr1-number_of_clusters+1):(number_of_clusters*gene_nr1)
					sf_part=whole_data$SF[import_pos]
					Q=Q_all[import_pos]			

					# preparing the coordinate system for the plotting
					print (gene_name)
					m_2=1
					pos_last=1
					middle=numeric()
					markers_from_beginning=0

					print (c("markers_on_chromosomes:",markers_on_chromosomes))
					for(m in markers_on_chromosomes){
						# converting the value to a number and calculating the first marker on the given chromosome given the length of all the presiding markers
						m=as.numeric(m)
						markers_from_beginning=markers_from_beginning+m
						m=markers_from_beginning
						pos_ins=tail(which((as.numeric(gsub("V","",names(sf_part))) <= m) & (as.numeric(gsub("V","",names(sf_part))) >= m_2)),1)
						print (names(sf_part))
						print (m)
						print (m_2)
						print (pos_ins)	
						sf_part=append(sf_part,c(NA,NA,NA), pos_ins)
						Q=append(Q,c(NA,NA,NA), pos_ins)
						middle=c(middle,(pos_last+pos_ins+3)/2)                 
						m_2=m
						pos_last=pos_ins
					}
					qtl_pos=(which (Q < sign_level_FDR))

					# ploting the sf score
					pdf(paste(plot_folder, paste("sf_sig_dist",gene_name,"pdf",sep="."),sep=""), height=12, width=17)
					par(cex=0.9, xaxs="r", yaxs="r")
					plot(sf_part, type = "h", ylab = "adj.selectionfrequency",main= paste("RFSF of ",gene_name," (FDR",sign_level_FDR,"%)",sep=""), xaxt='n')
					print ("habada3")
					print (middle)
					print (chrnames(cross_data))
					print (markers_on_chromosomes)
					axis(side=1,tick=F,labels=chrnames(cross_data), at=middle)
					points(qtl_pos,sf_part[qtl_pos], col="red",lwd=1.5)


					# adding the gene nearest Marker
					print (whole_data$gene_marker_pos)
					print (gene_nr1)
					print (whole_data$gene_marker_pos[gene_nr1])
					if (is.na(whole_data$gene_marker_pos[gene_nr1])){ print ("pos: unknown")
					}else{
						gene_marker_pos=whole_data$gene_marker_pos[gene_nr1]
						cat ("gene_marker_pos:",gene_marker_pos,"\n")
						points(gene_marker_pos,0, col="green",lwd=2.0, pch=2)
						text(gene_marker_pos,0, labels=gene_oring_pos, pos=1, cex=1.2, col="green")
						abline(v=gene_marker_pos, lty = "dotted", col="green", cex=0.3)
				        }
					qtl_markers=gsub("V","",names(sf_part[qtl_pos]))
					text(qtl_pos,sf_part[qtl_pos], labels=qtl_markers, pos=4, cex=0.8, col="red")
					dev.off()
				

					print(find.markerpos(cross_data, qtl_markers)) 
					cat ("!! Q_vals:",Q[qtl_pos] ,"\n")

					#producing the effectplots
					min_y=min(cross_data$pheno[gene_name], na.rm=T)
					max_y=max(cross_data$pheno[gene_name], na.rm=T)
				       
					for_par=round(sqrt(length(qtl_markers))+1)
					for_par2=round(sqrt(length(qtl_markers)))
					if (for_par==0){
						for_par=1
						for_par2=1
					}else if (for_par==1){
						for_par=1
						for_par2=2
					}
					pdf(paste(paste(plot_folder, "effectplot_qtl_FDR",sep=""),gene_name, "pdf", sep="."), height=10, width=17)
					par(mfrow=c(for_par,for_par2),  oma = c( 0, 0, 2, 0 ) ) 
					for (p in c(1:length(qtl_markers))){
						       
						Qval=Q[qtl_pos]
						chr=find.markerpos(cross_data, qtl_markers[p])$chr

						print ("---")
						print (sf_part)
						print (qtl_pos)
						print (names(sf_part[qtl_pos]))
						print ("/////")
						print (qtl_markers)
						print (length(qtl_markers))
						print (qtl_pos)
						print (Q)
						print (Qval)
						print (gene_name)
						print (qtl_markers[p])
						print ("---")


						print ("habada//")
						print (for_par)
						print (for_par2)
						print ("habada2//")
						plot.pxg(cross_data, pheno.col=gene_name, marker=qtl_markers[p], ylim=c(min_y,max_y),  xlab=paste("Q Value: ",Qval,sep=""), ylab="", main= (paste("Marker:",qtl_markers[p], "(chr: ",chr," )" ,"Q Value: ",Qval[p],sep="")     ))      
				}
					par(mfrow=c(1,1))
					title(paste("Effect plot on Marker(s) ",toString(qtl_markers)," of the gene " ,gene_name, sep=""), outer=TRUE)
					dev.off()

					for (num in c(1:length(qtl_markers))){		

						QTL_num=qtl_markers[num]
						# In case the Markers have been clustered the confidence interval is defined as the range of the markers in one cluster
						if (Clustering_used == 1)
						{	
							markers_raw=marker_positions[c(which(marker_positions$Marker==QTL_num),which(marker_positions$Marker==QTL_num),which(marker_positions$Marker==QTL_num)+1),]$Marker
							markers_raw[3]=markers_raw[3]-1
							markers_raw[2]=round(mean(c(markers_raw[1],markers_raw[3])))
							confInt=paste(markers_raw,collapse = "_")
						}else{
							confInt="-"
						}
						qval=Qval[num]
						sf_score=sf_part[paste('V',QTL_num,sep="")]
						chr=find.markerpos(cross_data, QTL_num)$chr
						pos=find.markerpos(cross_data, QTL_num)$pos
						#QTL_to_print_cach=matrix(c(paste(gene_name,"_",QTL_num,sep=""), gene_name,chr,pos,gene_oring_pos),ncol=5)
						QTL_to_print_cach=matrix(c(gene_name,"RF","-",chr,pos,qval,sf_score, confInt),ncol=8)
						QTL_to_print = rbind(QTL_to_print,QTL_to_print_cach)
						
						
					}
					break
				}

			}			


		} 	

	}


print ("here!!")

colnames(QTL_to_print)=QTL_to_print_names       
QTL_to_print=na.omit(QTL_to_print)
write.table(QTL_to_print, file = paste(output_folder,"QTLs_identified_multivariant_FDR.txt",sep=""), row.names=FALSE)

# saving the cross_file as an R object for a faster loading time during the next steps. It overwrites potentially the cross_file cerated by the univariate methods (no worries, its the same file)
save(cross_data, file=paste(tmp_folder_path,"cross_file_ready.R",sep=""))

}else{
       print ("No significant QTLs found")
      # print (min(Q_all))
      # best_pos=which(Q==min(Q, na.rm=T))
      # pdf(paste(plot_folder, paste("sf_dist",miRNA,"pdf",sep="."),sep=""), height=12, width=17)
      # par(cex=0.9, xaxs="i", yaxs="r")
#	plot(sf.corr, type = "h", ylab = "adj.selectionfrequency",main= paste("RFSF of ",miRNA," Q-val: ",min(Q) ,sep=""), xaxt='n')
#       points(best_pos,sf.corr[best_pos], col="blue",lwd=1.5)
#       qtl_markers=gsub("V","",names(markers_only[best_pos]))
#       text(best_pos,sf.corr[best_pos], labels=qtl_markers, pos=4, cex=0.8, col="blue")
 #      dev.off()
}


#######






