#!/bin/bash

#%%%% HEAD %%%%#
#/-
#%% this script runs the following R scripts
# - eQTL mapping using the univariate methods from then QTL/R package
# - (if desired) a FDR correction of the resulting p_values of QTLs
# - effectplots of the found QTLs
# The main output is a qtl summary file ($qtl_file)

use_variable=$1
RIL_USED=$2
Memery_eQTL=$3
Number_of_threads_eQTL=$4
output_name_eQTL=$5
scripts_dir=$6
cross_file_variable=$7
output_dir="${8}"
plot_dir=${9}
step_v=${10}
error_prob_v=${11}
map_function_v=${12}
stepwidth_v=${13}
n_draws_v=${14}
n_perm_v=${15}
tmp_folder_path=${16}
sign_level=${17}
sign_level_FDR=${18}
output_process_file=${19}
use_FDR=${20}
Clustering_used=${21}
Marker_pos_variable=${22}
Memory_eQTL_FDR=${23}
Number_of_threads_eQTL_FDR=${24}
output_name_eQTL_FDR=${25}
Memory_effectplot=${26}
output_name_effectplots=${27}
drop=${28}
cross_data_object_ready=${29}
cross_data_object=${30}
Number_of_machienes_eQTL=${31}
Memory_eQTL_preload=${32}
Number_of_threads_eQTL_preload=${33}
output_name_clustering=${34}
LIST_of_gene_NAMES=${35}
gene_names_LIST_used=${36}

echo -e "			Starting eQTL-mapping using univariate methods (standard interval mapping, Extended Haley-Knott and multiple imputation)"
echo -e "			Starting eQTL_mapping using univariate methods (standard interval mapping, Extended Haley-Knott and multiple imputation).\n			cross_file is ${cross_file_variable}" >> ${output_dir}/${output_process_file}


#%%%% BODY %%%%#
#/-

############# #Running the eQTL calculations
# the mapping is done with the cross_file in the variable $cross_file_variable, which is either clustered, or not, depending the the users choice.

# The cross_file R-Object is created

echo -e "			Staring eQTL preloading. Requesting RAM: $Memory_eQTL_preload. Requesting CPUs: $Number_of_threads_eQTL_preload" >> ${output_dir}/${output_process_file}
echo -e "Memory_eQTL_preload=$Memory_eQTL_preload"
echo -e "qsub -l "h_vmem=${Memory_eQTL_preload}" -pe smp ${Number_of_threads_eQTL_preload} -wd "${tmp_folder_path}/" -N preload_${output_name_eQTL} -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -v PATH=$PATH -b y "Rscript ${scripts_dir}univariate_methods_preload.r $cross_file_variable $RIL_USED $step_v $error_prob_v $cross_data_object_ready $cross_data_object $use_variable $map_function_v $stepwidth_v $n_draws_v "$tmp_folder_path/" $Clustering_used" $Number_of_threads_eQTL_preload "
qsub -l "h_vmem=${Memory_eQTL_preload}" -pe smp ${Number_of_threads_eQTL_preload} -wd "${tmp_folder_path}/" -N preload_${output_name_eQTL} -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -v PATH=$PATH -b y "Rscript ${scripts_dir}univariate_methods_preload.r $cross_file_variable $RIL_USED $step_v $error_prob_v $cross_data_object_ready $cross_data_object $use_variable $map_function_v $stepwidth_v $n_draws_v "$tmp_folder_path/" $Clustering_used" $Number_of_threads_eQTL_preload
# reading the file with the gene_number.
for_greping=$(echo preload_${output_name_eQTL} |sed 's/./&\n/10' |head -1)
R_eQTL_preload_j=$(qstat |grep ${for_greping} |awk '{print $1}')
while [ "$R_eQTL_preload_j" != "" ]  
do
        sleep 10
        R_eQTL_preload_j=$(qstat |grep ${for_greping} |awk '{print $1}')
        echo -e "waiting for the QTL preloading:" $R_eQTL_preload_j
done
sed 1d ${tmp_folder_path}/gene_number_file.txt | sed 's/"//g' > ${tmp_folder_path}/tmp_f.txt
mv ${tmp_folder_path}/tmp_f.txt ${tmp_folder_path}/gene_number_file.txt
source ${tmp_folder_path}/gene_number_file.txt

# Submitting the eQTL jobs
echo -e "		submitting jobs"
# The number of phenotypes submitted varies per used thread. 
first_number=1
echo $gene_number
echo $((${Number_of_machienes_eQTL}-1))

# In case you only need specific gene names
if [ $gene_names_LIST_used -ne 0 ];then
	gene_number=$(cat $LIST_of_gene_NAMES |wc -l)
	echo -e "Just testing the genes from the following file:" $LIST_of_gene_NAMES
fi;

step_size=$(($gene_number/$((${Number_of_machienes_eQTL}-1))))
echo $step_size
if [ $step_size -eq 0 ];then step_size=1;fi;
echo -e "			The eQTL has been started (Memory: ${Memery_eQTL}, Cores: ${Number_of_threads_eQTL}, on ${Number_of_machienes_eQTL} machines). Significance level for eQTLs: $sign_level.\n			NOTE: The amount on permutaions ($n_perm_v) used to determine the p_value" >> ${output_dir}/${output_process_file}
#

# Consider that one core more is requested, because of the way the scanone function n.perm attribute works (martin email)
# starting multiple procceses is different in case you have a cross_object ready

# Please note: due to the nature of the scanone function, while requesting N CPUs to do the calculations, the Master job, creating the N slave jobs also does calculations, thus requiring a CPU. Therefore for the qsub command a +1 core is requested. 
for second_number in $(seq 1 $step_size $gene_number |awk '(NR>1){print $0}')
do
		echo -e "                       submitting job with phenotypes from $first_number to $second_number"
		echo -e "                       submitting job with phenotypes from $first_number to $second_number" >> ${output_dir}/${output_process_file} 
		qsub -l "h_vmem=${Memery_eQTL}" -pe smp $((${Number_of_threads_eQTL}+1)) -wd "${tmp_folder_path}/" -N ${output_name_eQTL}_uniVar -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -v PATH=$PATH -o ${job_name}.${ojob_id} -b y "Rscript ${scripts_dir}eQTL_univariate_methods.r $Marker_pos_variable ${plot_dir} $Number_of_threads_eQTL "$tmp_folder_path/" $sign_level $use_FDR $drop $first_number $second_number $use_variable $n_perm_v $LIST_of_gene_NAMES" >> ${output_dir}/${output_process_file}
		first_number=$(($second_number+1))
		cores_used=$(($cores_used+1))
		sleep 4
done
# Submitting the rest
if [ $(($gene_number-$second_number)) != 0 ]; then
        echo -e "                       submitting job with phenotypes from $(($second_number+1)) to $gene_number"
        echo -e "                       submitting job with phenotypes from $(($second_number+1)) to $gene_number" >> ${output_dir}/${output_process_file}
	qsub -l "h_vmem=${Memery_eQTL}" -pe smp $((${Number_of_threads_eQTL}+1)) -wd "${tmp_folder_path}/" -N ${output_name_eQTL}_uniVar -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -v PATH=$PATH -o ${job_name}.${ojob_id} -b y "Rscript ${scripts_dir}eQTL_univariate_methods.r $Marker_pos_variable ${plot_dir} $Number_of_threads_eQTL "$tmp_folder_path/" $sign_level $use_FDR $drop $(($second_number+1)) $gene_number $use_variable $n_perm_v $LIST_of_gene_NAMES" >> ${output_dir}/${output_process_file}
	last_n1=$(($second_number+1))
	last_n2=$(($first_pos_check-2))
fi;

#qsub -l "h_vmem=${Memery_eQTL}" -pe smp ${Number_of_threads_eQTL} -wd "${tmp_folder_path}/" -N ${output_name_eQTL}_uniVar -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -v PATH=$PATH -o ${job_name}.${ojob_id} -b y "Rscript ${scripts_dir}eQTL_univariate_methods.r $use_variable $cross_file_variable $Marker_pos_variable ${plot_dir} $RIL_USED $step_v $error_prob_v $map_function_v $stepwidth_v $n_draws_v $Number_of_threads_eQTL $n_perm_v "$tmp_folder_path/" $sign_level $Clustering_used $use_FDR $drop $cross_data_object_ready $cross_data_object"

# Defining the cross_file Object, produced by eQTL_univariate_methods.r 
# NOTE: The R object is produced based on the preproceding options. (In contains only clustered markers in case they have been clustered)
cross_file_R_OBJECT="${tmp_folder_path}/cross_file_ready.R"

#Checking if the proceses are finished
for_greping=$(echo ${output_name_eQTL}_uniVar |sed 's/./&\n/10' |head -1)
R_eQTL_job_for_loop=$(qstat |grep ${for_greping} |awk '{print $1}')
while [ "$R_eQTL_job_for_loop" != "" ]  
do   
	sleep 100
	qtl_count=$(grep "\[1\]" ${tmp_folder_path}/${output_name_eQTL}_uniVar.o* -A1 |sed 's/There were no LOD peaks above the threshold.//g' |sed 's/--//g'| sed '/^ *$/d' | awk '/chr/{print x};{x=$0}' |sed 's/\[1\] //g' | sed 's/"//g' |awk '{print $2}' |sort | uniq -c )                                

	R_eQTL_job_for_loop=$(qstat |grep ${for_greping} |awk '{print $1}')
	echo -e "waiting for the QTL job:" $R_eQTL_job_for_loop
	echo -e "so far found number of QTLs(em,ehk,imp) not corrected for multiple testing:\n $qtl_count"
done       

############## # Producing a summary qtl_file
if [ $(grep "There were some monomorphic markers"  ${tmp_folder_path}/preload_${output_name_eQTL}.o* -c) -eq 1 ] || [ $(grep "There were some monomorphic markers"  ${tmp_folder_path}/${output_name_clustering}.o* -c) -eq 1 ];then
	echo -e "		There were some monomorphic markers in the dataset. They have been removed"
	echo -e "		$(grep "There were some monomorphic markers" ${tmp_folder_path}/preload_${output_name_eQTL}.o* ${tmp_folder_path}/${output_name_clustering}.o* -A 2 -h | tail -n2 | sed 's/\[1\]//g')"
fi;
grep "\[1\]" -h ${tmp_folder_path}/${output_name_eQTL}_uniVar.o* -A1 | sed 's/There were no LOD peaks above the threshold.//g' |sed 's/--//g'| sed '/^ *$/d' | awk '/chr/{print x};{x=$0}' |sed 's/\[1\] //g' > ${tmp_folder_path}/qtl_names_tmp
echo -e "name\tmethod\tpval\tchr\tpos\tqvals\tscore\tconfInt" > ${output_dir}QTLs_identified_univariant.txt
fgrep -h -f ${tmp_folder_path}/qtl_names_tmp ${tmp_folder_path}/${output_name_eQTL}_uniVar.o* -A3 |grep "chr" -v | sed ':a;N;$!ba;s/\n/ /g' |sed 's/\[1\]//g' |sed 's/^ //g' |sed 's/   / /g' | awk '{$1=$1}1' OFS="\t" |sed 's/--/\n/g' |sed 's/^\t//g' | sed '/^ *$/d'|awk -F "\t" '{print $1FS$2FS$7FS$4FS$5FS"-"FS$6FS$8}' >>${output_dir}QTLs_identified_univariant.txt
# declaring the qtl_file variable
qtl_file=${output_dir}QTLs_identified_univariant.txt
echo -e "			\e[00;35m ----------------\n\e[00m eQTL mapping ready! Identified QTLs: \e[00;32m $qtl_file \e[00m"
echo -e "			 ----------------\neQTL mapping ready! Identified QTLs: $qtl_file" >> ${output_dir}/${output_process_file}

############## # Running the FDR correction if desired
if [ "${use_FDR}" -eq 1 ];then
	echo -e "\n			\e[00;35m NOTE\n\e[00m: Running an FDR correction for the results"
	echo -e "\n			NOTE: Running an FDR correction for the results" >> ${output_dir}/${output_process_file}
	
	# creating a summary p_val file out of all the p_val files
	cat $tmp_folder_path/eQTL_univariate_results_pvalues* > $tmp_folder_path/eQTL_univariate_results_pvalues_all.txt
	grep -v "chr" $tmp_folder_path/eQTL_univariate_results_pvalues_all.txt > $tmp_folder_path/tmp_1
	mv $tmp_folder_path/tmp_1 $tmp_folder_path/eQTL_univariate_results_pvalues_all.txt
	for_head=$(ls -l $tmp_folder_path/ | grep eQTL_univariate_results_pvalues[1234567890] |head -1 | awk -F " " '{print $8}')
	head -1 $tmp_folder_path/$for_head > $tmp_folder_path/eQTL_univariate_results_pvalues_all_head.txt
	cat $tmp_folder_path/eQTL_univariate_results_pvalues_all_head.txt $tmp_folder_path/eQTL_univariate_results_pvalues_all.txt >  $tmp_folder_path/tmp_1
	mv  $tmp_folder_path/tmp_1 $tmp_folder_path/eQTL_univariate_results_pvalues_all.txt

	qsub -l "h_vmem=${Memory_eQTL_FDR}" -pe smp ${Number_of_threads_eQTL_FDR} -wd "${tmp_folder_path}/" -N ${output_name_eQTL_FDR} -v PATH=$PATH -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -o ${job_name}.${ojob_id} -b y "Rscript ${scripts_dir}FDR_corr_all.r "$tmp_folder_path/" $use_variable $sign_level_FDR $plot_dir $RIL_USED $step_v $error_prob_v $map_function_v $stepwidth_v $n_draws_v $cross_file_R_OBJECT $output_dir $Number_of_threads_eQTL_FDR"
	#Checking if the proceses are finished
	for_greping_FDR=$(echo ${output_name_eQTL_FDR}|sed 's/./&\n/10' |head -1)
	R_eQTL_job_FDR=$(qstat |grep $for_greping_FDR |awk '{print $1}')
	R_eQTL_job_for_loop_FDR=$(qstat |grep ${for_greping_FDR} |awk '{print $1}')
	while [ "$R_eQTL_job_for_loop_FDR" != "" ]  
	do   
		sleep 70
		R_eQTL_job_for_loop_FDR=$(qstat |grep ${for_greping_FDR} |awk '{print $1}')
		echo -e "waiting for the FDR job:" $R_eQTL_job_FDR
	done       
	# short post-proccesing
	sed 's/ /\t/g' ${output_dir}QTLs_identified_univariant_FDR.txt > ${tmp_folder_path}/tmp
	mv ${tmp_folder_path}/tmp ${output_dir}QTLs_identified_univariant_FDR.txt
	# declaring the qtl_file variable
	qtl_file=${output_dir}QTLs_identified_univariant_FDR.txt
	echo -e "			File with identified QTLs has been updated: \e[00;32m $qtl_file \e[00m"
	echo -e "			File with identified QTLs has been updated: $qtl_file" >> ${output_dir}/${output_process_file}
	# checking whether any results were found (if any of the QTLs survived the FDR)
	check_check=$(cat $qtl_file | wc -l)
fi;


############## # Producing effectplots of the significant QTLs 
# The Effetplots are done using the qtl_file varible $qtl_file, which are either with or without the FDR corrected p_values. Either way the qtl_file has the identical form. (Note that the position of the columns are different, but the names are identical between the two variants of the file)
if [ "${check_check}" -eq 1 ];then
	echo -e "			No significant QTLs were found under the current FDR threshold: $sign_level_FDR"
	echo -e "			No significant QTLs were found under the current FDR threshold: $sign_level_FDR" >> ${output_dir}/${output_process_file}
else
	# running the effectlot on all the found QTLs
	echo -e "			Producing effectplots for the identified QTLs"
	echo -e "			Producing effectplots for the identified QTLs" >> ${output_dir}/${output_process_file}
	echo "			Submitting the job"
	echo $qtl_file $Marker_pos_variable $cross_file_R_OBJECT $plot_dir
	qsub -l "h_vmem=$Memory_effectplot" -wd "${tmp_folder_path}/" -N ${output_name_effectplots} -v PATH=$PATH -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -b y "Rscript ${scripts_dir}effect_plots.r $qtl_file $Marker_pos_variable $cross_file_R_OBJECT $plot_dir"
	echo $qtl_file
	for_greping_eff=$(echo ${output_name_effectplots}|sed 's/./&\n/10' |head -1)
	R_eQTL_job_for_loop=$(qstat |grep $for_greping_eff |awk '{print $1}')
	while [ "$R_eQTL_job_for_loop" != "" ]
	do
		sleep 50
		R_eQTL_job_for_loop=$(qstat |grep $for_greping_eff|awk '{print $1}')
		echo -e "waiting for the Effectplot job:" $R_eQTL_job_for_loop
	done
fi;


### 

# READY

