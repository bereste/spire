#!/bin/bash
# This script is a part of the miRNA expression analysis pipeline. 
# It mappes reads of a given sample to a given miRNA library and returns a coverage file

read_path=$1
for x in `echo $read_path | tr "/" " "`; do read=$x; done
miRNAlib_path=$2
for x in `echo $miRNAlib_path | tr "/" " "`; do miRNAlib=$x; done
Number_of_threads_read_mapping=$3
mapping_software=$4
mp=$5


mkdir -p ${read}_${miRNAlib}_tmp
cd ${read}_${miRNAlib}_tmp

echo -e "	convert_n_map.sh (microRazorS) ($read):   converting the file to fasta format" 
fastq2fasta.pl $read_path > ${read}.fasta #fasta converting  
echo -e "	convert_n_map.sh($read):   collapsing reads in the file" 
collapse_reads.pl~ ${read}.fasta seq > ${read}.collapsed # collapsing reads 
# settings: -m 1 (max 1 best hin), -f: forward hits only -pa: delete reads with more then max-hist best matches
echo -e "	convert_n_map.sh($read):   mapping the reads to the genes in the library using $mp. Forward and reverse mapped reads are saved into separate files." 
#echo "/home/ikel/bin/micro_razers64 $miRNAlib_path ${read}.collapsed -vv -m 1 -f -pa -o ${read}.collapsed_vs_miRNALib_mRs"
# staring the appropriate software
if [ $mapping_software -eq 1 ]
	output_name=${read}.collapsed_vs_GeneLib_Rs
	razers3 $miRNAlib_path ${read}.collapsed -vv -o $output_name #mapping
else
	output_name=${read}.collapsed_vs_miRNALib_mRs
	micro_razers64 $miRNAlib_path ${read}.collapsed -vv -m 1 -f -pa -o $output_name #mapping
	micro_razers64 $miRNAlib_path ${read}.collapsed -vv -m 1 -r -pa -o ${output_name}_reverse #mapping
fi;

echo -e "	convert_n_map.sh($read):   the read coverage for each gene is calculated" 
echo -e ${read} > ${read}.miRNA_cov
echo -e ${read} > ${read}.miRNA_not_mapped
for miRNA in $(cat $miRNAlib_path| grep "^>"| sed 's/>//g')
do
	cov=$(grep -w -i -e $miRNA ${output_name} |awk '{print $1}' |awk -F"_" '{print $3}' |sed 's/x//g' |awk '{SUM += $1} END {print SUM}')
	if [ "$cov" != "" ];then
		echo -e $miRNA"\t"$cov >> ${read}.miRNA_cov 	
	else
		echo -e $miRNA"\t"1 >> ${read}.miRNA_cov	
		echo -e $miRNA >> ${read}.miRNA_not_mapped
	fi;
done

cd ..


