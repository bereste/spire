#%%%% HEAD %%%%#
#/-
#%% This script calculates the hotspots for the significant QTLs

# reading the command line arguments
args <- commandArgs(TRUE)
qtl_confInt_file <- args[1]
marker_pos_file <- args[2]
tmp_folder<-args[3]
bin_step<-as.numeric(args[4])
p_val_hotspot<-as.numeric(args[5])

# loading files 
qtl_confint<-read.table(qtl_confInt_file)
marker_positions <- read.table(marker_pos_file ,header=TRUE,sep="\t");

## going through the results and putting them into bins
bin_step=bin_step
first_marker=marker_positions$Marker[1]
last_marker=tail(marker_positions$Marker, n=1)
bins=seq (first_marker, last_marker,bin_step)

#matrix to store the hits
m=matrix(ncol=1,nrow=length(bins),0)
rownames(m)=bins

for (i in 1:dim(qtl_confint)[1])
{
	gene_name=qtl_confint[i,][1]
	gene_conf_int_1=qtl_confint[i,][2]
	gene_conf_int_2=qtl_confint[i,][3]

	# selecting which bin should get a hit 
	s=which (as.numeric(rownames(m))>=as.numeric(gene_conf_int_1) & as.numeric(rownames(m))<=as.numeric(gene_conf_int_2))
	if (length(which(as.numeric(rownames(m))==as.numeric(gene_conf_int_1)))==0 & length(s)!=0) 
	{
		# If the marker is not present in bins, which means, that we need to count from one position before 
		s=c(s[1]-1,s)
	}
	if (length(s)==0){s=tail(which (as.numeric(rownames(m))<=as.numeric(gene_conf_int_2)),1)}
	m[s,]=m[s,]+1
}
		
# the hits matrix. Also selecting the bins with at least 1 hit
mm=subset(m, m!=0)

# checking the significance under poison distribution
cat (paste("Under a poisson distribution, these are the significant QTLs (p_val <= ",p_val_hotspot,"):\n"))
lambda_m=mean(mm)
for (i in 1:dim(mm)[1])
{
	res=dpois(mm[i],lambda=lambda_m)
	if (res <= p_val_hotspot)
	{
		print(c(rownames(mm)[i],res,mm[i]))
	}
}

