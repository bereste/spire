#%%%% HEAD %%%%#
#/-
# This script processes the eQTL mapping results file and produces the final output table neatly formated#
# Needs a prerun cis_trans module, so that it can process the QTLs_identified_[method]_final.txt files


# Importing packages
import sys 
import glob # to list specific files in a dir

# Reading the attributes 
output_dir = sys.argv[1]
marker_pos = open("%s"%(sys.argv[2]),"rU")   

# writing here
QTL_results_file= open("".join([output_dir,"QTL_results_file.csv"]),"w")

# Declaring the variables
results_dict={} # key: gene_name, value: [[methods],[p_vals],[eqtl chrs],[qvals],[[conf_int_markers]],[[conf_int_pos]],[cis/trans], gene_pos, gene_chr, gene_marker,[eqtl_markers],[eqtl_markers_pos]]
marker_dict={} #key: Marker_index, value:[chr,mean_pos,left_pos,right_pos]

#"name" "method" "pval" "chr" "pos" "qvals" "score" "confInt" "gene_position" "gene_chr" "gene_Marker" "cis_trans"
#0	1	   2	  3	4	5	6	7	  8		9		10		11

# Listing the files that have to be processed
list_of_files =  glob.glob("".join([output_dir,'QTLs_identified_*_final.txt'])) 
print (list_of_files)

# reading Marker position into a dictionary
for num,line in enumerate(marker_pos):
	if num>=1:
		line_split=line.split("\t")
		if len((line.split("\t")[2].replace("\n","")).split("-"))==2:
			marker_dict[int(line.split("\t")[0])]=[line.split("\t")[1],float(line.split("\t")[3].replace("\n","")),float((line.split("\t")[2].replace("\n","")).split("-")[0]),float((line.split("\t")[2].replace("\n","")).split("-")[1])]	
		else:
			marker_dict[int(line.split("\t")[0])]=[line.split("\t")[1],float(line.split("\t")[3].replace("\n","")),float((line.split("\t")[2].replace("\n",""))),float((line.split("\t")[2].replace("\n","")))]


for res_file in list_of_files:
	if res_file!="QTLs_identified_ALL_final.txt":
		results = open("%s"%(res_file),"rU")
		print (res_file)
		# creating a big dictionary  with the result of each method separately
		for num,line in enumerate(results):
			if num>=1:
				lines_line=line.split(" ")
				gene_name=lines_line[0].replace("\"","")
				method=lines_line[1].replace("\"","")
				p_val_t=lines_line[2].replace("\"","")
				if p_val_t!="-":
					p_val=float(p_val_t)
				else:
					p_val=p_val_t
				e_chr=lines_line[3].replace("\"","")
				q_val_t=lines_line[5].replace("\"","")
				if q_val_t!="-":
					qval=float(q_val_t)
				else:
					qval=q_val_t
				conf_int_markers=[int(lines_line[7].replace("\"","").split("_")[0]),int(lines_line[7].replace("\"","").split("_")[2])]
				# finding the locus positions of those markers
				# in case of clustered markers, the Markers_pos_clustered.txt does not have all of the marker and therefore the processing should be done a bit different
				if marker_dict.has_key(int(lines_line[7].replace("\"","").split("_")[1])):
					eqtl_marker=int(lines_line[7].replace("\"","").split("_")[1])
				else:
					eqtl_marker=int(lines_line[7].replace("\"","").split("_")[0])
					
				eqtl_markers_pos=marker_dict[eqtl_marker][1]

				# the same for conf int
				if marker_dict.has_key(conf_int_markers[1]):
					conf_int_pos=[marker_dict[conf_int_markers[0]][1],marker_dict[conf_int_markers[1]][1]]
				else:
					conf_int_pos=[marker_dict[eqtl_marker][2],marker_dict[eqtl_marker][3]]
				cis_trans=lines_line[11].replace("\"","").replace("\n","")
				if results_dict.has_key(gene_name):
					method_tmp=results_dict[gene_name][0]	
					method_tmp.append(method)
					results_dict[gene_name][0]=method_tmp
					p_val_tmp=results_dict[gene_name][1]	
					p_val_tmp.append(p_val)
					results_dict[gene_name][1]=p_val_tmp
					e_chr_tmp=results_dict[gene_name][2]	
					e_chr_tmp.append(e_chr)
					results_dict[gene_name][2]=e_chr_tmp
					qval_tmp=results_dict[gene_name][3]	
					qval_tmp.append(qval)
					results_dict[gene_name][3]=qval_tmp
					conf_int_markers_tmp=results_dict[gene_name][4]	
					conf_int_markers_tmp.append(conf_int_markers)
					results_dict[gene_name][4]=conf_int_markers_tmp
					conf_int_pos_tmp=results_dict[gene_name][5]	
					conf_int_pos_tmp.append(conf_int_pos)
					results_dict[gene_name][5]=conf_int_pos_tmp
					cis_trans_tmp=results_dict[gene_name][6]	
					cis_trans_tmp.append(cis_trans)
					results_dict[gene_name][6]=cis_trans_tmp
					eqtl_marker_tmp=results_dict[gene_name][10]	
					eqtl_marker_tmp.append(eqtl_marker)
					results_dict[gene_name][10]=eqtl_marker_tmp
					eqtl_markers_pos_tmp=results_dict[gene_name][11]	
					eqtl_markers_pos_tmp.append(eqtl_markers_pos)
					results_dict[gene_name][11]=eqtl_markers_pos_tmp
				else:
					gene_pos=lines_line[8].replace("\"","")
					gene_chr=lines_line[9].replace("\"","")
					gene_marker=lines_line[10].replace("\"","")
					
					method_tmp=[lines_line[1].replace("\"","")]
					p_val_tmp_t=lines_line[2].replace("\"","")
					if p_val_tmp_t!="-":
						p_val_tmp=[float(p_val_tmp_t)]
					else:
						p_val_tmp=[p_val_tmp_t]

					e_chr_tmp=[lines_line[3].replace("\"","")]
					q_val_tmp_t=lines_line[5].replace("\"","")
					if q_val_tmp_t!="-":
						qval_tmp=[float(q_val_tmp_t)]
					else:
						qval_tmp=[q_val_tmp_t]
					conf_int_markers_tmp=[conf_int_markers]
					conf_int_pos_tmp=[conf_int_pos]
					cis_trans_tmp=[lines_line[11].replace("\"","").replace("\n","")]
					eqtl_marker_tmp=[int(lines_line[7].replace("\"","").split("_")[1])]
					eqtl_markers_pos_tmp=[eqtl_markers_pos]

					results_dict[gene_name]=[method_tmp, p_val_tmp, e_chr_tmp, qval_tmp, conf_int_markers_tmp, conf_int_pos_tmp, cis_trans_tmp, gene_pos, gene_chr, gene_marker, eqtl_marker_tmp, eqtl_markers_pos_tmp]

print (results_dict)
print (results_dict.keys())

# reducing the number of QTLs found by different methods
#for r_key in results_dict.keys():
#	for num,qtl_mark in enumerate(results_dict[r_key][10]:
		
	

# write the formated file
QTL_results_file.write("Gene_name\tGene_marker\tGene_chr\tGene_pos\tQTL_Marker\tQTL_position\tQTL_chr\tQTL_support_interval_markers\tQTL_support_interval_positions\tSupported by methods\tP_values\tQ_values\tCis/trans\n")
for r_key in results_dict.keys():
	for num,qtl_mark in enumerate(results_dict[r_key][10]):
		QTL_results_file.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n"%(r_key, results_dict[r_key][9],results_dict[r_key][8],results_dict[r_key][7],qtl_mark,results_dict[r_key][11][num], results_dict[r_key][2][num],results_dict[r_key][4][num],results_dict[r_key][5][num],results_dict[r_key][0][num], results_dict[r_key][1][num],results_dict[r_key][3][num],results_dict[r_key][6][num]))

