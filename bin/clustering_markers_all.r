#%%%% HEAD %%%%#
#/-
#%% This script clusters markers and produces a cross_file object with clustered markers, as well as a file, which is then used to produce a new Marker_positions file 

# loading libraries
library(qtl)
library(snow)
library(amap)

# reading the command line arguments
args <- commandArgs(TRUE)
Markers_to_cluster <- args[1] # /data/bioinformatics/Collaborations/Hawaii/QX/QX_30RILs/Markers_only.txt
output_folder <- args [2] # /data/bioinformatics/Collaborations/Hawaii/QX/QX_30RILs/
cross_file <- args[3]
function_folder <- args[4]
RIL_used <- args[5]
tmp_folder <- args[6]
plot_folder <- args[7]
eQTL_method <- as.numeric(args[8])
ntree_v <- as.numeric(args[9])
Number_of_threads_Clustering<-as.numeric(args[10])
Marker_pos_file <- args[11]
markers_on_chr <- unlist(strsplit(args[12], "_"))

print (markers_on_chr)
print (Number_of_threads_Clustering)
# reading the cross_file
suppressWarnings(cross_data <-read.cross("csv", dir="", file=cross_file, sep="\t", estimate.map=FALSE));

# reading Marker_pos file	
Marker_pos<-read.table(Marker_pos_file, header=T)

# converting the data to a special format in case it is a RIL dataset
print (cross_data)
if (RIL_used==1)
{
        suppressWarnings(cross_data <- convert2riself(cross_data))
        # Also any monomorphic markers are ommited (All of the samples have the same allel on at least one of the markers), as they produce a singularity matrix. ()
        if ((any(geno.table(cross_data)$BB==0)) || (any(geno.table(cross_data)$AA==0)) )
        {   
                print ("!eQTL mapping: There were some monomorphic markers. They will be removed from the dataset. Removing markers:")
                dropNames=rownames(geno.table(cross_data)[which (geno.table(cross_data)$AA==0 | geno.table(cross_data)$BB==0),])
		print ("Number of removed markers")
		print (length(dropNames))
                cross_data=drop.markers(cross_data, dropNames)
        }   
}else{
        if ((any(geno.table(cross_data)$BB==0)) || (any(geno.table(cross_data)$AA==0)) || (any(geno.table(cross_data)$AB==0)))
        {   
                print ("!eQTL mapping: There were some monomorphic markers. They will be removed from the dataset. Removing markers:")
                dropNames=rownames(geno.table(cross_data)[which ((geno.table(cross_data)$AA==0 | geno.table(cross_data)$BB==0 | geno.table(cross_data)$AB==0) & (geno.table(cross_data)$chr)!="X"),])
		print ("Number of removed markers")
                print (length(dropNames))
		# specific for the X chr
		print ("!eQTL mapping: Removing Monomorphic markers on X chr. ")
		dropNamesX=rownames(geno.table(cross_data)[which ((geno.table(cross_data)$ABf==0 & geno.table(cross_data)$ABr==0 & geno.table(cross_data)$AB==0 & geno.table(cross_data)$chr)=="X"),])
		print ("Number of removed markers (chr X only)")
                print (length(dropNamesX))
		dropNames=c(dropNames,dropNamesX)
		cross_data=drop.markers(cross_data, dropNames)
        }     
}

## Reading the markers
print ("Reading the Markers_only file to cluster the markers..")
markers_only=read.table(Markers_to_cluster, header=F)
a=""
# preparing a helper file with the clusterd markers. 
write.table(a, file = paste(tmp_folder,"clustered_markers_helper_file.txt", sep=""), row.names=FALSE, col.names=FALSE)
write.table(a, file = paste(tmp_folder,"clustered_markers_helper_file2.txt", sep=""), row.names=FALSE, col.names=FALSE)

#-/
print (cross_data)
print (dim(Marker_pos))

#%%%% BODY %%%%#
#/-
first_pos=1
markers_from_beginning=0
clustered_markers=numeric()
habra_store=numeric()
new_clusters_all_2=vector()
number_of_preciding_markers=0
# Splitting them to avoid clustering markers on different chromosomes
for (chr_split in markers_on_chr)
{
	print ("Starting the clustering on the following Chromosomes..")
	print (markers_on_chr)
	print (chr_split)
	print (cross_data)
	
	print ("--1")
	#number_of_preciding_markers=number_of_preciding_markers+as.numeric(chr_split)
	#chr_split=number_of_preciding_markers
	# if there is more than one chr in the dataset the positions of the markers in the following chromosomes will be altered due to the work on the previous chromosomes. This is why the actuall chromosome length is computed every time 
	#chr_split=as.numeric(tail(markernames(cross_data,find.markerpos(cross_data, as.numeric(chr_split)-1)$chr),1))
	# converting the value to a number and calculating the first marker on the given chromosome given the length of all the presiding markers
	chr_split=as.numeric(chr_split)
	markers_from_beginning=markers_from_beginning+chr_split
	chr_split=markers_from_beginning

	print (chr_split)
	print (tail(find.markerpos(cross_data, as.numeric(chr_split)-1)$chr,1))
	# Find the position of the given Chromosome in the order on the Marker_pos
		#chr_pos=which(unique(Marker_pos$chr) %in% tail(find.markerpos(cross_data, as.numeric(chr_split)-1)$chr,1))
		#markers_to_check=as.numeric(markernames(cross_data,tail(find.markerpos(cross_data, as.numeric(chr_split)-1)$chr,1)))



	# removing any potential other characters from the data
		#first_pos=markers_to_check[1]
		#chr_split=first_pos+as.numeric(chr_split)-1
	print ("--2")
	print (c(first_pos,chr_split))
	data_matrix=t(markers_only[first_pos:chr_split])
	print (dim(data_matrix))
	# excluding the monomorphic markers from them set before doing the clustering
	print (dropNames)
	print (first_pos:chr_split)
	print ((first_pos:chr_split) %in% as.numeric(dropNames))
	dropNames_spec=which((first_pos:chr_split) %in% as.numeric(dropNames))
	print (dropNames_spec)
	if (length(dropNames_spec)>=1){data_matrix=data_matrix[-as.numeric(dropNames_spec),]}
	print ("After excluding the monomorphic markers ")
	print (dim(data_matrix))
	data_matrix[data_matrix!=0 & data_matrix!= 1 & data_matrix!= 2] <- NA
	habra=gsub("V", "", names(which(rowSums(is.na(data_matrix))>=1)))
	habra_store=c(habra_store,habra)
	cross_data = drop.markers(cross_data, habra)
	data_matrix<-na.exclude(data_matrix)
	print ("After excluding NA values")
	print (dim(data_matrix))
	print (dim(Marker_pos))
	print (cross_data)
	print ("--3")
	# building a distance matrix required for the clustering
	#matr_dist=Dist(as.matrix(data_matrix, ncol=nrow(markers_only)),method ="manhattan", nbproc = Number_of_threads_Clustering)
	first_pos=chr_split+1
	print ("--4")
	# Find the corresponding Chromosome
	chr_cluster=find.markerpos(cross_data, chr_split-1)$chr
	print ("--5")
	print (chr_split)
	print (find.markerpos(cross_data, chr_split-1))
	if (is.na(chr_cluster)){chr_cluster=find.markerpos(cross_data, chr_split)$chr}
	print (chr_cluster)
	print (length(markernames(cross_data, chr_cluster)))
	# clustering the markers
	# Checking if the number of markers on a chromosome is bigger than 1
	if ( length(markernames(cross_data, chr_cluster))>=2)
	{
		#clusters=hclust(matr_dist)
		clusters<-hcluster(as.matrix(data_matrix, ncol=nrow(data_matrix)), method = "manhattan", link = "complete", nbproc = Number_of_threads_Clustering)

		print ("--6")
		# Ploting a clustered tree of the markers for each chromosome
		pdf(paste(paste(plot_folder,"Markers_clustered","_chr",chr_cluster, sep=""),"pdf",sep="."),height=20, width=30)
		plot(clusters, cex=0.7)
		dev.off()
		print ("--7")
		# Defining a what level to cut the tree (the level of simularity at which the markers should be clustered)	
		new_clusters_all=cutree(clusters, h=0)
		new_markers=names(subset(new_clusters_all, !duplicated(new_clusters_all)))
		print ("--8")
		# Getting the names of the clustered markers from the cross_file, which will be droped from the cross_file
		drop=names(subset(new_clusters_all, ! names(new_clusters_all) %in% new_markers))
		drop=gsub("V", "", drop)
		print ("--9")
		# Droping the markers	
		print(drop)
		cross_data = drop.markers(cross_data, drop)
		# Writing the clustered markers to a pecial helper file for future modification of the file Markers_pos.txt
		new_markers=gsub("V", "", new_markers)
		print ("--10")
		clustered_markers=c(clustered_markers, as.numeric(new_markers))
		write.table(as.numeric(new_markers), file = paste(tmp_folder,"clustered_markers_helper_file.txt", sep=""), row.names=FALSE, col.names=FALSE, append=TRUE)
		write.table(new_clusters_all, file = paste(tmp_folder,"clustered_markers_helper_file2.txt", sep=""),  row.names=T, col.names=F, append=TRUE)
		new_clusters_all_2=c(new_clusters_all_2,new_clusters_all)
		print ("--11")
	}else {
		new_markers=gsub("V", "", markernames(cross_data, chr_cluster))
		print (new_markers)
		clustered_markers=c(clustered_markers, as.numeric(new_markers))
		write.table(as.numeric(new_markers), file = paste(tmp_folder,"clustered_markers_helper_file.txt", sep=""), row.names=FALSE, col.names=FALSE, append=TRUE)
		write.table(new_clusters_all, file = paste(tmp_folder,"clustered_markers_helper_file2.txt", sep=""),  row.names=T, col.names=F, append=TRUE)
		cat ("!CLUSTERING MARKERS:			There is only one Marker on the chromosome", chr_cluster,". Therefore it cant be clustered\n")
	}
}
######################################################################################################
#  Excluding the clustered markers from the markers_only file
print (dim(markers_only))
print (clustered_markers)
markers_only=markers_only[,as.numeric(clustered_markers)]
print (dim(markers_only))
#print (markers_only)
# In case the multivariate method Random Forest was used we need to estimate the Bias
print ("!!1")
if (eQTL_method %in% c(0,1,3,8))
{

#	library(foreach)
#	library(doMC)
#	registerDoMC(Number_of_threads_Clustering)	

	print ("starting the calculation for the bias correction for the multivariate Random forest method")
	# Doing a bais correction of the SF score
	source(paste(function_folder,"rf_functions.r", sep="")) # fuctions from the Michaelson paper
#	ptime <- system.time({corr_bias=estBias(markers_only, ntree=ntree_v, verbose = TRUE, multicore=FALSE)})
	print ("!!1")
	corr_bias=estBias(markers_only, ntree=ntree_v, verbose = TRUE)
	print (corr_bias)
	#ptime <- system.time({corr_bias=estBias(markers_only, ntree=ntree_v, verbose = TRUE)})
	#print (ptime)
	# caving the bias correction 
	save(corr_bias, file=paste(tmp_folder,"bias_corr_RF",sep=""))
}
##########################
# writing the modified genotype file after clustering
print ("printing the file markers_only_clusterd.txt")
write.table(as.matrix(markers_only), file = paste(tmp_folder,"markers_only_clusterd.txt",sep=""), sep="\t", col.names=TRUE, row.names=FALSE)
# writing the modified Marker_pos file
#removing the monomorphic and NA markers from the Marker_pos_file
drop_all=c(dropNames, habra_store)
Marker_pos=Marker_pos[-as.numeric(drop_all),]
write.table(Marker_pos, file = paste(tmp_folder,"Marker_pos_excluded_rows.txt",sep=""), sep="\t", col.names=TRUE, row.names=FALSE)
## calculating the new Marker_positions_file for clustered markers

print ("printing the modified file with clustered Marker positions")
aa=paste("Marker","chr","PP","Mean", sep="\t")
write.table(aa, file = paste(tmp_folder,"Markers_pos_clustered.txt", sep=""), row.names=FALSE, col.names=FALSE)
names(new_clusters_all_2)=gsub("V","",names(new_clusters_all_2))
print (new_clusters_all_2)
print (new_clusters_all)
for (chr_name in unique(Marker_pos$chr)){print (chr_name)}
for (chr_name in unique(Marker_pos$chr))
{
	print (chr_name)
	control_Markers=Marker_pos[Marker_pos$chr==chr_name,]$Marker
	new_clusters_all_for_chr=new_clusters_all_2[which(names(new_clusters_all_2) %in% control_Markers)]
	print (control_Markers)
	print (new_clusters_all_for_chr)
	if (length(new_clusters_all_for_chr)==0)
	{
		print ("-----")
		print (control_Markers)
		print (chr_name)
		print(Marker_pos[which (Marker_pos$Marker==as.numeric(control_Markers)),])
		Pos_1=Marker_pos[which (Marker_pos$Marker==as.numeric(control_Markers)),]$PP
		to_add=paste(control_Markers, chr_name, Pos_1,Pos_1)	
		write.table(to_add, file = paste(tmp_folder,"Markers_pos_clustered.txt", sep=""), row.names=FALSE, col.names=FALSE, append=TRUE)
	}else{
		print (control_Markers)
		print (new_clusters_all_for_chr)
		for (clst in 1:as.numeric(tail(new_clusters_all_for_chr,1)))
		{
			markers_to_clust=names(new_clusters_all_for_chr[which(new_clusters_all_for_chr==clst)])
			clst_name=markers_to_clust[1]
			mark_pos_part=Marker_pos[Marker_pos$Marker %in% markers_to_clust,]
			first_pos=as.numeric(mark_pos_part$PP[1])
			last_pos=as.numeric(tail(mark_pos_part$PP, 1))
			chr_m=mark_pos_part$chr[1]
			new_marker_pos=paste(first_pos,"-",last_pos,sep="") 
			to_add=paste(clst_name, chr_m, new_marker_pos,mean(c(first_pos,last_pos)))
			write.table(to_add, file = paste(tmp_folder,"Markers_pos_clustered.txt", sep=""), row.names=FALSE, col.names=FALSE, append=TRUE)
			
		}
	}
}
#write(as.matrix(markers_only), file = paste(tmp_folder,"markers_only_clusterd.txt",sep=""), sep="\t", ncolumns=length(markers_only), header=TRUE)
# establishing a new map
print ("calculating the genetic map of the cross data")
newmap <- est.map(cross_data, verbose=T, n.cluster=Number_of_threads_Clustering)
cross_data <- replace.map(cross_data, newmap)
# writing a cross data with a new map to a file
print ("writing cross_file_clustered_markers.csv and cross_file_clustered_markers.R")
print (paste(output_folder,"cross_file_clustered_markers",sep=""))
write.cross(cross_data, format="csv", paste(output_folder,"cross_file_clustered_markers",sep=""))
save(cross_data, file=paste(tmp_folder,"cross_file_clustered_markers.R",sep=""))
#######

