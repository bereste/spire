#!/bin/bash
usage()
{
cat << EOF
usage: $0 options


++++     ------------------------------------------------------------------------------------     ++++
++++                               	SPIRE 				                          ++++
++++            Software for Polymorphism Identification Regulating Expression                    ++++
++++     ------------------------------------------------------------------------------------     ++++

Spire is a pipeline for quantitative trait locus analysis using expression data as phenotypes (eQTL). It provides a framework for analyzing eQTLs by using different univariate and multivariate methods and integrating useful tools for the pre- and the post-analysis of the results.


OPTIONS:
   -h           Show this message
   -x           (!) Define the path to the config file. Please make sure, that all of the options are set correctly
-----------------------------------------------
-----------------------------------------------
   -m           SPECIAL. Use two libraries.
Use two two closely related sets on genes instead of one (e.g. two different strains of one species). In this case the read mapping will be performed for both of the libraries separately, thus producing individual expression values for both libraries. (Further options can be set in the configuration file)
       -k        SPECIAL. Two gene libs. Preselections already done.
(If you have two gene libraries (-m)) If the preselection of the expression values for all the genes from the two libraries has already been done, provide the file with the results

   -n        SPECIAL. Selective Normalization.
Exclude one or more short-read files from the normalization (can be defined in the config file)

   -o           SPECIAL. SNP Tagging.
Select TagSNPs by clustering the genotype markers on each chromosome depending on the genotype across the samples. Speeds up the eQTL mapping.

#REMOVE THIS   -p        SPECIAL. Perform SNP-taging according to their linkage disequilibrium (LD)

   -s         SPECIAL. Separate chromosomes.
The genotype data will be processed one chromosome at a time. This will speed up the R processing of the data and the eQTL calculation

   -v        SPECIAL. Precalculated R-Object.
If you already have an precalculated R-object (R/QTL package) with the dataset you can use it for the eQTL mapping.


   -r        SPECIAL. RIL
Select if your dataset consists of recombinant inbred lines (RIL)

   -w        SPECIAL. Gene selection for eQTL
If you only need certain gene name tested for eQTL, supply a list with the names. (One name, one line)

   -t         DEBUGING. In case clustering was already prerun, skip the clustering, but use the already generated output and the defined R object
   -u        DEBUGING. (multivariate RF method)In case clustering was already prerun use the already generated. Provide the R object with the values for the p_val correction
-----------------------------------------------
-----------------------------------------------
   -a           PREPROCESSING.  Read filtering.
Read quality filtering (fastx). Adapter clipping (far2). All detailed options can be set in the options file
Necessary input data: Expression data. 

       -b           PREPROCESSING. Barcode splitting.
Multiplexed reads are separated according to their respective barcode(requires -a)
Necessary input data: Expression data. 

    --------------------
   -d           MAIN. Read coverage.
Generates a read coverage file for all genes present in the given library
Necessary input data: Expression data, Gene Sequences.

   -e           MAIN. Normalization.
Normalization of the read count
Necessary input data: Expression data.

   -f           MAIN. Further Statistical plots (MA_plot...

   -g           MAIN. eQTL analysis.
eQTL mapping (Please adjust the settings for this function inside the options file)
Necessary input data: Normalized expression data, Gene Annotations, Genotypes. 

   -q        MAIN. External methods
Include external eQTL-mapping methods. (For description see below.)

   -i           MAIN. The expression of Target genes of miRNAs is plotted (non-generic)

   -j           MAIN. Hotspot.
Calculation of hotspots of significant QTLs. Furthermore cis/trans classification of found QTLs is calculated. 
Necessary input data:  eQTL mappinf results, Gene annotations.


-----------------------------------------------

-q Inculde external eQTL mapping methods

The pipeline can use a method provided externally by the user for the eQTL mapping. It has to be written accepting the following input format:


To use it follow the following steps:
A bash (.sh) script file must be supplied and has to be present in the $"script_dir" folder.
In the $"dataset_dir" the following file has to be present: external_eqtl_methods.txt
There the script will parse for the name of the algorithm file and an algorithm name. The format is the following (Tab separated, each line corresponds to one algorithm):

script_name1.sh    The Name of the algorithm1
script_name2.sh    The Name of the algorithm2
.
.
.
script_name_n.sh    The Name of the algorithm_n

The methods will than be executed.






Contact:
Ivan Kel. Institute for Biomedical Technology – National Research Council of Italy
ivan.kel@itb.cnr.it

EOF
}

options_file="./options_file.cfg"
Preproc=0
Barcode_split=0
Main=0
Read_miRNA_coverage=0
Normalization=0
Statistical_analysis=0
eQTL_mapping=0
target_expression=0
cis_trans=0
cross_data_object_ready=0
DUAL_BIB_MODE=0
CLUSTER_MARKERS=0
EXCLUDE_NORM=0
RIL_USED=0
SNP_TAG=0
ONE_CHR_PROC=0
CLUST_SPEC=0
CLST_R_OBJ=0
RF_BIAS_OBJ=0
cross_data_object=0
EXPRESSION_SEL_DONE=0
normalized_coverage_file_for_eQTL_prov=0
LIST_of_gene_NAMES=0
gene_names_LIST_used=0

while getopts “hx:abdefgijmnopqrst:u:v:k:w:” OPTION
do
     case $OPTION in
         h)   
             usage
             exit 1
             ;;   
         x)   
             options_file=$OPTARG
             ;;   
         a)   
             Preproc=1
             ;;   
         b)   
             Barcode_split=1
             ;;
#         c)
#             Main=1
#             ;;
         d)
             Main=1
             Read_miRNA_coverage=1
             ;;
         e)
             Main=1
             Normalization=1
             ;;
         f)
             Main=1
             Statistical_analysis=1
             ;;
         g)
             Main=1
             eQTL_mapping=1
             ;;
         i)
             Main=1
             target_expression=1
             ;;
         j)
             Main=1
             cis_trans=1
             ;;
         m)
             DUAL_BIB_MODE=1
             ;;
         n)
             EXCLUDE_NORM=1
             ;;
         o)
             CLUSTER_MARKERS=1
             ;;
         p)
             SNP_TAG=1
             ;;
         q)
             extr_meth=1
             ;;
         r)
             RIL_USED=1
             ;;
         s)
	     ONE_CHR_PROC=1
             ;;
         t)
	     CLUST_SPEC=1
	     CLST_R_OBJ=$OPTARG
             ;;
         u)
	     CLUST_SPEC=1
	     RF_BIAS_OBJ=$OPTARG
             ;;
         v)
	     cross_data_object_ready=1
	     cross_data_object=$OPTARG
             ;;
         k)
	     EXPRESSION_SEL_DONE=1 
	     normalized_coverage_file_for_eQTL_prov=$OPTARG  
             ;;
	 w)  
	     gene_names_LIST_used=1
             LIST_of_gene_NAMES=$OPTARG 
             ;;  
         ?)
             usage
             exit
             ;;
     esac
done

echo -e "\e[00;31m############################################################################\e[00m"
echo -e "\e[00;31m---------------------------- Starting the Script ---------------------------\e[00m" 
echo "Checking the config file..."
# checking if the config file exists
if [ ! -f "$options_file" ]
then
    echo -e "\e[00;31mERROR\e[00m: The config file" "$options_file" "does not exist. Please make sure to provide a valid config file\n"
	usage
	exit
fi;

# checking if the config file is valid
if  egrep -q -v '^ |^$|^#|^[^ ]*=[^;]*' "$options_file"; then
  echo -e "\e[00;31mERROR\e[00m: ...Config file is unclean. The file can only contain comments and variable declarations" 
	echo "The file contains following unclean lines: "
	 egrep -v -n '^ |^$|^#|^[^ ]*=[^;]*' "$options_file"
  exit
else
  echo "...Config file is valid"
  source "$options_file"
fi;

## setting the plots folder 
plot_dir=$(echo -e ${dataset_dir}"plots/")


## creating a tmp folder for tmp output
tmp_folder_name=output_dir_tmp_$(date +%F-%H%M%S)
tmp_folder_path=$(pwd)/$tmp_folder_name
mkdir $tmp_folder_name
echo -e "Creating folder for temporary files (\e[00;31m"${tmp_folder_name}"\e[00m)"

# initializing the output file
output_process_file="output_process_$(date +%F-%H%M%S).txt"

echo -e "#########################################################################" >> ${output_dir}/${output_process_file}
echo -e "#############--------------- Run starts ------------------###############" >> ${output_dir}/${output_process_file}
echo -e "--- Starting time: $(date +%F-%T)\n\n" >> ${output_dir}/${output_process_file}
echo "Parameter used: " >> ${output_dir}/${output_process_file}
echo -e "(-x) options_file= ${options_file}\n
(-a) Preproc=${Preproc}
(-b) Barcode_split=${Barcode_split}
(-c) Main=${Main}
(-d) Read_miRNA_coverage=${Read_miRNA_coverage}
(-e) Normalization=${Normalization}
(-f) Statistical_analysis=${Statistical_analysis}
(-g) eQTL_mapping=${eQTL_mapping}
(-i) target_expression=${target_expression}
(-j) cis_trans=${cis_trans}
(-m) DUAL_BIB_MODE=${DUAL_BIB_MODE}
(-l) CLUSTER_MARKERS=${CLUSTER_MARKERS} 
(-l) SNP_TAG=${SNP_TAG} 
(-n) EXCLUDE_NORM=${EXCLUDE_NORM}\n" >> ${output_dir}/${output_process_file}

echo -e "Folder for temporary files: "${tmp_folder_name} >> ${output_dir}/${output_process_file}
echo -e "#########################################################################" >> ${output_dir}/${output_process_file}







### some walkaround. Need to change it
# (Just variable naming issues)
miRNA_lib=$gene_lib
miRNA_lib2=$gene_lib2














# ---------------------------------------------------- preprocessing block --------------------------------------------------
if [ "$Preproc" -eq 1 ];then				
		
	echo -e "\n\n\n------------------------------Starting the preprocessing block-------------------------------------"  >>  ${output_dir}/${output_process_file}
	echo -e "\n\n\n------------------------------\e[00;34mNOTE:\e[00m Starting the preprocessing block-------------------------------------"
	# ------------------------ Splitting reads by their barcodes if needed ----------------------

	${scripts_dir}preproccesing_part.sh ${Number_of_threads_barcode} ${Memory_barcode} ${tmp_folder_path} $read_file $barcodes_for_read ${output_name} $barcode_seq ${dataset_dir} ${q_par} ${p_par} ${Memory_fastx} ${Min_overlap_far2_1} ${End_far2_1} ${Number_of_threads_far2} ${Memory_far2} $adapters ${Min_overlap_far2_2} ${End_far2_2} ${output_dir} ${output_process_file} ${Barcode_split} "${all_files_ready[@]}"
	# This file is read into the bash environment of the parent script. (preprocessing_part.sh as child script cannot change the global variable, so I need to implement this workaround)
	source ${tmp_folder_path}/all_files_ready_update.txt
	
fi;								


# ------------------------------------------------------------------- Main Algorithm -------------------------------------------------------------------------
# ---- two possibilities. 1) preprocessing has been done on the files 2) no preprocessing has been done on the files 
# ---- in both cases algorithm receives the array: $all_files_ready, containing the names of the files, which are located in the $output_dir
# --------------------------------------------------------------------


if [ "$Main" -eq 1 ];then
	echo -e "\n\n\n------------------------\e[00;34mNOTE:\e[00m The main part of the pipeline starts here---------------------------------"	
	echo -e "\n\n\n------------------------------The main part of the pipeline starts here-------------------------------------" >>  ${output_dir}/${output_process_file}	
	mkdir -p ${plot_dir}
	

	## --------------------------------------- Converting the reads to fasta file, collapsing the reads and mapping the reads to the miRNA library -------------------------
	if [ "$Read_miRNA_coverage" -eq 1 ]; then
		echo "Mapping of the reads "
		echo "	----------------------------------------------------------READ-MAPPING------------------------------------------" >> ${output_dir}/${output_process_file}	
		echo "${all_files_ready[@]}"
		
		echo "	Mapping of the reads to the gene library: ${miRNA_lib} using mapping method number $mapping_software (Requesting ${Memory_read_mapping} RAM on $Number_of_threads_read_mapping threads)" >> ${output_dir}/${output_process_file}	
		${scripts_dir}read_mapping_part.sh "${miRNA_lib}" "${Memory_read_mapping}" "${output_dir}" "${output_process_file}" "${Preproc}" "${PATH}" "${tmp_folder_path}" "${scripts_dir}" "${dataset_dir}" "${DUAL_BIB_MODE}" "${miRNA_lib2}" $mapping_software $Number_of_threads_read_mapping "${all_files_ready[@]}"
	fi;	
	


	## ------------------------------------- Normalization of data (and also plotting) --------------------------------------
	if [ "$Normalization" -eq 1 ];then
		echo -e "\n	----------------------------------------------------------NORMALIZATION------------------------------------------" >> ${output_dir}/${output_process_file}	
		${scripts_dir}normalization_part.sh "${norm_meth}" "${output_dir}" "${output_process_file}" "$DUAL_BIB_MODE" "$miRNA_lib" "$miRNA_lib2"  "$norm_func_file" "${tmp_folder_path}" "$EXCLUDE_NORM" "$both_libs" "${scripts_dir}" "${plot_dir}" "${excluding_normalization[@]}"
		# This file is read into the bash environment of the parent script. (child scripts cannot change a global variable, so I need to implement this workaround)
		source ${tmp_folder_path}/normalized_coverage_file_update.txt
	fi;



	## ------------------------------------- Doing further statistical analysis ------------------------------------
	if [ "$Statistical_analysis" -eq 1 ];then
		echo "  ----------------------------------------------------------STATISTICAL PLOTS------------------------------------------" >> ${output_dir}/${output_process_file}
		${scripts_dir}stat_part.sh "${miRBase_conservations}" "${miRNA_lib}" "${normalized_coverage_file}" "$tmp_folder_path" "$DUAL_BIB_MODE" "$miRNA_lib2"  "$output_dir" "${output_process_file}" "$scripts_dir" "$cons_lib" $plot_dir $both_libs
	fi;




	## ----------------------------------- A eQTL mapping of given markers onto the expression of the genes  --------------------------------------------
	if [ "$eQTL_mapping" -eq 1 ];then

		echo "  ----------------------------------------------------------eQTL mapping------------------------------------------" >> ${output_dir}/${output_process_file}
		echo $cross_data_object_ready $cross_data_object
		${scripts_dir}eQTL_part.sh $normalized_coverage_file $output_dir $output_process_file $all_genotype_Maker $SNP_TAG $tmp_folder_path $gene_annotations $DUAL_BIB_MODE $expr_selection_method $scripts_dir $miRNA_lib $miRNA_lib2 $RIL_USED $Memory_Clustering $output_name_clustering $plot_dir $eQTL_method $ntree_v $dataset_dir $Memory_eQTL $Number_of_threads_eQTL $output_name_eQTL $step_v $error_prob_v $map_function_v $stepwidth_v $n_draws_v $n_perm_v $sign_level $sign_level_FDR $use_FDR $CLUSTER_MARKERS $Memory_eQTL_FDR $Number_of_threads_eQTL_FDR $output_name_eQTL_FDR $Memory_effectplot $output_name_effectplots $nforest_v $sign_level_FDR $drop $ONE_CHR_PROC $Number_of_threads_Clustering $CLUST_SPEC $CLST_R_OBJ $Memory_eQTL_RF $Number_of_threads_eQTL_RF $RF_BIAS_OBJ $Number_of_cores_eQTL_RF $cross_data_object_ready $cross_data_object $Number_of_machienes_eQTL $Memory_eQTL_preload $EXPRESSION_SEL_DONE $normalized_coverage_file_for_eQTL_prov $Number_of_threads_eQTL_preload $LIST_of_gene_NAMES $gene_names_LIST_used

		source ${tmp_folder_path}/identified_QTLs_file_update.txt	
		source ${tmp_folder_path}/identified_QTLs_file_update.txt
	fi;



		###########################################################################################################################################################
	## ---------------------------------------------------------------------- cis/trans plots -------------------------------------
	# cis/trans classification of QTLs
	if [ "${cis_trans}" -eq 1 ]; then
		echo "  ----------------------------------------------------------cis/trans------------------------------------------" >> ${output_dir}/${output_process_file}
		
		echo  $identified_QTLs_file $output_dir $output_process_file $scripts_dir $Marker_pos_variable $gene_annotations $plot_dir
		${scripts_dir}cis_trans_part.sh $identified_QTLs_file $output_dir $output_process_file $scripts_dir $Marker_pos_variable $gene_annotations $plot_dir ${tmp_folder_path} $bin_step $p_val_hotspot
	fi;



	# qtl_file.txt > qtl_results.txt is done (yet) manually. Just need to switch column positions and rename the columns
	# awk -F "\t" '{print $1FS$1FS$6FS$5FS$3}' qtl_file.txt > qtl_results.txt
	# Keep in mind, taht the file qtl_results.txt_RF needs to be adjusted: the culumns must be moved to match the qtl_results.txt file
	
	#target_expression
	if [ "${target_expression}" -eq 1 ]; then
		echo "		Target expression stuff"
		
		${scripts_dir}target_exp.sh "/data/bioinformatics/Collaborations/Hawaii/Pictar_v18_RIna_novel_all/pictar_all/" qtl_results.txt "/data/bioinformatics/Collaborations/Hawaii/Rockman_RIL/RILGeneExpressionData.R" cross_data_new_map.R ${scripts_dir} qtl_results_method3/
	
	fi;



		# Plots of the Density function of the Expression of the Targets of miRNAs with significant QTLs


	# Generating the overall file with RF and legacy methods output in one file.
	# cat QTL_all.txt_* | sed 's/ /\t/g' QTL_all.txt | grep -v QTL_name > tmp2
	# head -1 QTL_all.txt | sed 's/ /\t/g' >tmp1
	# cat tmp1 tmp2 > tmp3
	# mv tmp3 QTL_all.txt
	# awk '{print $1"\t"$2"\t"$4"\t"$3"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' QTL_all_RF.txt_RF > QTL_all_RF.txt_RF_right_order
	# awk '(NR>1){print $1"\t"$2"\t"$4"\t"$3"\t""\"RF\"""\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' QTL_all_RF.txt_RF > QTL_all_RF.txt_RF_right_order
	# cat QTL_all.txt QTL_all_RF.txt_RF_right_order > ALL_qtl.txt 
	
fi;	

# TODO: 
# 	- implement generation of statistics about read numbers (overall, filterd, mapped etc)

