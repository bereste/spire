#!/bin/bash

#%%%% HEAD %%%%#
#/-
#%% this script runs the following R scripts
# - eQTL mapping using the multivariate methods (Random Forest)
# - (if desired) a FDR correction of the resulting p_values of QTLs
# - effectplots of the found QTLs
# The main output is a qtl summary file

# To run this method a multicore processor or a cluster system is strongly recomended.
# running the first part of the script (destributing the analysis over several cores)


use_variable=$1
Markers_only=$2
cross_file_variable=$3 
plot_folder=$4 
gene_annotations=$5
Marker_pos_variable=$6 
nforest_v=$7 
tmp_folder_path=${8} 
Clustering_used=${9}
first_pos_check=${10}
Memery_eQTL_RF=${11}
output_name_eQTL=${12}
scripts_dir=${13}
output_dir=${14}
output_process_file=${15}
gene_nr=${16}
Number_of_threads_eQTL_RF=${17}
RIL_used=${18}
ntree_v=${19}
sign_level_FDR=${20}
markers_on_chromosomes=${21}
univar_run=${22}
Number_of_cores_eQTL_RF=${23}
cross_data_object_ready=${24}
cross_data_object=${25}



#%%%% BODY %%%%#
#/-
############# #Running the eQTL calculations
echo -e "\n  \e[00;35m ---------------------------------\n                      NOTE\e[00m: Starting eQTL-mapping using multivariate methods (Random Forest)\n"
echo -e "\n ---------------------------------\n                       NOTE: Starting eQTL-mapping using multivariate methods (Random Forest).\n                    cross_file is ${cross_file_variable}" >> ${output_dir}/${output_process_file}

# The number of phenotypes submetted varies per used thread. 
first_number=1
cores_used=0
step_size=$(($gene_nr/$Number_of_threads_eQTL_RF))
if [ "$step_size" -lt 1 ];then
	step_size=1
fi;
for second_number in $(seq 1 $step_size $gene_nr |awk '(NR>1){print $0}')
do
	echo -e "			submitting job with phenotypes from $first_number to $second_number"
	echo -e "			submitting job with phenotypes from $first_number to $second_number" >> ${output_dir}/${output_process_file} 
	qsub -l "h_vmem=${Memery_eQTL_RF}" -pe smp $Number_of_cores_eQTL_RF -wd "${tmp_folder_path}/" -N ${output_name_eQTL}_multiVar -v PATH=$PATH -o ${job_name}.${ojob_id} -b y "Rscript ${scripts_dir}random_forest_eQTL_multithread_firstpart.r $Markers_only $cross_file_variable $plot_folder $first_number $second_number $gene_annotations ${Marker_pos_variable} $nforest_v "${tmp_folder_path}/" $Clustering_used $RIL_used $scripts_dir $ntree_v $Number_of_cores_eQTL_RF"
	first_number=$(($second_number+1))
	cores_used=$(($cores_used+1))
	sleep 2
done
# Submitting the rest
if [ $(($gene_nr-$second_number)) != 0 ]; then
	echo -e "			submitting job with phenotypes from $(($second_number+1)) to $gene_nr"
	echo -e "			submitting job with phenotypes from $(($second_number+1)) to $gene_nr" >> ${output_dir}/${output_process_file}
	qsub -l "h_vmem=${Memery_eQTL_RF}" -pe smp $Number_of_cores_eQTL_RF -wd "${tmp_folder_path}/" -N ${output_name_eQTL}_multiVar -v PATH=$PATH -o ${job_name}.${ojob_id} -b y "Rscript ${scripts_dir}random_forest_eQTL_multithread_firstpart.r $Markers_only $cross_file_variable $plot_folder $(($second_number+1)) $(($first_pos_check-2)) $gene_annotations ${Marker_pos_variable} $nforest_v ${tmp_folder_path}/ $Clustering_used $RIL_used ${scripts_dir} $ntree_v $Number_of_cores_eQTL_RF"
	cores_used=$(($cores_used+1))
fi;

echo -e "                       The eQTL has been started (Memory: ${Memery_eQTL_RF}, Cores: ${Number_of_threads_eQTL_RF}, submited jobs: $cores_used)." >> ${output_dir}/${output_process_file}

# The grep for the output name it has to be cut at mqx 10 characters: The qstat output supports only 10 char long names. The output files will still be named with the full name
for_greping=$(echo ${output_name_eQTL}_multiVar |sed 's/./&\n/10' |head -1)
R_eQTL_job=$(qstat |grep $for_greping |awk '{print $1}')
while [ "$R_eQTL_job" != "" ]
do
	sleep 80
	R_eQTL_job=$(qstat |grep $for_greping |awk '{print $1}')
	echo -e "waiting for the QTL job:" $R_eQTL_job
	echo -e "Genes processed so far: $(grep gene_marker_pos ${tmp_folder_path}/${output_name_eQTL}_multiVar.o* | wc -l)/$gene_nr"
done

# running the second part of the script (ploting the results)
echo $markers_on_chromosomes
qsub -l "h_vmem=${Memery_eQTL_RF}" -wd "${tmp_folder_path}/" -N ${output_name_eQTL}_multiVar_secPart -v PATH=$PATH -o ${job_name}.${ojob_id} -b y "Rscript ${scripts_dir}random_forest_eQTL_multithread_secondpart.r $plot_folder "$tmp_folder_path/" $RIL_used $cross_file_variable $Clustering_used $sign_level_FDR $output_dir $univar_run $Marker_pos_variable "${markers_on_chromosomes}""

for_greping=$(echo ${output_name_eQTL}_multiVar_secPart |sed 's/./&\n/10' |head -1)
R_eQTL_job=$(qstat |grep $for_greping |awk '{print $1}')
while [ "$R_eQTL_job" != "" ]
do
	sleep 50
	R_eQTL_job=$(qstat |grep $for_greping |awk '{print $1}')
	echo -e "waiting for the QTL job:" $R_eQTL_job
done

sed 's/ /\t/g' ${output_dir}QTLs_identified_multivariant_FDR.txt | awk '/QTL/{gsub(/"/,"")};{print}' > ${tmp_folder_path}/tmp
mv ${tmp_folder_path}/tmp ${output_dir}QTLs_identified_multivariant_FDR.txt

############################################### QTL analysis is ready
