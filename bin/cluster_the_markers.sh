#!/bin/bash

#%%%% HEAD %%%%#
#/-
#%% This script clusters the markers (if desired so by the user). The markers are clustered hierarchically according to their genotype. Markers with the same genotype over all 
#%% samples are considered as one, as the eQTL mapping cannot distinguish between them. 



cross_file_variable=$1
gene_nr=$2
Number_of_markers=$3
RIL_USED=$4
output_dir=$5
tmp_folder_path=$6
scripts_dir=$7
plot_dir=$8
output_process_file=${9}
eQTL_method=${10}
ntree_v=${11}
Marker_pos_variable=${12}
Markers_only=${13}
Number_of_threads_Clustering=${14}
markers_on_chromosomes=${15}
#-/

#%%%% BODY %%%%#

#/-
# Running the clustering + eQTL mapping
echo $markers_on_chromosomes
Rscript ${scripts_dir}clustering_markers_all.r $Markers_only ${output_dir} $cross_file_variable ${scripts_dir} $RIL_USED "${tmp_folder_path}/" ${plot_dir} $eQTL_method $ntree_v ${Number_of_threads_Clustering} $Marker_pos_variable "${markers_on_chromosomes}"

sed 's/"//g' ${tmp_folder_path}/Markers_pos_clustered.txt | sed 's/ /\t/g' > ${tmp_folder_path}/tmp
mv ${tmp_folder_path}/tmp ${output_dir}Markers_pos_clustered.txt


### modifying the file with the markers positions (Marker_pos.txt)
#clst_helper=${tmp_folder_path}/clustered_markers_helper_file.txt
## reformating the helper file into a special format
#awk '(NR>1){print $0}' $clst_helper |sed 's/$/\t/g' |sed 's/^/^/g' |sed 's/"//g' > $tmp_folder_path/clustered_markers_tmp.txt
#grep -f $tmp_folder_path/clustered_markers_tmp.txt ${Marker_pos_variable} -B1 -b| awk '(NR>1){print $0}'  |grep "\-\-" -v | sed 's/[1234567890]*:/:/g'|sed 's/[1234567890]*-/-/g'> $tmp_folder_path/markers_tmp_format
#tail -1 $Marker_pos_variable | sed 's/^/-/g' >> $tmp_folder_path/markers_tmp_format
## running a python script, which creates the new Marker_pos with with clustered markers
#python ${scripts_dir}modifying_marker_pos.py $tmp_folder_path/markers_tmp_format $output_dir
## Sorting the file and placing it in the right directory
#sort -g ${output_dir}Markers_pos_clustered.txt > $tmp_folder_path/Markers_pos_clustered.tmp
#mv $tmp_folder_path/Markers_pos_clustered.tmp ${output_dir}Markers_pos_clustered.txt
#-/
# END
