# This script gets a gene with its positions and the file with all Marker (Genotyped SNP) position. 
# It finds the proximate Marker to the given gene
# In case there are several markers inside one gene, the first one returned

import sys 

def prox_marker():


	Marker_positions = open("%s"%(sys.argv[1]),"rU")
	miRNA_position = open("%s"%(sys.argv[2]),"rU")


	chr_rom=["I","II","III","IV","V","VI","VII"]
	chr_change=["1","2","3","4","5","6","7"]
	marker_dict={} #key: Marker_index, value:[chr,pos] 
	# miRNA_pos := [chr, start, end]
	prox_markers=[]

	# converting romic letteres to numbers
	for miRNA_line in miRNA_position:
		miRNA_pos=miRNA_line.split("\t")
		miRNA_pos[2]=miRNA_pos[2].replace("\n","")
		for num,i in enumerate(chr_rom):
			if i==miRNA_pos[0]:
				miRNA_pos[0]=chr_change[num] 	

			

	# reading Marker position into a dictionary
	for line in Marker_positions:
		line_split=line.split("\t")
		marker_dict[line.split("\t")[0]]=[line.split("\t")[1],line.split("\t")[2]]	




	# seaching the markers for the nearest marker to the miRNA position
	first=0
	for marker in marker_dict:
		if marker_dict[marker][0] == miRNA_pos[0]:
			# setting the reference the first time
			if first==0:
				current_dist_up= int(marker_dict[marker][1]) - int(miRNA_pos[1])
				current_dist_down= int(miRNA_pos[2]) - int(marker_dict[marker][1])
				first=1
			# if a marker lies inside the gene
			if int(miRNA_pos[1]) > int(marker_dict[marker][1]) and int(miRNA_pos[2]) < int(marker_dict[marker][1]):
				prox_markers.append(int(marker))			
			# if the marker is upstream of the gene
			elif (int(marker_dict[marker][1]) - int(miRNA_pos[1])) <= current_dist_up and (int(marker_dict[marker][1]) - int(miRNA_pos[1]))>=0: 
				current_dist_up=int(marker_dict[marker][1]) - int(miRNA_pos[1])	
				marker_name_up=int(marker)
			elif (int(miRNA_pos[2]) - int(marker_dict[marker][1])) <= current_dist_down and (int(miRNA_pos[2]) - int(marker_dict[marker][1]))>=0:
				current_dist_down=int(miRNA_pos[2]) - int(marker_dict[marker][1])	
				marker_name_down=int(marker)


			
				
	if current_dist_up == current_dist_down:
		prox_markers.append(marker_name_up,marker_name_down)			
	else: 
		if max(current_dist_up,current_dist_down)==current_dist_up:
			prox_markers.append(marker_name_up)
		else:
			prox_markers.append(marker_name_down)

	# converting the output into fitting format
	for num,ii in enumerate(prox_markers):
		if marker_dict[str(ii)][0] == "X":
			new_name=("%s_%s"%(marker_dict[str(ii)][0],marker_dict[str(ii)][1].replace("\n","")))
		else:
			new_name=("X%s_%s"%(marker_dict[str(ii)][0],marker_dict[str(ii)][1].replace("\n","")))
		prox_markers[num]=new_name	
	print ','.join(prox_markers)

prox_marker()
