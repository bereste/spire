#%%%% HEAD %%%%#
#/-
#%% This is the first part of the script, which runs the eQTL mapping using the multivariate method Random Forest (started from multivariate_methods.sh)
#%% The splitting is needed because of the multiprocessing

# loading libraries
suppressMessages(library(randomForest))
suppressMessages(library(eqtl))
suppressMessages(library(snow))
suppressMessages(library(car))
suppressMessages(library(foreach)) # multicore stuff
suppressMessages(library(doSNOW))  # multicore stuff
suppressMessages(library(snow))
#suppressMessages(library(Hmisc)) # used for the %nin% operator

# reading the parameters
args <- commandArgs(TRUE)
Markers_num <- args[1] # matrix with Markers transformed into nummeric(0/1)
cross_file <- args[2] # needed to extract the matrix of phenotype (expression) value valuess
plot_folder <- args[3] # /data/bioinformatics/Collaborations/Hawaii/QX/RF/
first_trait <- args[4]
last_trait <- args[5]
gene_positions_file <- args[6]
marker_positions_file <- args[7]
nforest_v <- as.numeric(args[8])
tmp_folder <- args[9]
Clustering_used<-as.numeric(args[10])
RIL_used<-as.numeric(args[11])
function_folder<-args[12]
ntree_v<-as.numeric(args[13])
Number_of_cores_eQTL_RF<-as.numeric(args[14])



########### loading the predefined functions for RF method
source(paste(function_folder,"rf_functions.r", sep="")) # fuctions from the Michaelson paper

########### creating the enviroment for the multicore proccesing
###registerDoSNOW(makeCluster(Number_of_cores_eQTL_RF, type="SOCK"))
cl = makeSOCKcluster(Number_of_cores_eQTL_RF)
clusterExport(cl, "rfsf")
ff = function(y, x, ntree) 
{
	rf = randomForest::randomForest(y = y, x = x, ntree = ntree)
	sf = rfsf(rf)
}


################ Reading the needed data
# reading the cross_file
# In case the markers were previously clustered, the R object is loaded, which is faster, than reading a table
if (Clustering_used == 1)
{
        load(paste(tmp_folder,"cross_file_clustered_markers.R",sep=""))
	markers_only=read.table(Markers_num, header=T)
}else{
        cross_data <-read.cross("csv", dir="", file=cross_file, sep="\t");
	markers_only=read.table(Markers_num, header=F)
}
# converting the data to a special format in case it is a RIL dataset
if (RIL_used==1)
{
        cross_data <- convert2riself(cross_data)
}
cross_data

gene_positions <- read.table(gene_positions_file ,header= TRUE, row.names=1, as.is=T);
marker_positions <- read.table(marker_positions_file ,header=TRUE,sep="\t");


############ loading the bias correction of the SF score
load(paste(tmp_folder,"bias_corr_RF",sep=""))

################################################################
### Droping phenotypes to subbmit only the needed phenotypes (Also ignoring the individuals which have not been genotyped (sex==NA))
cross_data=cleanphe(cross_data,phenames(cross_data)[1])
pheno_expr=cross_data$pheno[which(!is.na(cross_data$pheno$sex)),][first_trait:last_trait]
################################################################

#################### starting the computation of the Random forest stuff
P_all = list()
print (names(pheno_expr))
for (i in names(pheno_expr))
{
	cat ("Gene: ",i,"\n")
	gene_name=i
	expr=pheno_expr[i][,1]
		pdf(paste(plot_folder, paste("trait_dist_",i,".pdf",sep=""),sep=""), height=12, width=17)
        	pdens(pheno_expr[i][,1], main = i)
        	dev.off()
	#print (expr)
	print (ntree_v)
	print (length(expr))
	print (dim(markers_only))
		#print (dim(as.matrix(expr)))
		#print (t(as.matrix(expr)))
	
	rf=randomForest(y = expr,x = markers_only, ntree=ntree_v)
		#rf <- foreach(ntree = rep(ntree_v/Number_of_cores_eQTL_RF, Number_of_cores_eQTL_RF), .combine = combine, .packages = "randomForest") %dopar% randomForest(markers_only, expr, ntree = ntree)
		#stopCluster(cl)
	
		#sf = parApply(cl, t(as.matrix(expr)), 1, ff, markers_only, ntree_v)
	sf=rfsf(rf)
	# bias correction
	print (length(sf))
	print (length(corr_bias))
	sf.corr=sf - corr_bias
		#sf.corr=t(sf - corr_bias)
	sf.corr[sf.corr < 0] = 0

	########
	# Generating a distributions of scores under the null-hypothesis
	sf.null = numeric()
	print ("running the impirical p-value calculation")
	for (i in 1:nforest_v) { 
		rf.null=randomForest(y = sample(pheno_expr[gene_name][,1]), x = markers_only, ntree = ntree_v) 
		#rf.null=foreach(ntree = rep(ntree_v/Number_of_cores_eQTL_RF, Number_of_cores_eQTL_RF), .combine = combine, .packages = "randomForest") %dopar% randomForest(y = sample(pheno_expr[gene_name][,1]), x=markers_only, ntree = ntree)
		tmp = rfsf(rf.null) - corr_bias
		tmp[tmp < 0] = 0 
		sf.null = c(sf.null, tmp) 
		print (i)
	}  
	pdf(paste(plot_folder,gene_name, "_null_dist.pdf", sep=""), height=12, width=17)
	hist(sf.null, main = "selection frequency under null hypothesis",xlab = "selection frequency", breaks = 200)
	dev.off()

	pnull = ecdf(sf.null)
	P = 1 - pnull(sf)

	######  
	# getting the nearest Marker to the gene
	#gene_pos_uno=gene_positions[gsub("^X","",gene_name),]
	gene_pos_uno=gene_positions[grep(paste(gsub("^X","",gene_name),"$",sep=""), rownames(gene_positions)),]
#	if (is.na(miRNA_pos_uno$chr)){
#		miRNA_pos_uno=miRNA_positions[paste("cel.", miRNA, sep=""),]
#	}
#	if (is.na(miRNA_pos_uno$chr)){
#		miRNA_pos_uno=miRNA_positions[paste("hw.", miRNA, sep=""),]
#	}
#	if (is.na(miRNA_pos_uno$chr)){
#		miRNA_pos_uno=miRNA_positions[paste("cel_", miRNA, sep=""),]
#	}
#	if (is.na(miRNA_pos_uno$chr)){
#		miRNA_pos_uno=miRNA_positions[paste("hw_", miRNA, sep=""),]
#	}
#	if (is.na(miRNA_pos_uno$chr)){
#		miRNA_pos_uno=miRNA_positions[paste("hw_hw_", miRNA, sep=""),]
#	}
	print (gene_pos_uno)
	if (dim(gene_pos_uno)[1]==0){
		gene_marker_pos=-1
		gene_Marker=-1
		print (gene_pos_uno,"pos: unknown")
	}else{

		# Calculating the position of the gene to check (in case the markers have been clutered)
		pos_to_check=((as.integer(gene_pos_uno$stop)-as.integer(gene_pos_uno$start))/2)+as.integer(gene_pos_uno$start)
		print (pos_to_check)
		chr_of_gene=(gene_pos_uno$chr)
		print (chr_of_gene)
		chr_pos=subset(marker_positions,marker_positions$chr %in% chr_of_gene )
		print (dim(chr_pos))
		# finding the marker with the shortest distance to the gene position
		line=which(abs(chr_pos$Mean-pos_to_check)==min(abs(chr_pos$Mean-pos_to_check)))
		print (line)
		gene_Marker=chr_pos[line,]$Marker
		print (gene_Marker)
		#calculating the position of the Marker for the further cis/trans plot (-> the position in the grid)
		gene_marker_pos_orig=which(markernames(cross_data) %in% gene_Marker)
		print (gene_marker_pos_orig)
		## Chr X has a special treatment, because of the position
		# Checking as what position the Chr X is listed
		#X_pos=which(chrnames(cross_data) %in% "X")
		#chr_rest=subset (chrnames(cross_data), chrnames(cross_data) %nin% "X")
		#print (chr_rest)
		#if (chr_of_gene != "X" and chr_of_gene != "mtDNA"){gene_marker_pos=gene_marker_pos_orig+(as.numeric(as.character(chr_of_gene))*3)+length(markernames(cross_data,"X"))}else{gene_marker_pos=gene_marker_pos_orig-length(markernames(cross_data,chr_rest))}
		if (chr_of_gene == "X")
		{
			gene_marker_pos=gene_marker_pos_orig		
		}else{
			gene_marker_pos=gene_marker_pos_orig+(as.numeric(as.character(chr_of_gene))*3)		
		}
		cat ("gene_marker_pos:",gene_marker_pos,"\n")
		
	}
	######


	# saving a binary files with the information. The second part of the script will then collect and merge them for further analysis
	P_all$gene_name=c(P_all$gene_name,gene_name)
	P_all$SF=c(P_all$SF,sf.corr)	
	P_all$P = c(P_all$P, P) 
	P_all$gene_marker_pos = c(P_all$gene_marker_pos, gene_marker_pos)
	P_all$gene_Marker= c(P_all$gene_Marker,gene_Marker)

}

save(P_all, file=paste(tmp_folder,"P_all_",first_trait,"_",last_trait,sep=""))

