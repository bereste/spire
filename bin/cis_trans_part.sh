#!/bin/bash

#%%%% HEAD %%%%#
#/-
#%% This script produces the cis/trans plots of the QTL results

# loading the variables
identified_QTLs_file=$1
output_dir=$2
output_process_file=$3
scripts_dir=$4
Marker_pos_variable=$5
gene_annotations=$6
plot_dir=$7
tmp_folder_path=$8
bin_step=$9
p_val_hotspot=${10}


#%%%% BODY %%%%#


#### This script is the part, that does that produces the overview plot of the eQTL results

# cis/trans classification of QTLs
echo -e "       \n\n---------------------------------\nCalculating the cis/trans classification of found significant QTLs"
echo -e "       \n\n---------------------------------\nCalculating the cis/trans classification of found significant QTLs" >> ${output_dir}/${output_process_file}
echo -e "       \n\nUsing the following file to produce the plots: \e[00;32m${identified_QTLs_file}\e[00m"
echo -e "       \n\nUsing the following file to produce the plots: ${identified_QTLs_file}" >> ${output_dir}/${output_process_file}


# running the script
#Rscript ${scripts_dir}cis_trans_part.r ${identified_QTLs_file} ${Marker_pos_variable} ${gene_annotations} ${output_dir} ${plot_dir}

# creating the final file
#cat ${output_dir}QTLs_identified_*_final.txt |  sed 's/"//g' | awk '!x[$0]++' > ${output_dir}QTLs_identified_ALL_final.txt

echo -e "			Writing the final output table"
# post-processing the output and creating a final output file
python ${scripts_dir}summary_table.py ${output_dir} ${Marker_pos_variable}

echo -e "			Starting the hotspot calculations"
#### calculating the hotspots
awk -F "\t" '{print $1"\t"$8}' ${output_dir}QTL_results_file.csv  | sed 's/]//g' | sed 's/\[//g' | sed 's/,/\t/g' | sed '1d' > ${tmp_folder_path}/qtl_results_for_hotspot.txt
Rscript ${scripts_dir}hotspot_part.r ${tmp_folder_path}/qtl_results_for_hotspot.txt ${Marker_pos_variable} ${tmp_folder_path} $bin_step $p_val_hotspot

echo -e "       \n\n---------------------------------\e[00;35m NOTE\e[00m: plotting is ready! The plots can be found in: \e[00;35m${plot_dir}\e[00m \nThe final results can be found here: \e[00;35m${output_dir}QTLs_identified_ALL_final.txt\e[00m"
echo -e "       \n\n---------------------------------NOTE: plotting is ready! The plots can be found in: ${plot_dir} \nThe final results can be found here: ${output_dir}QTLs_identified_ALL_final.txt" >> ${output_dir}/${output_process_file}


