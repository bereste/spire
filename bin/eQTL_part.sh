#!/bin/bash

#### This script is a the part, that does the eQTL mapping 

# Reading the arguments
normalized_coverage_file=${1}
output_dir=${2}
output_process_file=${3}
all_genotype_Maker=${4}
SNP_TAG=${5}
tmp_folder_path=${6}
gene_annotations=${7}
DUAL_BIB_MODE=${8}
expr_selection_method=${9}
scripts_dir=${10}
miRNA_lib=${11}
miRNA_lib2=${12}
RIL_USED=${13}
Memory_Clustering=${14}
output_name_clustering=${15}
plot_dir=${16}
eQTL_method=${17}
ntree_v=${18}
dataset_dir=${19}
Memory_eQTL=${20}
Number_of_threads_eQTL=${21}
output_name_eQTL=${22}
step_v=${23}
error_prob_v=${24}
map_function_v=${25}
stepwidth_v=${26}
n_draws_v=${27}
n_perm_v=${28}
sign_level=${29}
sign_level_FDR=${30}
use_FDR=${31}
CLUSTER_MARKERS=${32}
Memory_eQTL_FDR=${33}
Number_of_threads_eQTL_FDR=${34}
output_name_eQTL_FDR=${35}
Memory_effectplot=${36}
output_name_effectplots=${37}
nforest_v=${38}
sign_level_FDR=${39}
drop=${40}
ONE_CHR_PROC=${41}
Number_of_threads_Clustering=${42}
CLUST_SPEC=${43}
CLST_R_OBJ=${44}
Memory_eQTL_RF=${45}
Number_of_threads_eQTL_RF=${46}
RF_BIAS_OBJ=${47}
Number_of_cores_eQTL_RF=${48}
cross_data_object_ready=${49}
cross_data_object=${50}
Number_of_machienes_eQTL=${51}
Memory_eQTL_preload=${52}
EXPRESSION_SEL_DONE=${53}
normalized_coverage_file_for_eQTL_prov=${54}
Number_of_threads_eQTL_preload=${55}
LIST_of_gene_NAMES=${56}
gene_names_LIST_used=${57}


echo $Number_of_threads_Clustering"_"$CLUST_SPEC"_"$CLST_R_OBJ"_"$Memory_eQTL_RF"_"$Number_of_threads_eQTL_RF"_"$RF_BIAS_OBJ"_"$Number_of_cores_eQTL_RF"_"$cross_data_object_ready"_"$cross_data_object"_"$Number_of_machienes_eQTL "_"$Memory_eQTL_preload "_"$EXPRESSION_SEL_DONE "_"$normalized_coverage_file_for_eQTL_prov "_"


echo -e "\n\e[00;34mNOTE:\e[00m Starting eQTL_mapping.\nGenerating files in the format needed by R package eqtl\nUsing the file with normalized data: \e[00;32m${normalized_coverage_file}\e[00m"
echo -e "	NOTE: Starting eQTL_mapping.\n	Generating files in the format needed by R package eqtl\n	Using the file with normalized data: ${normalized_coverage_file}" >> ${output_dir}/${output_process_file}


# ----------------
#### Preparing files
## Preprocessing genotype data	
echo -e "		Preparing the genotype and annotation files" 
echo -e "		Preparing the genotype and annotation files"  >> ${output_dir}/${output_process_file}
if [ $(echo $(basename $all_genotype_Maker) | awk -F "." '{print $2}') == "txt" ];then
	echo -e "		\e[00;35mNOTE:\e[00m Internal genotype file format provided. \e[00;32m$all_genotype_Maker\e[00m"
	echo -e "		NOTE: Internal genotype file format provided. $all_genotype_Maker"  >> ${output_dir}/${output_process_file}
else
	format=$(echo $(basename $all_genotype_Maker)| awk -F "." '{print $2}') 
	echo -e "		\e[00;35mNOTE:\e[00m Genotype file format provided: $format . processing..."
	echo -e "		NOTE: Genotype file format provided: $format . processing..."  >> ${output_dir}/${output_process_file}
	genotype_preproc.sh $all_genotype_Maker
		
	# After the formating, the $all_genotype_Maker should be updated.
fi;
## SNP Taging if desired
if [ "${SNP_TAG}" -eq 1 ]; then
	echo -e "		\e[00;35mNOTE:\e[00m Calculating Tag SNPs"
	echo -e "		NOTE: Calculating Tag SNPs" >> ${output_dir}/${output_process_file}
fi;

## extracting only used lines (individuals) from the ${all_genotype_Maker} file. It is done in case your file with genotypes consist of more than the individuals you need for analysis
awk '(NR==1){print $0}' ${all_genotype_Maker}> ${tmp_folder_path}/genotype_marker.txt
awk '(NR==1) {print $0}' ${normalized_coverage_file} | sed 's/ \t//g' | sed 's/$/\t/g'| sed 's/\t/\t\n/g' | sed '1d' | sed '$d' > ${tmp_folder_path}/names.txt
grep -f ${tmp_folder_path}/names.txt ${all_genotype_Maker} >> ${tmp_folder_path}/genotype_marker.txt 
mv ${tmp_folder_path}/genotype_marker.txt ${output_dir}genotype_marker.txt

## Writing the Marker_pos.txt file, cosisting of the positions of all markers from the dataset 	
echo -e "Marker\tchr\tPP\tMean" > ${output_dir}Marker_pos.txt
Number_of_markers=$(head -1 ${all_genotype_Maker} |sed 's/\t/\n/g' |awk '(NR>1){print $0}'|wc -l) 
head -1 ${all_genotype_Maker}| awk '{sub(/\r$/,"")};1' |sed 's/\t/\n/g' |awk -F"_" '(NR>1) {print NR-1"\t"$1"\t"$2"\t"$2}' >> ${output_dir}Marker_pos.txt 	
Marker_pos_variable=${output_dir}Marker_pos.txt

## Preprocessing gene anotation file
## producing the gene annotation file	

echo -e "Gene annotation		... ready. Using genotype data file: \e[00;32m${all_genotype_Maker}\e[00m, \n		and the gene annotations: \e[00;32m${gene_annotations}\e[00m"
echo -e "Gene annotation		... ready. Using genotype data file:" ${all_genotype_Maker}", and the gene annotations: "${gene_annotations} >> ${output_dir}/${output_process_file}
# ----------------

######################################################################
# In case of using two libraries of miRNAs, following is the selection of the method for miRNA selection for the QTL mapping
if [ "$cross_data_object_ready" -eq 0 ];then
# in case the R Object was already generated
	if [ "$DUAL_BIB_MODE" -eq 1 ];then
		if [ "$EXPRESSION_SEL_DONE" -eq 0 ];then
		
			echo -e "		Using two gene libraries" 
			echo -e "		Using two gene libraries"  >> ${output_dir}/${output_process_file}

			case $expr_selection_method in 
			1*) 
				# In this method each gene is selected if it is present in the primary library, and otherwise the one in the secondary library is taken
				echo "		Using the simple (1) method for the selection of the gene expression for the eQTL" 
				echo "		Using the simple (1) method for the selection of the gene expression for the eQTL" >> ${output_dir}/${output_process_file}
				${scripts_dir}eQTl_mapping_2lib_expression_selection_method1.sh $miRNA_lib $miRNA_lib2 $normalized_coverage_file $tmp_folder_path $output_dir
				ms_method=1;;

			2*) 
				echo "		Using the the good (2) method of selection of miRNAs for eQTL" 
				ms_method=2;;
			
			3*) 
				echo "		Using the complete (3) method of selection of miRNAs for eQTL" 
				echo "          Using the complete (3) method for the selection of the gene expression for the eQTL" >> ${output_dir}/${output_process_file}
				${scripts_dir}eQTl_mapping_2lib_expression_selection_method3.sh $miRNA_lib $miRNA_lib2 $normalized_coverage_file $tmp_folder_path $output_dir $gene_annotations $scripts_dir $Marker_pos_variable
				ms_method=3;;

			*) 
				echo "		NOTE: no gene expression selection method has been specified. Default is 1: simple." 
				ms_method=1;;
			esac
			# This file is read into the bash environment of the parent script. (child scripts cannot change a global variable, so I need to impelemt this workaround)
			source ${tmp_folder_path}/normalized_coverage_file_for_eQTL_update.txt
			echo -e "		File with the normalization data: \e[00;32m${normalized_coverage_file_for_eQTL}\e[00m"
			echo -e "		File with the normalization data: ${normalized_coverage_file_for_eQTL}" >> ${output_dir}/${output_process_file}
		else
			# in case the file with the preselected values for all the genes is provided
			normalized_coverage_file_for_eQTL=$normalized_coverage_file_for_eQTL_prov
		fi;
		
	else
		# In case just one library is used the normalized read count file is used
		normalized_coverage_file_for_eQTL=$normalized_coverage_file
	fi;

else
	 normalized_coverage_file_for_eQTL=$normalized_coverage_file
fi;


########################################################################################
### Preparing the files for the eQTL mapping

## Generating the cross the header for the cross file, which is used by the eQTL R package. 
## For the details on the format please refer to the eQTL/R manual. 


if [ "$cross_data_object_ready" -eq 0 ];then
	echo -e "producing the main Expression data + Marker values file. (cross_file)\n	Using the following expression file to estimate gene names: \e[00;32m$normalized_coverage_file_for_eQTL\e[00m"            
	echo -e "producing the main Expression data + Marker values file. (cross_file)\n	Using the following expression file to estimate gene names: $normalized_coverage_file_for_eQTL" >> ${output_dir}/${output_process_file}
	awk '(NR > 1){printf "\t"$1}' ${output_dir}Marker_pos.txt | sed '$a\' > ${tmp_folder_path}/marker_turned
	awk '(NR > 1){print $1}' $normalized_coverage_file_for_eQTL |awk '{printf "\t"$1}'> ${tmp_folder_path}/gene_names_turned
	cat ${tmp_folder_path}/gene_names_turned ${tmp_folder_path}/marker_turned | awk '{print $0}' |sed 's/[-:+]/./g' > ${tmp_folder_path}/cross_file.txt_header        

	# inserting the necessary amount of empty lines and chr coordinates for Markers into the cross_file header.
	gene_nr=$(awk '(NR>1){print $1}' $normalized_coverage_file_for_eQTL |wc -l) 
	head -1 $all_genotype_Maker |sed 's/\t/\n\t/g' |awk -F"_" '(NR>1) {printf $1}' | awk '{sub(/^[ \t]+/, ""); printf $0}'  > ${tmp_folder_path}/marker_chr_tmp
	echo -en "\n" >> ${tmp_folder_path}/marker_chr_tmp
	echo -en "  \t" > ${tmp_folder_path}/extra_lines_tmp
	echo -ne $(seq -s ' -\t' $(($gene_nr+1)) | sed 's/[0-9]//g') | sed 's/-/ /g' | sed ':a;N;$!ba;s/\n//g' >> ${tmp_folder_path}/extra_lines_tmp
	cat ${tmp_folder_path}/extra_lines_tmp ${tmp_folder_path}/marker_chr_tmp > ${tmp_folder_path}/extra_line
	cat ${tmp_folder_path}/cross_file.txt_header ${tmp_folder_path}/extra_line > ${tmp_folder_path}/tmp_header
	mv ${tmp_folder_path}/tmp_header ${tmp_folder_path}/cross_file.txt_header

	# running a small python script, which parses the phenotype and genotype files and places them in the needed order into the final cross file, which then can be used by the eQTL/R package
	python ${scripts_dir}generate_file_for_etql.py ${tmp_folder_path}/cross_file.txt_header $normalized_coverage_file_for_eQTL ${output_dir}genotype_marker.txt
	# removing empty lines
	sed '/^ *$/d' ${tmp_folder_path}/cross_file.txt_header > ${output_dir}cross_file_ready.txt

	#####
	# declaring the cross_file variable and the marker positions
	Marker_pos_variable=${output_dir}Marker_pos.txt
	cross_file_variable="${output_dir}cross_file_ready.txt"
	echo -e "	The cross file is ready: \e[00;32m${cross_file_variable}\e[00m"
	echo -e "	The cross file is ready: ${cross_file_variable}" >> ${output_dir}/${output_process_file}
else
	
	Marker_pos_variable=${output_dir}Marker_pos.txt
	cross_file_variable="$cross_data_object"
	echo -e "	Using supplied cross_file R object: \e[00;32m${cross_data_object}\e[00m"
	echo -e "	Using supplied cross_file R object: ${cross_data_object}" >> ${output_dir}/${output_process_file}

fi;
##############################################################


if [ "$cross_data_object_ready" -eq 0 ];then
	## Converting the Markers to a numeric format
	first_pos_check=$(($gene_nr +2)) # +2 because the first phenotype in cross object is not gene and we need the first pos of genetype
	last_pos_check=$((${gene_nr}+${Number_of_markers}+1))
	echo -e $first_pos_check $gene_nr ${Number_of_markers} $last_pos_check
	# In case RILs are used, the conversion must be done slightly different
	if [ "$RIL_USED" -eq 1 ];then
		awk -F "\t" -v f=${first_pos_check} -v t=${last_pos_check} '(NR>2){ for (i=f; i<=t;i++) printf("%s%s", $i,(i==t) ? "\n" : OFS) }' ${cross_file_variable}| sed 's/A/0/g' |sed "s/H/1/g;s/B/1/g" | sed '/^ *$/d' > ${tmp_folder_path}/Markers_only.txt    
	else
		awk -F "\t" -v f=${first_pos_check} -v t=${last_pos_check} '(NR>2){ for (i=f; i<=t;i++) printf("%s%s", $i,(i==t) ? "\n" : OFS) }' ${cross_file_variable}| sed 's/A/0/g' |sed 's/H/1/g'| sed 's/B/2/g' | sed '/^ *$/d' > ${tmp_folder_path}/Markers_only.txt
	fi;
	# Defining the Markers_only variable
	Markers_only=${tmp_folder_path}/Markers_only.txt
	echo -e "		Genotypes represented with 1/0: \e[00;32m${Markers_only}\e[00m"
	echo -e "		Genotypes represented with 1/0: ${Markers_only}" >> ${output_dir}/${output_process_file}

	# getting the number of markers on each of the cromosomes
	markers_on_chromosomes=$(awk '(NR>1) {print $2}' $Marker_pos_variable | uniq -c | sed 's/ /\t/g' | awk '{print $1}' | sed ':a;N;$!ba;s/\n/ /g'|sed 's/ /_/g')
	#markers_on_chromosomes=$(awk '(NR>1) {print $2}' $Marker_pos_variable |sert -n | uniq -c | sed 's/ /\t/g' | awk '{print $1}' | sed ':a;N;$!ba;s/\n/ /g'|sed 's/ /_/g')

	#echo $(awk '(NR>1) {print $2}' $Marker_pos_variable | sort -n | uniq -c | sed 's/ /\t/g' | awk '{print $1}' | sed ':a;N;$!ba;s/\n/ /g'|sed 's/ /_/g') > ${tmp_folder_path}/markers_on_chromosomes.txt
	#markers_on_chromosomes=${tmp_folder_path}/markers_on_chromosomes.txt

	###Performing (if needed) the clustering of markers
	## 
	if [ "${CLUSTER_MARKERS}" -eq 1 ];then
		if [ "${CLUST_SPEC}" -eq 0 ];then
			echo -e "	Clustering the Markers depending on their Genotype across the Individuals"
			echo -e "	Clustering the Markers depending on their Genotype across the Individuals. \n (Memory requested: $Memory_Clustering, CPUs requested: $Number_of_threads_Clustering)" >> ${output_dir}/${output_process_file}
			echo ${Memory_Clustering}
			echo ${Number_of_threads_Clustering}
			qsub -l "h_vmem=${Memory_Clustering}" -pe smp ${Number_of_threads_Clustering} -wd "${tmp_folder_path}/" -N ${output_name_clustering} -v PATH=$PATH -o ${job_name}.${ojob_id} -b y "${scripts_dir}cluster_the_markers.sh $cross_file_variable $gene_nr $Number_of_markers $RIL_USED $output_dir $tmp_folder_path $scripts_dir $plot_dir $output_process_file $eQTL_method $ntree_v $Marker_pos_variable $Markers_only $Number_of_threads_Clustering $markers_on_chromosomes"
			for_greping=$(echo ${output_name_clustering} |sed 's/./&\n/10' |head -1)
			R_eQTL_job=$(qstat |grep $for_greping |awk '{print $1}')
			while [ "$R_eQTL_job" != "" ]
			do
				sleep 70
				R_eQTL_job=$(qstat |grep $for_greping |awk '{print $1}')
				echo -e "waiting for the QTL job:" $R_eQTL_job
			done

			# replacing the commas in the output file with tabs, as well as decoding the genotype
			cat ${output_dir}cross_file_clustered_markers.csv | sed 's/,/\t/g' | sed 's/AA/A/g' | sed 's/BB/B/g' > ${output_dir}cross_file_clustered_markers.csv_tmp
			mv ${output_dir}cross_file_clustered_markers.csv_tmp ${output_dir}cross_file_clustered_markers.csv
			cross_file_variable="${output_dir}cross_file_clustered_markers.csv"
			Marker_pos_variable=${output_dir}Markers_pos_clustered.txt
			Markers_only=${tmp_folder_path}/markers_only_clusterd.txt
			echo -e "			The cross file has been updated: \e[00;32m${cross_file_variable}\e[00m.	\n			The modified file with the Marker positions has been created: \e[00;32m$Marker_pos_variable\e[00m\n			The 1/0 represented Markers file has been updated to: \e[00;32m$Markers_only\e[00m"
			echo -e "	The cross file has been updated: ${cross_file_variable}.\n			The modified file with the Marker positions has been created: $Marker_pos_variable\n			The 1/0 represented Markers file has been updated to: $Markers_only" >> ${output_dir}/${output_process_file}

		else
			echo  $CLST_R_OBJ $RF_BIAS_OBJ
			cp $CLST_R_OBJ ${tmp_folder_path}/
			cp $RF_BIAS_OBJ ${tmp_folder_path}/
			cross_file_variable="${output_dir}cross_file_clustered_markers.csv"
			Marker_pos_variable=${output_dir}Markers_pos_clustered.txt
			Markers_only=${output_dir}markers_only_clusterd.txt
		fi;

	fi;
fi;

	### eQTL mapping
	## Defining what eQTL mapping algorithm(s) to us
	univar_run=0
	multivar_run=0
	case $eQTL_method in 
	0*) 
		# Using all algorithms. Including the once defined by the user  
		# Running all the predefined UNI-variate methods 
		univar_run=1	
		multivar_run=1
		use_variable="use_all_variable"
		for (( c=1; c<=$(cat ${dataset_dir}external_eqtl_methods.txt | wc -l); c++ ))
			do
				line=c
				external_script_file=$(awk -F "\t" -v aa=$line '(NR==line) {print $1}' ${dataset_dir}external_eqtl_methods.txt)
				external_script_name=$(awk -F "\t" -v aa=$line '(NR==line) {print $1}' ${dataset_dir}external_eqtl_methods.txt)
				external_script_uni_multivariate=$(awk -F "\t" -v aa=$line '(NR==line) {print $1}' ${dataset_dir}external_eqtl_methods.txt)
				echo -e "	Running externaly defined Algorithm: $external_script_name ($external_script_file)"
				$external_script_file
			done;;		
	1*) 
		# Running all the predefined methods by the script
		univar_run=1	
		multivar_run=1
		use_variable="use_all_variable";;
	2*) 
		# Running all the predefined UNI-variate methods 
		univar_run=1	
		use_variable="use_all_variable";;
	3*) 
		# Running all the predefined MULTI-variate methods by the script
		multivar_run=1
		use_variable="use_all_variable";;
	4*) 
		# Using the algorithm (s) provided by the user  
		echo "	bla	" 
		for (( c=1; c<=$(cat ${dataset_dir}external_eqtl_methods.txt | wc -l); c++ ))
			do
				line=c
				external_script_file=$(awk -F "\t" -v aa=$line '(NR==line) {print $1}' ${dataset_dir}external_eqtl_methods.txt)
				external_script_name=$(awk -F "\t" -v aa=$line '(NR==line) {print $1}' ${dataset_dir}external_eqtl_methods.txt)
				external_script_uni_multivariate=$(awk -F "\t" -v aa=$line '(NR==line) {print $1}' ${dataset_dir}external_eqtl_methods.txt)
				echo -e "	Running externaly defined Algorithm: $external_script_name ($external_script_file)"
				$external_script_file
			done		;;
	5*) 
		# Running only the standard interval mapping
		univar_run=1	
		use_variable="em";;
	6*) 
		# Running only the Extended Haley-Knott method
		univar_run=1    
		use_variable="ehk";;
	7*) 
		# Running only the multiple imputation method
		univar_run=1    
		use_variable="imp";;
	8*) 
		# Running only the Random Forest method
		multivar_run=1
		use_variable="RF_var";;
	9*) 
		# Running only the ANOVA method (which is basicaly a marker regression, an F-test on each Marker)
		univar_run=1
		use_variable="mr";;
	*) 
		# (default) Running all the predefined methods by the script
		univar_run=1
		multivar_run=1
		use_variable="use_all_variable";;
	esac

	echo $RIL_used
	# Staring the eQTL mapping runs
	if [ $univar_run -eq 1 ];then
		# Running the univariate methods 
		${scripts_dir}univariate_methods.sh $use_variable $RIL_USED $Memory_eQTL $Number_of_threads_eQTL $output_name_eQTL $scripts_dir $cross_file_variable $output_dir $plot_dir $step_v $error_prob_v $map_function_v $stepwidth_v $n_draws_v $n_perm_v $tmp_folder_path $sign_level $sign_level_FDR $output_process_file $use_FDR $CLUSTER_MARKERS $Marker_pos_variable $Memory_eQTL_FDR $Number_of_threads_eQTL_FDR $output_name_eQTL_FDR $Memory_effectplot $output_name_effectplots $drop $cross_data_object_ready $cross_data_object $Number_of_machienes_eQTL $Memory_eQTL_preload $Number_of_threads_eQTL_preload $output_name_clustering $LIST_of_gene_NAMES $gene_names_LIST_used
	fi;
	if [ $multivar_run -eq 1 ];then
		# running the multivariate methods
		echo -e "${scripts_dir}multivariate_methods.sh $use_variable $Markers_only $cross_file_variable $plot_dir $gene_annotations $Marker_pos_variable $nforest_v $tmp_folder_path $CLUSTER_MARKERS $first_pos_check $Memory_eQTL_RF $output_name_eQTL $scripts_dir $output_dir $output_process_file $gene_nr $Number_of_threads_eQTL_RF $RIL_USED $ntree_v $sign_level_FDR $markers_on_chromosomes $univar_run $Number_of_cores_eQTL_RF $cross_data_object_ready $cross_data_object"
		${scripts_dir}multivariate_methods.sh $use_variable $Markers_only $cross_file_variable $plot_dir $gene_annotations $Marker_pos_variable $nforest_v $tmp_folder_path $CLUSTER_MARKERS $first_pos_check $Memory_eQTL_RF $output_name_eQTL $scripts_dir $output_dir $output_process_file $gene_nr $Number_of_threads_eQTL_RF $RIL_USED $ntree_v $sign_level_FDR $markers_on_chromosomes $univar_run $Number_of_cores_eQTL_RF $cross_data_object_ready $cross_data_object
	fi;

	##


# QTL ANALYSIS READY. 
echo -e "\n\e[00;34mNOTE:\e[00m --------------------------------------------------------------"
echo -e "\n\e[00;34mNOTE:\e[00m QTL analysis is ready!"	
echo "NOTE: --------------------------------------------------------------"  >> ${output_dir}/${output_process_file}
echo "NOTE: QTL analysis is ready!"  >> ${output_dir}/${output_process_file}
if [ $univar_run -eq 1 ]; then
	echo -e "Univariate methods have been used for the eQTL mapping"
	echo -e "Univariate methods have been used for the eQTL mapping" >>  ${output_dir}/${output_process_file}

	if [ $use_FDR -eq 1 ]; then
		echo -e "The values have been corrected for multiple testing (FDR)"
		echo -e "The values have been corrected for multiple testing (FDR)" >> ${output_dir}/${output_process_file}
		QTL_file_variable=${output_dir}QTLs_identified_univariant_FDR.txt
		QTL_number_found=$(awk '(NR>1){print $0}' $QTL_file_variable | wc -l)
		if [ $QTL_number_found -eq 0 ];then
			QTL_number_found_norm=$(awk '(NR>1){print $0}' ${output_dir}QTLs_identified_univariant.txt | wc -l)
			echo -e "No significant QTL have been found. \nPlease consider the results without the FDR correction. (QTLs_identified_univariant.txt: \e[00;32m$QTL_number_found_norm\e[00m QTLs found.  \e[00;32m${output_dir}QTLs_identified_univariant.txt\e[00m)"
			echo -e "No significant QTL have been found. \nPlease consider the results without the FDR correction. (QTLs_identified_univariant.txt:$QTL_number_found_norm QTLs.  found ${output_dir}QTLs_identified_univariant.txt)" >> ${output_dir}/${output_process_file}
		else
			QTL_number_found=$(awk '(NR>1){print $0}' $QTL_file_variable | wc -l)
			echo -e "Number of significant QTLs found: $QTL_number_found" >> ${output_dir}/${output_process_file}
			echo -e "Number of significant QTLs found: \e[00;32m$QTL_number_found\e[00m"
		fi;
	else
		QTL_file_variable=${output_dir}QTLs_identified_univariant.txt
	fi;
	QTL_number_found_em=$(awk '(NR>1){print $0}' $QTL_file_variable | grep -c "em")
	QTL_number_found_ehk=$(awk '(NR>1){print $0}' $QTL_file_variable | grep -c "ehk")
	QTL_number_found_imp=$(awk '(NR>1){print $0}' $QTL_file_variable | grep -c "imp")
	gene_names_found=$(awk -F"\t" '(NR>1){print $1}' $QTL_file_variable | sort | uniq)
	echo -e "Number of QTL found (\e[00;32m$QTL_file_variable\e[00m): \n\tStandard interval mapping: \e[00;32m$QTL_number_found_em\e[00m, \n\tExtended Haley-Knott: \e[00;32m$QTL_number_found_ehk\e[00m,\n\tMultiple imputation: \e[00;32m$QTL_number_found_imp\e[00m. \n\tNames of the genes with signifiacnt QTL: \n\e[00;32m$gene_names_found\e[00m"  
	echo -e "Number of QTL found ($QTL_file_variable): \n\tStandard interval mapping: $QTL_number_found_em, \n\tExtended Haley-Knott: $QTL_number_found_ehk,\n\tMultiple imputation: $QTL_number_found_imp. \n\tNames of the genes with signifiacnt QTL: $gene_names_found" >> ${output_dir}/${output_process_file}
else
	echo -e "\n\e[00;35mNOTE:\e[00m The univariate Methods have not been used"
	echo -e "The univariate Methods have not been used" >> ${output_dir}/${output_process_file}
fi;
if [ $multivar_run -eq 1 ]; then
	echo -e "\n\e[00;35mNOTE:\e[00m Multivariate methods have been used for the eQTL mapping"
	echo -e "Miltivariate methods have been used for the eQTL mapping" >>  ${output_dir}/${output_process_file}

	if [ $use_FDR -eq 1 ]; then
		echo -e "The values have been corrected for multiple testing (FDR)"
		echo -e "The values have been corrected for multiple testing (FDR)" >> ${output_dir}/${output_process_file}
		QTL_file_variable_multi=${output_dir}QTLs_identified_multivariant_FDR.txt
		QTL_number_found=$(awk '(NR>1){print $0}' $QTL_file_variable_multi | wc -l)
		if [ $QTL_number_found -eq 0 ];then
			echo -e "No significant QTLs have been found"
		else
			QTL_number_found=$(awk '(NR>1){print $0}' $QTL_file_variable_multi | wc -l)
		fi;
	else
		QTL_file_variable_multi=${output_dir}QTLs_identified_multivariant.txt
		QTL_number_found=$(awk '(NR>1){print $0}' $QTL_file_variable_multi | wc -l)
	fi;
	gene_names_found=$(awk -F"\t" '(NR>1){print $1}' $QTL_file_variable_multi | sort | uniq)
	echo -e "Number of QTL found: \e[00;32m$QTL_number_found\e[00m. Names of the genes with significant QTLs: \n\e[00;32m'${gene_names_found}'\e[00m"  
	echo -e "Number of QTL found: \e[00;32m$QTL_number_found\e[00m. Names of the genes with significant QTLs: $gene_names_found"  >> ${output_dir}/${output_process_file}

else
	echo -e "The multivariate Methods have not been used"
	echo -e "The multivariate Methods have not been used" >> ${output_dir}/${output_process_file}
fi;

echo -e "The corresponding plots can be found in the folder: \e[00;32m${plot_dir}\e[00m"

# producing the file with all results in one file
cat $QTL_file_variable $QTL_file_variable_multi |  sed 's/"//g' | awk '!x[$0]++' | sed 's/^X//g'  > ${output_dir}QTLs_identified_ALL.txt

# overwriting the identified_QTLs_file variable
rm -f ${tmp_folder_path}/identified_QTLs_file_update.txt
echo "identified_QTLs_file=${output_dir}QTLs_identified_ALL.txt" >> ${tmp_folder_path}/identified_QTLs_file_update.txt


# defining the Markers_position variable
rm -f ${tmp_folder_path}/Marker_pos_variable_update.txt
echo "Marker_pos_variable=${Marker_pos_variable}" >> ${tmp_folder_path}/Marker_pos_variable_update.txt


