#%%%% HEAD %%%%#
#/-
#%% Effectplots of the results of univariate eQTL mapping 

# Opening liraries
suppressMessages(library(qtl))

# Reading arguments
args <- commandArgs(TRUE)
qtl_file_location <- args[1]
marker_pos_location <- args[2]
cross_file_R_object <- args[3]
plot_dir<- args[4]

# opening needed files
qtl_file <- read.table(qtl_file_location, header=TRUE, sep="\t")
marker_positions <- read.table(marker_pos_location ,header=TRUE,sep="\t");
load (cross_file_R_object)
#-\
print (dim(qtl_file))

# the loop over all qtls 
for(i in 1:length(rownames(qtl_file)))
{
        # defining all the variables
        pheno_name=toString(qtl_file$name[i])
        qtl_method=qtl_file$method[i]
        pvalue=qtl_file$pval[i]
        pos=qtl_file$pos[i]
        chr=qtl_file$chr[i]
        marker=find.marker(cross_data,chr=chr, pos=pos)
	print (marker)
        physic_pos=subset(marker_positions, Marker %in% marker)$Mean
	if (length(physic_pos)[1]==0)
	{
		physic_pos=subset (marker_positions, PP %in% marker)
		marker_name=subset (marker_positions, PP %in% marker)$Marker
	}else{
		marker_name=marker
	}
	lod=qtl_file$score[i]	
	qval=qtl_file$qvals[i]	
	print (physic_pos)
	
	# Defining the y measurs for the y
	print (pheno_name)
        min_y=min(cross_data$pheno[pheno_name], na.rm=T)
        max_y=max(cross_data$pheno[pheno_name], na.rm=T)
	print (c(min_y,max_y))

        # producing the plots
        pdf(paste(paste(plot_dir,"effectplot_QTL_",sep=""),"genename-",pheno_name,"_marker-", marker_name,"_method-", qtl_method, ".pdf", sep=""), height=10, width=17)
        par(mfrow=c(1,2),  oma = c( 0, 0, 2, 0 ) )
        #mtext( paste("Effect plot on Marker ",marker, "(chr:" ,chr ,"position: ",physic_pos,") of miRNA ",pheno_name), outer = TRUE )
        effectplot(cross_data, mname1=marker, pheno.col=pheno_name, ylim=c(min_y,max_y), xlab="Genotype", ylab="Expression", main="")
        plot.pxg(cross_data, pheno.col=pheno_name, marker=marker, ylim=c(min_y,max_y),  xlab="Genotype", ylab="Expression", main="")

        par(mfrow=c(1,1))
        title(paste("Effect plot on Marker ",marker_name, "(chr:" ,chr ,"position: ",physic_pos,") of the gene ",pheno_name, "with a p-value of ", pvalue,"and a q_values (FDR) of:",qval, "(Method used: )",qtl_method), outer=TRUE)
        dev.off()

        ## producing a effektplot ovew the whole genome for a miRNA
        #pdf(paste(paste(plot_dir,"effectscan",sep=""),"_",pheno_name,"_", qtl_method, ".pdf", sep=""), height=10, width=17)
        #effectscan(cross_data, pheno.col=pheno_name, get.se=TRUE, ylab="Additive effect", add.legend=FALSE)
        # additive effect:  additive effect is estimated as half the difference between the phenotypic averages for the two homozygotes
        #title(paste("Additive effect of the QTL of the miRNA ",pheno_name, "across the whole genome"))
        #dev.off()

}

                

