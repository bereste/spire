#%%%% HEAD %%%%#
#/-
#%% This script gets produced QTL results and produces a cis/trans plot together with a statistical analysis.
# QTL_name	miRNA_name	QTL_pos_in_cM	chromosome	method
# QTL_name format:e.g "cel.mir.1"

############
# loading the libraries
suppressMessages(library(eqtl))
suppressMessages(library(car))
suppressMessages(library(maptools)) #pointLabel, same as text(), but searches for best place to put a text into the plot with no overlap to other stuff
suppressMessages(library(RColorBrewer)) # for brewer.pal()

# loadring the parameters
args <- commandArgs(TRUE)
QTL_file <- args[1] 		
marker_positions_file <- args[2]   
gene_positions_file <- args[3]  
results_folder <- args[4]
plot_dir<-args[5]
#############

# reading variables
#load(cross_file);
print (gene_positions_file)
QTL_all <- read.table(QTL_file ,header=TRUE,sep="\t");
gene_positions <- read.table(gene_positions_file ,header= TRUE, row.names=1,  as.is=T);
marker_positions <- read.table(marker_positions_file ,header=TRUE,sep="\t");
#############
#-/
#%%%% BODY %%%%#
#/-

# Creating color schema
if (length(levels(QTL_all$method))==1)
{
	myColors<-"red"
}else if (length(levels(QTL_all$method))==2){
	myColors<-c("red", "blue")
}else{
	myColors <- brewer.pal(length(levels(QTL_all$method)),"Set1")
}
print (myColors)
names(myColors) <- levels(QTL_all$method)
print (myColors)

# processing each of the methods in the output file separetly and producing a plot
for (meth in levels(QTL_all$method))
{
	print (meth)
	print (levels(QTL_all$method))
	first_run=1
	positions_holder=matrix(ncol=dim(QTL_all)[1], nrow=2)
	QTL<-subset(QTL_all, method==meth)
	for (i in 1:length(QTL$name))
	{ 
		# setting variables for the first initialisation
		# naming variables
		gene_name=toString(QTL[i,]$name)
		if (QTL[i,]$confInt!="-"){
			QTL_marker=as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_"))[2])
		}else{
			print ("!eQTL mapping: the following QTL has no support interval. Usually it means, that you've selected a higher FDR cutoff threshold, than for the significance test prior to the multiple testing correction")
			print (QTL[i,])
			levels(QTL$confInt)<-c(levels(QTL$confInt),"0_0_0")
			QTL[i,]$confInt<-"0_0_0"
			QTL_marker=as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_"))[2])
		}
		QTL_pos=QTL[i,]$pos
		QTL_chr=toString(QTL[i,]$chr)
		color_to_use=myColors[meth]
		print (myColors[meth])
		# nearest marker to the gene
		gene_pos_uno=gene_positions[grep(paste(gene_name," *$",sep=""), rownames(gene_positions)),][1,]
		pos_to_check=((as.integer(gene_pos_uno$stop)-as.integer(gene_pos_uno$start))/2)+as.integer(gene_pos_uno$start)
		print ("position uno")
		print (gene_pos_uno)
		chr_of_gene=(gene_pos_uno$chr)
		chr_pos=subset(marker_positions,marker_positions$chr %in% chr_of_gene)     
		#print (chr_pos)
		line=which(abs(chr_pos$Mean-pos_to_check)==min(abs(chr_pos$Mean-pos_to_check)))
		print (pos_to_check)
		print ("----")
		print (line)
		gene_Marker=chr_pos[line,]$Marker
		# getting the confidence interval for the given QTL
		print (length(as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_")))))
		print (QTL[i,]$confInt)
		if (length(as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_"))))!=3)
		{
			Conf_int=c(as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_"))[1]), as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_"))[2]))
			print ("1")
		}else{
			Conf_int=c(as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_"))[1]), as.integer(unlist(strsplit(toString(QTL[i,]$confInt),"_"))[3]))
			print ("2")
		}
		print (Conf_int)
		if (TRUE %in% (is.na(Conf_int))) 
		{
			print (Conf_int)
			if (sum(is.na(Conf_int))==2)
			{
				Conf_int=c(QTL_marker,QTL_marker)
			}else{
				print (is.na(Conf_int))
				real_val=Conf_int[which (!is.na(Conf_int))]
				print (real_val)
				Conf_int[which (is.na(Conf_int))]=real_val
			}
			print (Conf_int)
			
		}
		print (Conf_int)
		print ("!!!!!!!!!!!!!!!!!!!!!!!!!!!!1")
		# in casewe have some NA values (which basicaly means that there has been some problems), they will be ignored
		
		

		print (QTL$name[i])
		print (dim(QTL))
		if (first_run==1)
		{
			#preparing the cooredinate system for plotting: plot of the first QTL
			pdf(paste(paste(plot_dir, "cis_trans_", sep=""),meth,".pdf", sep=""), height=12, width=17)
			par(xaxs="i", yaxs="i")
			print (QTL_marker)
			print ("gene_Marker:")
			print (gene_Marker)
			print (color_to_use)
			
			plot(QTL_marker, gene_Marker, xlim=c(0, (tail(marker_positions,1)$Marker)), ylim=c(0, tail(marker_positions,1)$Marker),cex=1, col=color_to_use, xaxt='n', yaxt='n', xlab="QTL Position", ylab="Transcript Position", main="",cex.lab=2.0, pch="|", lwd=2)
			positions_holder[,i]=c(QTL_marker, gene_Marker)
			text(QTL_marker, gene_Marker, labels=gene_name, pos=3, cex=1.6)
			abline(0,1)
			segments(y0=gene_Marker, x0=Conf_int[1], y1=gene_Marker, x1=Conf_int[2], lwd=2, col="red")
			#lines of chromosome separation and labeling the X-axis
			for (ii in 1:length(unique(marker_positions$chr)))
			{
				# the separation is needed in case the markers have been clustred. For that case the ablines between the chromosomes are drawn between the first marker of the chr and the (first marker)-1 of the next chr. 
				if (ii==length(unique(marker_positions$chr)))
				{
					chr_now=unique(marker_positions$chr)[ii]
					chr_pos=subset(marker_positions,marker_positions$chr %in% chr_now)
					abline(v=tail(chr_pos,1)$Marker, lwd=0.5, col="grey")
                                        abline(h=tail(chr_pos,1)$Marker, lwd=0.5, col="grey")
					m=mean(c(tail(chr_pos,1)$Marker,head(chr_pos,1)$Marker))


				}else{
					# in case the markers are clustered, the abline should be produced not on the last marker of the given Chr, but on the (first Marker)-1 of the next Chr.
					chr_now=unique(marker_positions$chr)[ii]
					chr_next=unique(marker_positions$chr)[ii+1]
					chr_pos=subset(marker_positions,marker_positions$chr %in% chr_now)
					chr_pos_next=subset(marker_positions,marker_positions$chr %in% chr_next)
					abline(v=head(chr_pos_next,1)$Marker-1, lwd=0.5, col="grey")
					abline(h=head(chr_pos_next,1)$Marker-1, lwd=0.5, col="grey")
					m=mean(c(head(chr_pos_next,1)$Marker-1,head(chr_pos,1)$Marker))
				}
				# aqdding the names of the chromosomes
				axis(side=1,tick=F,labels=chr_now, at=m, cex.axis=1.4)
				axis(side=2,tick=F,labels=chr_now, at=m, cex.axis=1.4)
			}

		}else{
			
			# adding values to the plot
			print (QTL_marker)
			print (gene_Marker)
			points(QTL_marker, gene_Marker, cex=1, col=color_to_use, pch="|", lwd=2)
			positions_holder[,i]=c(QTL_marker, gene_Marker)
			text(QTL_marker, gene_Marker, labels=gene_name, pos=3, cex=1.6)
			segments(y0=gene_Marker, x0=Conf_int[1], y1=gene_Marker, x1=Conf_int[2], lwd=2, col="red")

		}

		# checking for the classification as cis or trans
		cis_check="cis"
		gene_extracted=gene_positions[grep(paste(gene_name," *$",sep=""), rownames(gene_positions)),]
		#subset(phenames(cross_data),(phenames(cross_data) %in%  phenames(cross_data)[grep(paste("*", ,sep=""), phenames(cross_data))]))
		gene_chr=gene_extracted$chr
		pos_to_check=((as.integer(gene_extracted$stop)-as.integer(gene_extracted$start))/2)+as.integer(gene_extracted$start)
		# creating the 
		# In case the markers have not been clustered, the PP parameter in the Markers_pos.txt file has no "-" simbol. Thus it needs special handling
		QTL_phy_pos=c(as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[1])$PP),"-")))[1],as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[2])$PP),"-")))[2])
		# in case we have non clustered markers the we have a slightly different value
		if (is.na(as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[2])$PP),"-")))[2]))
		{
			QTL_phy_pos=c(as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[1])$PP),"-")))[1],as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[2])$PP),"-")))[1])	
		}
		
		# In case there were some problems finding the right Marker 
		# (Probably during the RF method, as it produces discrete peaks instead of a curve, so that one QTL has a confidence interval of one Marker. In case of clustered Markers, the interval is from the beginning of the cluster to its end)
		if (is.na(as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[2])$PP),"-")))[2])) 
		{
			QTL_phy_pos=c(as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[1])$PP),"-")))[1],as.integer(unlist(strsplit(toString(subset(marker_positions, Marker %in% Conf_int[1])$PP),"-")))[2])
		}
		
		
		print ("/////////////")
		print (Conf_int)
		print (QTL_chr)
		print (gene_chr)
		print (pos_to_check)
		print (QTL_phy_pos)
		print (gene_chr)
		print (QTL_chr)
		print ("/////////////")
		
		if (sum(is.na(QTL_phy_pos))==2){QTL_phy_pos=c(0,0)}
		print (i)
		if ((gene_chr!=QTL_chr)||(pos_to_check > as.integer(QTL_phy_pos[2]) ||pos_to_check <  as.integer(QTL_phy_pos[1])))
		{
			cis_check="trans"
		}
		# caching the results
		if (first_run==1)
		{
			cach_names= c("gene_position", "gene_chr","gene_Marker", "cis_trans")
			cach = matrix(c(pos_to_check,as.character(gene_chr),as.integer(gene_Marker), cis_check),ncol=4)
			colnames(cach)=cach_names
			first_run=0
		}else{
			#cach2 = matrix(c(pos_to_check,miRNA_Marker$chr, QTL_Markers$Mean[1], tail(QTL_Markers$Mean,1),cis_check),ncol=5)
			cach2 = matrix(c(pos_to_check,as.character(gene_chr),as.integer(gene_Marker), cis_check),ncol=4)
			cach = rbind(cach,cach2)
		}

		print (cis_check)
		print ("------------")

	}
	#print (positions_holder)
	#pointLabel(positions_holder[1,], positions_holder[2,], labels=QTL$miRNA_name, cex=1.3)
	#QTL$miRNA_name
	dev.off()

	# saving the output data
	print (QTL)
	print (cach)
	QTL_new=cbind(QTL, cach)
	write.table(QTL_new, file = paste(results_folder,"QTLs_identified_",meth,"_final.txt",sep=""), row.names=FALSE)
}






### Following is for multiple QTL Model
### Generating a qtl-object. The function refineqtl is needed for the further procedure 
#QTL_list_raw <- makeqtl(cross_data, QTL_chr, QTL_pos, miRNA_name, what="draws")
#QTL_list <- refineqtl(cross_data, qtl=QTL_list_raw, method="imp")





