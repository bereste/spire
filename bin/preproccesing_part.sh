#!/bin/bash

# This part coordinates the preprocessing 


Number_of_threads_barcode=$1
Memory_barcode=$2 
tmp_folder_path=$3 
read_file=$4 
barcodes_for_read=$5 
output_name=$6  
barcode_seq=$7 
dataset_dir=$8 
q_par=$9 
p_par=${10} 
Memory_fastx=${11}
Min_overlap_far2_1=${12}
End_far2_1=${13}
Number_of_threads_far2=${14}
Memory_far2=${15}
adapters=${16}
Min_overlap_far2_2=${17}
End_far2_2=${18}
output_dir=${19}
output_process_file=${20}
Barcode_split=${21}
all_files_ready=("${@:22}")

# ------------------------ Splitting reads by their barcodes if needed ----------------------
if [ "$Barcode_split" -eq 1 ];then
	unset all_files
	echo -e "\nSplitting the reads according to the barcode (Using" ${Number_of_threads_barcode} "threads and" ${Memory_barcode} "RAM)" >> ${output_dir}/${output_process_file}
	echo "Spliting the reads according to the barcode... "
	echo "    ... submitting jobs (Using" ${Number_of_threads_barcode} "threads and" ${Memory_barcode} "RAM)" 
	qsub -pe smp ${Number_of_threads_barcode} -l "h_vmem=${Memory_barcode}" -wd ${tmp_folder_path} -N "far2" -v PATH=$PATH -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -o ${job_name}.${ojob_id} -b y "far2 --source $read_file --barcode-reads $barcodes_for_read --target ${output_name} -f fastq --barcodes $barcode_seq --demultiplex-only yes --nr-threads ${Number_of_threads_barcode}"

	let count=0
	for i in $(cat ${barcode_seq}|grep ">" |sed 's/>//g' )  
	do   
		i_for_insert=$(echo -e "QX_barcode_${i}.fastq") 
		all_files[$count]=${i_for_insert}      
		((count++))
	done 


else 
	# fastx cant handle .'s in the files, so we have to rename them to N's
	echo "replacing .'s in the read files with N's"
	echo -e "\nNot demultiplexing. Therefore replacing .'s in the read files with N's for the following read filtering" >> ${output_dir}/${output_process_file}
	echo "    ... submitting jobs"
	for file in "${all_files[@]}"
	do   
		qsub -l "h_vmem=5G" -cwd -N "noDots" -wd ${tmp_folder_path} -v PATH=$PATH -b y "sed '2~4s/\./N/g' ${dataset_dir}$file > $file.noDots"
	done 
fi;  

# check whether the jobs are done
jobs_still_waiting=$(qstat |grep "noDots\|far2" |awk '{print $1}')
while [ "$jobs_still_waiting" != "" ] 
	do
		sleep 10
		jobs_still_waiting=$(qstat |grep "noDots\|far2" |awk '{print $1}')      
		echo -e "waiting for jobs:" $jobs_still_waiting
	done 
###################### 


# ----------------------- fastx filtering of the reads ---------------------------------
echo "Starting fastx_filtering with parameters: -q:${q_par} -p:${p_par}(for description see options file code)" >> ${output_dir}/${output_process_file}
echo "Starting fastx_filtering with parameters: -q:${q_par} -p:${p_par}(for description see options file code)"
echo "    ... submitting jobs"
for file in "${all_files[@]}"
do   
	if [ "$Barcode_split" -eq 1 ];then
		qsub -l "h_vmem=${Memory_fastx}"  -wd ${tmp_folder_path} -N "fastx_all" -v PATH=$PATH -b y "fastq_quality_filter -q ${q_par} -p ${p_par} -i ${file} -o  ${file}_filtered -v"
	else
		qsub -l "h_vmem=${Memory_fastx}"  -wd ${tmp_folder_path} -N "fastx_all" -v PATH=$PATH -b y "fastq_quality_filter -q ${q_par} -p ${p_par} -i ${file}.noDots -o ${file}_filtered -v"
	fi;
done

# check whether the jobs are done
jobs_still_waiting=$(qstat |grep "fastx" |awk '{print $1}')
while [ "$jobs_still_waiting" != "" ]
	do
		sleep 30
		jobs_still_waiting=$(qstat |grep "fastx" |awk '{print $1}')
		echo -e "Jobs still waiting:" $jobs_still_waiting
	done

# ----------------------- first run of adapter cliping with far 2 --------------------------------------
echo -e "\e[00;35mNOTE:\e[00m Two runs of far2 with different parameteres are necessary."
echo -e "       (NOTE: Two runs of far2 with different parameteres are necessary.) \n   Starting the first run of far2 adapter cliping (using: -min_overlap ${Min_overlap_far2_1} -end ${End_far2_1})" >> ${output_dir}/${output_process_file}
echo "Starting the first run of far2 adapter cliping (using: -min_overlap ${Min_overlap_far2_1} -end ${End_far2_1})"
echo "    ... submitting jobs"
for file in "${all_files[@]}"

	do
		qsub -pe smp ${Number_of_threads_far2} -l "h_vmem=${Memory_far2}" -wd ${tmp_folder_path} -N "far2" -v PATH=$PATH -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -b y "far2 --source ${file}_filtered --target ${file}_first --adapters $adapters --format fastq --cut-off 3 --nr-threads ${Number_of_threads_far2} --min-readlength 18 --min-overlap ${Min_overlap_far2_1} -end ${End_far2_1}"
	done

# check whether the jobs are done
jobs_still_waiting=$(qstat |grep "far2" |awk '{print $1}')
while [ "$jobs_still_waiting" != "" ]
	do
		sleep 15
		jobs_still_waiting=$(qstat |grep "far2" |awk '{print $1}')
		echo -e "Jobs still waiting:" $jobs_still_waiting
	done


# ---------------------- second run of adapter cliping with far2 ---------------------------------------
echo "starting the second run of far2 adapter cliping (using: -min_overlap ${Min_overlap_far2_2} -end ${End_far2_2})"
echo "  Starting the second run of far2 adapter cliping (using: -min_overlap ${Min_overlap_far2_2} -end ${End_far2_2})" >> ${output_dir}/${output_process_file}
echo -e " " > ${tmp_folder_path}/used_files.tmp.txt_tmp
echo "    ... submitting jobs"
for file in "${all_files[@]}"
	do
		qsub -pe smp ${Number_of_threads_far2} -l "h_vmem=${Memory_far2}" -N "far2" -wd ${tmp_folder_path} -v PATH=$PATH -v LD_LIBRARY_PATH=$LD_LIBRARY_PATH -b y "far2 --source ${file}_first.fastq --target ${file}_second --adapters $adapters --format fastq --cut-off 3 --nr-threads ${Number_of_threads_far2} --min-readlength 18 --min-overlap ${Min_overlap_far2_2} -end ${End_far2_2}"
		echo -e "${file}_second.fastq" >> ${tmp_folder_path}/used_files.tmp.txt_tmp
	done

# check whether the jobs are done
jobs_still_waiting=$(qstat |grep "far2" |awk '{print $1}')
while [ "$jobs_still_waiting" != "" ]
	do
		sleep 15
		jobs_still_waiting=$(qstat |grep "far2" |awk '{print $1}')
		echo -e "Jobs still waiting:" $jobs_still_waiting
	done

awk '{if (NR!=1) {print}}' ${tmp_folder_path}/used_files.tmp.txt_tmp > ${tmp_folder_path}/used_files_far2.tmp.txt
rm ${tmp_folder_path}/used_files.tmp.txt_tmp


# --------------------- renaming the output files and placing them into the $input_dir


unset all_files_ready
let count2=0
for i in $(cat ${tmp_folder_path}/used_files_far2.tmp.txt)
do
	i_changed=$(echo $i |awk '{sub(/_barcode_QX/,"")}; 1' |awk '{sub(/_second.fastq/,"")}; 1')
	cp ${tmp_folder_path}/$i ${output_dir}/${i_changed}
	all_files_ready[$count2]=${i_changed}
	((count2++))
done
rm ${tmp_folder_path}/used_files_far2.tmp.txt

#### Exporting the files of files as a global variable
## The problem faced here is following: the parent script ./procedure.sh get the value of all_files_ready[@] from the config file (In case there is no preprocessing needed and THIS script does not run). If the child script ./preprocessing.sh runs it needs to update the variable all_files_ready[@]. Unfortunately the child bash script cannot update a global variable (even using export function). Therefore ./preprocessing.sh creates a small file, which holds the new value of the variable. This file is than read into the bash environment of the parent script. 
rm -f ${tmp_folder_path}/all_files_ready_update.txt
echo "all_files_ready=${all_files_ready[@]}" | sed 's/ /" "/g' | sed 's/=/=("/g' | sed 's/$/")/g' >> ${tmp_folder_path}/all_files_ready_update.txt


echo -e "\n\e[00;34mNOTE:\e[00m -------------- Preprocessing done. The following files can be found in " ${output_dir}
echo -e "\nPreprocessing done. The following files can be found in " ${output_dir} >> ${output_dir}/${output_process_file}
echo -e ${all_files_ready[@]} >> ${output_dir}/${output_process_file}
echo -e ${all_files_ready[@]}
echo -e "------------------------------------------------------------------------------------------------ " >> ${output_dir}/${output_process_file}
echo -e "------------------------------------------------------------------------------------------------ "
