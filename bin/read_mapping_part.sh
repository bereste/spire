#!/bin/bash

# The read mapping part of the main script (procedure.sh) (option: -d)

miRNA_lib=$1
Memory_read_mapping=$2
output_dir=$3
output_process_file=$4
Preproc=$5
PATH=$6
tmp_folder_path=$7
scripts_dir=$8
dataset_dir=${9}
DUAL_BIB_MODE=${10}
miRNA_lib2=${11}
mapping_software=${12}
Number_of_threads_read_mapping=${13}
all_files_ready=("${@:14}")


echo  "${all_files_ready[@]}"

case $mapping_software in
1*)
	mp="RazorS";;
2*) 
	mp="MicroRazorS";;
*)  
	mp="!NOTE: no mapping algorithm selected!"
esac


echo -e "    Using $mp for read mapping"
echo "    ... submitting jobs"

for read_file in "${all_files_ready[@]}"
do
	if [ "$Preproc" -eq 1 ];then
		reads_files=${output_dir}/${read_file}
	else
		reads_files=${dataset_dir}${read_file}
	fi;

	qsub -v PATH=$PATH -l "h_vmem=${Memory_read_mapping}"  -pe smp ${Number_of_threads_read_mapping} -wd ${tmp_folder_path} -N "RS_${read_file}" -b y "${scripts_dir}convert_n_map.sh $reads_files $miRNA_lib $Number_of_threads_read_mapping $mapping_software $mp" 

done

if [ "$DUAL_BIB_MODE" -eq 1 ];then
	echo "    ... submitting jobs with the second gene library"
	echo "	You use a second gene library and the jobs for the mapping were submitted (${miRNA_lib2})"  >> ${output_dir}/${output_process_file}


	for read_file in "${all_files_ready[@]}"
	do
		if [ "$Preproc" -eq 1 ];then
			reads_files=${output_dir}${read_file}
		else
			reads_files=${dataset_dir}${read_file}
		fi;

		qsub -v PATH=$PATH -l "h_vmem=${Memory_read_mapping}" -pe smp ${Number_of_threads_read_mapping} -wd ${tmp_folder_path} -N "RS2_${read_file}" -b y "${scripts_dir}convert_n_map.sh $reads_files ${miRNA_lib2} $Number_of_threads_read_mapping $mapping_software $mp"
	done
fi;

# check whether the jobs are done
jobs_still_waiting=$(qstat |grep "RS\|RS2" |awk '{print $1}')
while [ "$jobs_still_waiting" != "" ]
	do
		sleep 30
		jobs_still_waiting=$(qstat |grep "RS" |awk '{print $1}')
		echo -e "Jobs still waiting:" $jobs_still_waiting
	done

## ---------- Generating out of all datasets a file with all information in one coverage file

for x in `echo ${miRNA_lib} | tr "/" " "`; do miRNA_lib_nopath=$x; done
echo "A file is generated with the coverage for all genes from all samples"
echo "	A file with the mapping coverage (not normalized) is generated for all genes on all used gene libraries" >> ${output_dir}/${output_process_file}
# NOTE: please be careful to not delete an extra space between "pr -t -m -s\" and ${tmp...}. 
pr -t -m -s\  ${tmp_folder_path}/*${miRNA_lib_nopath}_tmp/*.miRNA_cov | awk -v used_sets=${#all_files_ready[@]} '(NR == 1) {for(i=1;i<=used_sets;i++)printf$i "\t";print""} (NR>1){for(i=1;i<=used_sets*2;i++) if(i%2==0) printf""$i "\t";print""}' > ${tmp_folder_path}/all_coverage_part1.txt
pr -t -m -s\  ${tmp_folder_path}/*${miRNA_lib_nopath}_tmp/*.miRNA_cov | awk '(NR==1){print " "} (NR>1){print $1}'  > ${tmp_folder_path}/all_coverage_part2.txt
paste ${tmp_folder_path}/all_coverage_part2.txt ${tmp_folder_path}/all_coverage_part1.txt > ${tmp_folder_path}/allQX_miRNA_coverage.txt_tmp


#pr -t -m -s\  QX*.fastq_tmp/QX*.fastq.miRNA_cov | awk '(NR == 1) {print "\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"} (NR > 1) {print $1"\t"$2"\t"$4"\t"$6"\t"$8"\t"$10"\t"$12"\t"$14"\t"$16"\t"$18"\t"$20"\t"}' > allQX_miRNA_coverage.txt_tmp

sed 's/.fastq//g' ${tmp_folder_path}/allQX_miRNA_coverage.txt_tmp | sed 's/_reproc//g' > ${output_dir}/allQX_miRNA_coverage.txt

if [ "$DUAL_BIB_MODE" -eq 1 ];then
	# preparing the second library
	for x in `echo ${miRNA_lib2} | tr "/" " "`; do miRNA_lib2_nopath=$x; done
	pr -t -m -s\  ${tmp_folder_path}/*${miRNA_lib2_nopath}_tmp/*.miRNA_cov | awk -v used_sets=${#all_files_ready[@]} '(NR == 1) {for(i=1;i<=used_sets;i++)printf$i "\t";print""} (NR>1){for(i=1;i<=used_sets*2;i++) if(i%2==0) printf""$i "\t";print""}' | sed 1d > ${tmp_folder_path}/all_coverage_part1_lib2.txt
	pr -t -m -s\  ${tmp_folder_path}/*${miRNA_lib2_nopath}_tmp/*.miRNA_cov | awk '(NR>1){print $1}' > ${tmp_folder_path}/all_coverage_part2_lib2.txt
	paste ${tmp_folder_path}/all_coverage_part2_lib2.txt ${tmp_folder_path}/all_coverage_part1_lib2.txt > ${tmp_folder_path}/allQX_miRNA_coverage.txt_tmp_lib2

	# converging the two files together
	sed 's/.fastq//g' ${tmp_folder_path}/allQX_miRNA_coverage.txt_tmp_lib2 | sed 's/_reproc//g' > ${output_dir}allQX_miRNA_coverage.txt_lib2
	mv ${output_dir}allQX_miRNA_coverage.txt ${output_dir}allQX_miRNA_coverage.txt_lib1
	cat ${output_dir}allQX_miRNA_coverage.txt_lib1 ${output_dir}allQX_miRNA_coverage.txt_lib2 > ${output_dir}allQX_miRNA_coverage.txt
fi;

##################
# Cleaning up
rm -I ${tmp_folder_path}/all_coverage_part1.txt ${tmp_folder_path}/all_coverage_part2.txt ${tmp_folder_path}/allQX_miRNA_coverage.txt_tmp
if [ "$DUAL_BIB_MODE" -eq 1 ];then
	rm -I ${tmp_folder_path}/all_coverage_part1_lib2.txt ${tmp_folder_path}/all_coverage_part2_lib2.txt ${tmp_folder_path}/allQX_miRNA_coverage.txt_tmp_lib2
fi;
#################

