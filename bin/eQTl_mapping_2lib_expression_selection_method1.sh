#!/bin/bash

#%%%% HEAD %%%%#
#/-
#%% In case two gene libraries are used for the eQTL - mapping part, the expression for each of the genes is selected using this script (method1)
#%%  Method 1: The expression values for each gene are taken for one of the libraries (user specified). Only the value of unique genes in taken from the other library

# reading the variables
gene_lib=$1 
gene_lib2=$2  
normalized_coverage_file=$3
tmp_folder_path=$4
output_dir=$5
#-/

#%%%% BODY %%%%#
#/-
# defining the names of the libraries
first_lib_grep=$(head -1 ${gene_lib} | sed 's/>//g' | awk -F "[-_]" '{print $1}')
second_lib_grep=$(head -1 ${gene_lib2} | sed 's/>//g' | awk -F "[-_]" '{print $1}')

# selecting the gene names for the following selection of the expression 
awk '(NR > 1){print $1}' $normalized_coverage_file | grep $first_lib_grep | sed -r "s/$first_lib_grep[_-]//g" >  ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt
awk '(NR > 1){print $1}' $normalized_coverage_file | grep $first_lib_grep >  ${tmp_folder_path}/long_gene_names_$first_lib_grep.txt
awk '(NR > 1){print $1}' $normalized_coverage_file | grep -v -i -f ${tmp_folder_path}/short_gene_names_$first_lib_grep.txt > ${tmp_folder_path}/unique_gene_names_$second_lib_grep.txt
cat ${tmp_folder_path}/long_gene_names_$first_lib_grep.txt ${tmp_folder_path}/unique_gene_names_$second_lib_grep.txt > ${tmp_folder_path}/needed_gene_names_for_method1.txt

# selecting the names of the lines
awk '(NR==1){print $0}' $normalized_coverage_file >  ${tmp_folder_path}/line_names.txt

# picking the expression of the appropriate genes
grep -i -f ${tmp_folder_path}/needed_gene_names_for_method1.txt $normalized_coverage_file > ${tmp_folder_path}/method1_expression_selected_genes_tmp.txt
cat ${tmp_folder_path}/line_names.txt ${tmp_folder_path}/method1_expression_selected_genes_tmp.txt > ${tmp_folder_path}/method1_expression_selected_genes_tmp.mvtmp
mv  ${tmp_folder_path}/method1_expression_selected_genes_tmp.mvtmp ${tmp_folder_path}/method1_expression_selected_genes.txt

# removing empty lines just in case
sed '/^ *$/d' ${tmp_folder_path}/method1_expression_selected_genes.txt > ${tmp_folder_path}/method1_expression_selected_genes.txt_Noemptylines
mv ${tmp_folder_path}/method1_expression_selected_genes.txt_Noemptylines ${tmp_folder_path}/method1_expression_selected_genes.txt

# moving the appropriate file to the output folder
bn=$(basename ${normalized_coverage_file} .txt)
mv ${tmp_folder_path}/method1_expression_selected_genes.txt ${output_dir}${bn}_expr_mth1.txt

#### Exporting the a global variable from a child script 
rm -f ${tmp_folder_path}/normalized_coverage_file_for_eQTL_update.txt
echo "normalized_coverage_file_for_eQTL=${output_dir}${bn}_expr_mth1.txt" >> ${tmp_folder_path}/normalized_coverage_file_for_eQTL_update.txt



