# statistical analysis of the gene counts.

library(methods)
library(MASS)
library(nnet)

library(limma) # normalizeBetweenArrays (quantile normalization)
library(car) # scatterplot.matrix
library(edgeR) # TMM normalization

args <- commandArgs(TRUE)
data_location<- args[1]
code_location<- args[2]
lib_name<- args[3]
norm_meth<- args[4]
lib_full_name<- args[5]
output_dir<- args[6]
plot_dir<-args[7]

data_raw <- read.table(data_location, header=T)
data <- log(data_raw)


# ------------------ pre-normalization part ----------------------

cat (paste("		(",lib_full_name,"):Boxplot of raw data\n", sep=""))


# boxplot
	pdf(paste(plot_dir,"boxplot_miRNAexpr_notNorm_",lib_name,".pdf",sep=""), height=12, width=17)
	op<-par(las=3)
	# notch=TRUE: if the notches around the median of the boxplots do not cross each other, it is a sign for significant different medians. 
	boxplot(data, notch=TRUE, boxwex=0.6, col = "red", ylab= "log(read_count)")
	invisible(dev.off())

#print ("normalization & plotting(RAW DATA):   scatterplot")
##scatterplot with regression line
#pdf("scatterplot_miRNAexpr_notNorm.pdf", height=12, width=17)
#scatterplotMatrix (data, smooth =FALSE)
#dev.off()

# qqplot relativ to QX20 + linear regression line 
#if (length(names(data))%%3==2 )
#{
#	op<-par(mfrow = c(length(names(data))/2,2))
#
#
#} else if (length(names(data))%%3==1){
#
#	op<-par(mfrow = c(length(names(data))/3,3))
#}
#pdf("qqplots_miRNAexpr_notNorm.pdf", height=12, width=15)
#ref_name=names(data)[1]
#for (i in names(data)) 
#{
#	if (i!=ref_name)
#	{
#		qqplot(data[,i], data$ref_name, xlab=i, ylab=ref_name)
#		abline(lm(data$ref_name~data[,i]), col="red")
#		#abline(0,1, col="red")
#	}
#}
#par(op)
#dev.off()


## ------------------ normalization of data  ----------------------
cat (paste("		(",lib_full_name,"):Normalization of data\n", sep=""))
# Fo used methods please refer to the code
source(code_location)

## absolute count 
data_absolut_count_norm <- as.data.frame(log(norm.totalcount(data_raw)))

# quantile normalization
data_quantile_norm <-  as.data.frame(log(normalizeBetweenArrays(as.matrix(data_raw), method="quantile"))) 

# reference based quantile normalization 
data_quantile_rahman_norm <- rahmann.scaling (data_raw, min.count=5, do.log=TRUE, pseudo=0, sizeFactors=FALSE)
data_quantile_rahman_norm_not_log <- rahmann.scaling (data_raw, min.count=5, do.log=FALSE, pseudo=0, sizeFactors=FALSE)

# trimmed end mean of M value (TMM) (works only on the new cluster)
tmm_factors <- calcNormFactors(as.matrix(data_raw),method="TMM")
data_tmm_norm_raw = data_raw
for (i in 1:ncol(data_raw)) {
   data_tmm_norm_raw[,i] = data_tmm_norm_raw[,i] * tmm_factors[i]
} 
data_tmm_norm = log(data_tmm_norm_raw)

# variance stabilizing normalization (VSN)
# actually log(x) is a VSN
 # following works only on the old cluster
 # data_vsn_norm <- log(normalizeBetweenArrays(as.matrix(data_raw), method="vsn"))


## ------------------ plotting the normalized data  ----------------------
#
#
## boxplot of normalized data vs not norm  
cat (paste("		(",lib_full_name,"):Boxplot of the normalized data\n", sep=""))
	pdf(paste(plot_dir,"boxplot_miRNAexpr_Norm_",lib_name,".pdf",sep=""), height=12, width=15)
	op<-par(mfrow = c(2,3), las=3, cex=0.8, cex.sub=1.4)
	boxplot(data, notch=TRUE, boxwex=0.6, col = "red", ylab= "log(read_count)", sub= "Raw counts")
	boxplot(data_absolut_count_norm, notch=TRUE, boxwex=0.6, col = "red", ylab= "log(read_count)", sub= "Absolut count normalization")
	boxplot(data_quantile_norm, notch=TRUE, boxwex=0.6, col = "red", ylab= "log(read_count)", sub= "Quantile normalization")
	boxplot(data_quantile_rahman_norm, notch=TRUE, boxwex=0.6, col = "red", ylab= "log(read_count)", sub= "Quantile normalization (rahman)")
	boxplot(data_tmm_norm, notch=TRUE, boxwex=0.6, col = "red", ylab= "log(read_count)", sub= "TMM normalization")
	invisible(dev.off())

#scatterplot with regression line
#print ("normalization & plotting(NORMALIZED DATA):   scatterplot")
#pdf("scatterplot_miRNAexpr_Norm_abs.pdf", height=12, width=17)
#scatterplotMatrix (data_absolut_count_norm, smooth =FALSE)
#dev.off()
#pdf("scatterplot_miRNAexpr_Norm_quantile.pdf", height=12, width=17)
#scatterplotMatrix (data_quantile_norm, smooth =FALSE)
#dev.off()
#pdf("scatterplot_miRNAexpr_Norm_rahman_quntile.pdf", height=12, width=17)
#scatterplotMatrix (data_quantile_rahman_norm, smooth =FALSE)
#dev.off()
#pdf("scatterplot_miRNAexpr_Norm_tmm.pdf", height=12, width=17)
#scatterplotMatrix (data_tmm_norm, smooth =FALSE)
#dev.off()

#head(data_quantile_rahman_norm)
#names(data_quantile_rahman_norm)

# qqplot relativ to QX20 + linear regression line 
#pdf("qqplots_miRNAexpr_Norm_abs.pdf", height=12, width=15)
#op<-par(mfrow = c(3,3))
#for (i in colnames(data_absolut_count_norm)) 
#{
#	if (i!="QX20")
#{
#		qqplot(data_absolut_count_norm[,i], data_absolut_count_norm$QX20, xlab=i, ylab="QX20")
#		abline(lm(data_absolut_count_norm$QX20~data_absolut_count_norm[,i]), col="red")
#	}
#}
#par(op)
#dev.off()

#pdf("qqplots_miRNAexpr_Norm_quantile.pdf", height=12, width=15)
#op<-par(mfrow = c(3,3))
#for (i in colnames(data_quantile_norm)) 
#{
#	if (i!="QX20")
#	{
#		qqplot(data_quantile_norm[,i], data_quantile_norm$QX20, xlab=i, ylab="QX20")
#		abline(lm(data_quantile_norm$QX20~data_quantile_norm[,i]), col="red")
#	}
#}
#par(op)
#dev.off()
#
#pdf("qqplots_miRNAexpr_Norm_rahman_quntile.pdf", height=12, width=15)
#op<-par(mfrow = c(3,3))
#for (i in names(data_quantile_rahman_norm)) 
#{
#	if (i!="QX20")
#	{
#		qqplot(data_quantile_rahman_norm[,i], data_quantile_rahman_norm$QX20, xlab=i, ylab="QX20")
#		abline(lm(data_quantile_rahman_norm$QX20~data_quantile_rahman_norm[,i]), col="red")
#	}
#}
#par(op)
#dev.off()

#pdf("qqplots_miRNAexpr_Norm_tmm.pdf", height=12, width=15)
#op<-par(mfrow = c(3,3))
#for (i in names(data_tmm_norm)) 
#{
#	if (i!="QX20")
#	{
#		qqplot(data_tmm_norm[,i], data_tmm_norm$QX20, xlab=i, ylab="QX20")
#		abline(lm(data_tmm_norm$QX20~data_tmm_norm[,i]), col="red")
#	}
#}
#par(op)
#dev.off()

#





# ----------------------------------- writing the data ----------------------------------------------
to_write=recode(as.integer(norm_meth),"1='data_absolut_count_norm';2='data_quantile_norm';3='data_quantile_rahman_norm_not_log';4='data_tmm_norm'") 
print (output_dir)
write.table(eval(parse(text = to_write)), file = paste(output_dir,"allQX_miRNA_coverage_norm.txt",sep=""), sep = "\t", quote = FALSE)

