# This script gets a miRNA-name + it's target predictions and produces a plot of the distribution of the expression of thoese targets

library(eqtl)
library(gplots)

args <- commandArgs(TRUE)
miRNA_name <- args[1]     
Target_list <- unlist(strsplit(args[2], ",")) 
RILs<-unlist(strsplit(args[3], ",")) # 111,113,115,144,147,157,16,171,179,194,20,217,22,233,237,31,3,40,42,44,45,55,58,61,67,69,71,72,95,9
cross_file_rockman <- args[4] #R object mRNA expression
cross_file <- args[5] # Robject miRNA expression
folder<- args[6]

load(cross_file_rockman);
cross_data_rockman <- convert2riself(GEx)
cross_data_rockman<-subset(cross_data_rockman, ind = (cross_data_rockman$pheno[,3] %in% RILs))


load(cross_file);
name_to_grep=gsub("-",".",miRNA_name)
miRNA_expr=cross_data$pheno[,grep(paste(name_to_grep," *$",sep=""),phenames(cross_data))]
miRNA_expr=miRNA_expr-min(miRNA_expr,na.rm = TRUE)
print (miRNA_expr)
miRNA_expr=(miRNA_expr/max(miRNA_expr,na.rm = TRUE))
miRNA_RILs_order=as.numeric(gsub("QX","",cross_data$pheno[,1]))

miRNA_expr_matr=matrix(miRNA_expr, ncol=30)
colnames(miRNA_expr_matr)=miRNA_RILs_order

# starting the script
all_expr_targets=numeric()
Symbols=array()
RIL_order=cross_data_rockman$pheno['QXstrain'][,1]
for (target in Target_list)
{
	Pr_it=ProbeInfo[grep(paste(target," *$",sep=""), ProbeInfo$Systematic),]$Probe
	Symbol=subset(ProbeInfo, Probe %in% Pr_it)$Symbol
	if (!identical(Pr_it, character(0))) {
		expr=rowMeans(cross_data_rockman$pheno[Pr_it],na.rm = TRUE)

		exp_min_check=expr-min(expr,na.rm = TRUE)
		exp_min_check=(exp_min_check/max(exp_min_check,na.rm=TRUE))
		all_expr_targets=c(all_expr_targets,exp_min_check)
	}else{
		cat("Genes is not in the Rockman dataset: ",target,"\n" )	
		Target_list<-Target_list[!Target_list == target]
	}
	
	Symbols=c(Symbols,Symbol)

}	

Symbols_all=unique(Symbols)
Symbols_all<-na.omit(Symbols_all)
if (length(all_expr_targets)>=2){
	x_min=min(min(density(all_expr_targets,na.rm = TRUE)$x),min(density(miRNA_expr,na.rm = TRUE)$x))
	x_max=max(max(density(all_expr_targets,na.rm = TRUE)$x),max(density(miRNA_expr,na.rm = TRUE)$x))
	y_min=min(min(density(all_expr_targets,na.rm = TRUE)$y),min(density(miRNA_expr,na.rm = TRUE)$y))
	y_max=max(max(density(all_expr_targets,na.rm = TRUE)$y),max(density(miRNA_expr,na.rm = TRUE)$y))
	pdf(paste(paste(folder,miRNA_name,"_target_exp", sep=""),"pdf", sep="."), height=12, width=17)
	plot(density(all_expr_targets,na.rm = TRUE), main= paste("Target genes expression of ",miRNA_name,sep=""), sub=paste("Number of Genes: ", length(Target_list), sep=""), xlim=c(x_min,x_max),ylim=c(y_min,y_max), col='blue')
	points(all_expr_targets,rep(0,length(all_expr_targets)),col= 'blue' ,pch='|')
	points(density(miRNA_expr), type="l", col='red')
	points(miRNA_expr,rep((y_max/37),length(miRNA_expr)),col= 'red' ,pch='|')
	legend("topright",c("miRNA Expression","Target genes Expression"),col=c("red","blue"), lty=1)
	dev.off()

	# heatmap
	all_expr_targets_matrix=matrix(all_expr_targets, ncol=30, byrow=T)
	colnames(all_expr_targets_matrix)=RIL_order
	miRNA_exp=miRNA_expr_matr[match(colnames(all_expr_targets_matrix),colnames(miRNA_expr_matr))]
	all_expr_targets_matrix=rbind(all_expr_targets_matrix, miRNA_exp)

	rownames(all_expr_targets_matrix)=c(Target_list,miRNA_name)
 	pdf(paste(paste(folder,miRNA_name,"_target_exp_heatmap", sep=""),"pdf", sep="."), height=12, width=17)
	heatmap.2(all_expr_targets_matrix, col=colorRampPalette(c("white","green","green4","violet","purple"))(100), scale="none",key=TRUE, symkey=FALSE, density.info="density", trace="none",  cexRow=0.8)
	dev.off()

}else{
	print ("No Probes found for these genes")
}

