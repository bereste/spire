Two datasets are available for the user. Due to it's size the dataset containing the RNA-Seq reads is not included in the Spire archive and can be downloaded separately
The example datasets are available as follows:

Normalized expression
- miRNA_normalized_coverage_example.txt: normalized read count for 30 RILs ((Recombinant inbred lines). The reads were mapped to two different miRNA libraries (N2 strain and CB4856 strain). Homologous miRNAs with non-identical expression have to be preprocessed prior to eQTL mapping, in order to generate unambiguous values for all miRNAs.
- miRNA_normalized_coverage_expression_selected_mth3_example.txt: normalized read count for 30 RILs ((Recombinant inbred lines) from above. Homologous miRNAs with non-identical expression have been preprocessed, so that all miRNAs have unambiguous expression values.
- genotype_example.txt: Genotypes of 30 RILs and their genotypes across 1455 SNPs across the genome of C.elegans.
- miRNA_annotation.txt: Annotated positions of miRNAs.
 


RNA-Seq Reads (231 Mb)
- miRNA_reads_example.zip: RNA-seq miRNA fastq read files for 30 RILs. The adapter sequence was already removed and the reads were prefiltered. Due to its size,  only the first 50000 reads for each of the files are available.
- cel_miRBase_v18_novel_hairpin_all.fa: miRNA library for the N2 strain
- hw_miRBase_v18_novel_hairpin_all.fa: miRNA library for the CB4856 (HW) strain
- miRNA_both_lib.txt: both of the libraries concatenated. 


