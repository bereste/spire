#############################################################################################################################################################
#############################################################################################################################################################
#############################################################################################################################################################
############################################################## CONFIG FILE FOR THE SRIPE PIPELINE ###########################################################

# This file is used to define all the detailed parameters for the SPIRE pipeline. 
# Please make sure that all the necessary variables are set. 
# Each of the blocks can be set separately 
# NOTE: In case you set parameters, which are not used by the pipeline, they will be ignored.

#############################################################################################################################################################
######################################### ++++++++++++ ######################################
######################################### GLOBAL PATHS ######################################
######################################### ++++++++++++ ######################################

dataset_dir="/path_to_Spire/test_dataset/"  		   # location of the input files. 
output_dir="/path_to_Spire/test_dataset/output_folder/"    # location of the directory, where all the output files are placed
scripts_dir="/path_to_Spire/bin/"                          # location of other scripts 

######################################### ++++++++++++ ######################################
###################################### PREPROCESSING BLOCK ##################################
######################################### ++++++++++++ ######################################

###################### Demultiplexing (Barcode splitting). ###############################
#------------------------------------------------------------------->#

# you will need a fastq reads file, a fastq-file with barcode-for-each-read and the barcode.fa file with the barcodes showing to which sample the given read belongs. 
read_file=${dataset_dir}testing_009-014_1.fastq     
barcodes_for_read=${dataset_dir}testing_009-014_2.fastq     
barcode_seq=${dataset_dir}barcodes.fa     
output_name="QX" # for each barcode, a separate fastq file will be generated with the basename: ${output_name}
Number_of_threads_barcode=2
Memory_barcode="2G"
#<-------------------------------------------------------------------#


###################### Quality filtering the reads ###############################
#------------------------------------------------------------------->#

### Defining file names BEFORE the fastx filtering step (in case you don't use demultiplexing. If you use demultiplexing in the first step, this parameter will be ignored)
all_files=("Reads_1_example_before_filtering.fastq" "Reads_2_example_before_filtering.fastq" "Reads_3_example_before_filtering.fastq" "Reads_4_example_before_filtering.fastq" "Reads_5_example_before_filtering.fastq" "Reads_6_example_before_filtering.fastq")

Memory_fastx="5G"
## min p_par% of the nucleotides in one read must have a score obove q_par (p_par=80, q_par=20 means 99% secure basecall) to not be filtered out
p_par=80
q_par=20
#<-------------------------------------------------------------------#


###################### Adapter cliping of reads ###############################
#------------------------------------------------------------------->#

# Based on our internal experience two runs of adapter removal show the best results. The following settings are used: first run: "-mo10 -end right", second run:  "-mo 1 -end right_tail".
adapters=${dataset_dir}adapter.fa # adapter sequence
Number_of_threads_far2=7
Memory_far2="2G"
Min_overlap_far2_1=10
End_far2_1="right"
Min_overlap_far2_2=1
End_far2_2="right_tail"
#<-------------------------------------------------------------------#

######################################### ++++++++++++ ######################################
########################################## MAIN BLOCK #######################################
######################################### ++++++++++++ ######################################

###################### Input data ###############################
#------------------------------------------------------------------->#

### Gene Annotation data
# Gene Annotation. The following formats of data will be accepted. 
# - Genebank (The annotations as well as the gene libraries are created) 
# - GFF3/GTF2 file (The annotations are extracted. The libraries with the sequences have to be supplied separately)
# - Automatic download (The user provides the name of the species and the newest gene annotation is fetched using the R Biomart, as well as the gene sequence)
# - Internal format. Two separate file are needed. (gene libraries and annotations). Please note that the Chromosomes should be noted numerically with the exception of the X chr and mitochondrial chr (mtDNA). Also first should come the X chr. The last should be (if applicable) the mtDNA
gene_annotations=${dataset_dir}miRNA_annotation.txt

# Gene libraries are used to map your short Reads to the sequence.
# The libraries are accepted in one of the following formats:
# - fasta. Containing names of the genes and the corresponding sequence Please note: the Names of the species of in the libraries are determined by parsing the gene names and selecting the first part after ">" and before "-".
# - Genebank. Both the gene sequences and gene Annotations are created from this format.
# - (Automatic download)Biomart. The sequences, as well as the annotations are downloaded automatically using Biomart with the newest Ensembl database. 
gene_lib=${dataset_dir}gene_sequence_library_1.fa                       # first gene library (primary)
gene_lib2=${dataset_dir}gene_sequence_library_2.fa 			# second gene library in case you use two libraries. (-m option starting the pipeline)
both_libs=${dataset_dir}gene_sequence_libraries_1and2.txt 		# location of both gene libraries concatenated into one file

# In case you have two gene libraries to analyze: for the eQTL mapping the expression for each of the genes is selected according to one of the following strategies: 
# Chose a method for selecting the expression of each of the genes for the dataset for eQTL mapping (the gene sequence should be unique)
# 	1: Simple method = all genes from the primary library will be selected + unique ones from the secondary library
#       2: (TOFIX)Good estimate = for each gene, for each Marker the bigger value of the two libraries is selected
#       3: Precise method = for each sample, for each gene: the decision,  which expression value to chose (from the mapping to the primary library or to the secondary library), depending on the value of the Marker nearest to the gene
# NOTE: In case you've selected method 1, the first library will automatically be considered as primary.

expr_selection_method=1
###

### Genotype input data
# Genotype Data  can be supplied in one of the following formats:
# - .vcf (as used by the 1000 genome project)
# - hapmap format 
# - Plink hapmap format 
# - Series Matrix File(s) as offered by the Gene Expression Omnibus database for a project 
# - Internal format (Specified further)
# Please NOTE: the values for the genotypes have to be coded, such that elements correspond to A (coding for a genotype AA), H (AB) and B (BB). na.strings have to be coded as either NA or "-". In case you have a dataset of recombinant inbred lines (RIL), only two different genotype values are needed
all_genotype_Maker=${dataset_dir}genotype_example.txt

##################################

#<-------------------------------------------------------------------#


#######################################################################################################

###################### Mapping reads to genes ###############################
#------------------------------------------------------------------->#

# file_names AFTER the preprocessing (In case you didn't need any preprocessing). If you used the preprocessing module this variable will be ignored
all_files_ready=("Reads_1_example_after_filtering.fastq" "Reads_2_example_after_filtering.fastq" "Reads_3_example_after_filtering.fastq" "Reads_4_example_after_filtering.fastq" "Reads_5_example_after_filtering.fastq" "Reads_6_example_after_filtering.fastq")
Memory_read_mapping="3G"
Number_of_threads_read_mapping=2

# What mapping software should be used:
# 1 - RazorS: A read mapping program with adjustable sensitivity based on counting q-grams
# 2 - microRazorS: A tool optimized for mapping short RNAs.
mapping_software=2


#<-------------------------------------------------------------------#


###################### Normalization #######################################
#------------------------------------------------------------------->#

## In case you have a file with mapped reads all ready you can also submit it for further analysis

### Note: normalization is done by default over all of the submitted datasets. You can exclude some of the datasets. Please define them as shown in the example below. 
## Example: excluding_normalization=("N2.fastq" "HW.fastq")
excluding_normalization=("")

norm_func_file=${scripts_dir}normalization_functions.r # Externally defined R normalization functions. Please edit only if you are sure

# What normalization method should be applied. (default: rahman quantile normalization)
# 1:absolute count 2:quantile normalization 3(default):reference based quantile normalization (rahman)  4:trimmed end mean of M value (TMM)
norm_meth=3 
#<-------------------------------------------------------------------#



###################### Normalized expression file #######################
#------------------------------------------------------------------->#

# In case you didn't run the normalization block with your data you can also run the script with an external normalized coverage. This file is than use in the following modules
normalized_coverage_file=${dataset_dir}miRNA_normalized_coverage_example.txt
#<-------------------------------------------------------------------#



###################### Further statistical analysis #######################
#------------------------------------------------------------------->#

# In case you don't want analysis regarding the conservation be sure that miRBase_conservations has no value

# Input format:
# First row: miRNA_name         5p-conservations(names of the phyla) 3p-conservations(names of the used phyla)
# miRNA_name    5p-conservations (e.g. - / + / - / +)   3p-conservations 
# output code:
# 0: o conservations
# 1: +---, -+--
# 2: ++--
# 3: +++-, ++-+, -+-+, +--+, -++-, +-+-, --++,+-++, (--+-),(---+)
# 4: ++++
miRBase_conservations=""

# Conservation groups. A file has to be present describing which conservation codes belong to which conservation group
cons_lib=${dataset_dir}conservation_code.txt
#<-------------------------------------------------------------------#


###################### eQTL mapping ####################################
#------------------------------------------------------------------->#


# In case you didn't run the normalization block with your data you can also run the script with an external normalized coverage. 
# Please edit the path to the file normalized_coverage_file variable (above)

# Parameters used for clustering of the markers (SNP Tagging)
output_name_clustering="Marker_clustering"
Number_of_threads_Clustering=3
Memory_Clustering="17G"

### choose a eQTL mapping method
# 0 : All methods
# 1 (default): All pre-defined methods
# 2: All per-defined UNI-variate methods
# 3: All per-defined MULTI-variate methods
# 4: All external methods, provided by the user (see the help file)
# 5: (Univariate Method) Standart interval mapping 
# 6: (Univariate Method) Extended Haley-Knott method 
# 7: (Univariate Method) Multiple imputation method 
# 8: (Multivariate Method) Random Forest
# 9: (Simplest Univervariate method) Marker Regression, ANOVA. 
eQTL_method=7

### Choose whether an FDR correction for multiple testing should be used and what significance level should be applied
use_FDR=1
sign_level=0.05
drop=1 # Drop defines the LOD support interval for a found QTL around the identified Marker position. (applicable only for the methods that use LOD score)
sign_level_FDR=0.05
Number_of_threads_eQTL_FDR=7
output_name_eQTL_FDR="eQTL_FDR"
Memory_eQTL_FDR="2G"


### Parameters to use during the eQTL mapping (univariate methods)
output_name_eQTL="b_eQTL_map"  
Memory_eQTL_preload="8G" # This is used to preload the cross_file and to calculate the estimated and missing genotypes. Needs a lot of RAM
Number_of_threads_eQTL_preload=1
Number_of_threads_eQTL=2
Number_of_machienes_eQTL=60
Memory_eQTL="9G"	# keep in mind, that the overall amount of memory used is the multiplied by the number of threads


### Internal parameters of the QTL/R package
# The following values are used for the functions calc.genoprob() and sim.geno(). Please refer to the R/QTL help manual for discription
step_v=0
error_prob_v=0.0001
map_function_v="kosambi"
stepwidth_v="fixed"
n_draws_v=16 # used only for sim.geno()
n_perm_v=40000 # How many permutations of the data should be used for for the p_value estimation (the p_value is estimated emiricaly) 

### Internal parameters of the Random Forest method
Number_of_threads_eQTL_RF=20 # how many different machienes are availible
Number_of_cores_eQTL_RF=7  # how many cores do those machines have?
Memory_eQTL_RF="3G"	# keep in mind, that the overall amount of memory used is the multiplied by the number of cores on the machnie
ntree_v=1000 # Number of trees used to estimate the selection bias in RF when the null hypothesis is true (i.e. no association between response and predictors). (Must be a multiple of 10)
nforest_v=20 # number of forests produced with the (ntree_v) tries for the imperial p_value estimation

### Parameters to use during the effectplots
output_name_effectplots="eQTL_effectplots"  
Memory_effectplot="2G"
#<-------------------------------------------------------------------#



###################### cis/trans plotting ####################################
#------------------------------------------------------------------->#
# The following parameters will be ignored in case you've run the eQTL mapping inside Spire
# the file with the identified QTLs
identified_QTLs_file=${dataset_dir}QTLs_identified.txt
# the file with 
Marker_pos_variable=${dataset_dir}Markers_pos_clustered.txt
# the size of the bin for QTL hotspots. (Measured in the number of Markers)
bin_step=1
# p_value of significant Hotspots. 
p_val_hotspot=0.025
